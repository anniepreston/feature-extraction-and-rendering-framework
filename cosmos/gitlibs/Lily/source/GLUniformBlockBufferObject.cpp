#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif

#include "GLError.h"
#include "GLUniformBlockBufferObject.h"

namespace lily{

	GLUniformBlockBufferObject::GLUniformBlockBufferObject(
		const std::string& name/*="Untitled UBBO"*/,
		GLenum bufferUsage/*=GL_STATIC_DRAW*/)
		:GLBufferObject(GL_UNIFORM_BUFFER, bufferUsage, name)
	{	
		
	}

}
