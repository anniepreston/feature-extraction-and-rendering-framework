#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif
#include <string>
#include <iostream>
#include "GLError.h"
#include <QMessageBox>
#include <QDebug>

namespace lily{
#define MAX_ERROR_LENGTH 256

GLError::GLError(void)
{
}

GLError::~GLError(void)
{
}

void GLError::glCheckError(const char* call)
{
	char enums[][50] =
	{
		"invalid enumeration", // GL_INVALID_ENUM
		"invalid value",       // GL_INVALID_VALUE
		"invalid operation",   // GL_INVALID_OPERATION
		"stack overflow",      // GL_STACK_OVERFLOW
		"stack underflow",     // GL_STACK_UNDERFLOW
		"out of memory",        // GL_OUT_OF_MEMORY
        "invalid framebuffer operation"// GL_INVALID_FRAMEBUFFER_OPERATION
	};

	GLenum errcode = glGetError();
	if (errcode == GL_NO_ERROR)
		return;

	errcode -= GL_INVALID_ENUM;
	QString message;
	message.sprintf("OpenGL %s in '%s'",enums[errcode], call);
	//qWarning()<<message;
    ErrorMessage(message);
}

void GLError::glCheckError( const std::string& call )
{
    glCheckError((const char*)call.data());
}

void GLError::purgePreviousGLError()
{
    glGetError();
}

void GLError::ErrorMessage(const std::string &msg )
{
    std::cerr << msg << std::endl;
	QMessageBox::warning(NULL,"Error",QString(msg.data()),QMessageBox::Ok);
#ifdef DEBUG
    std::vector<int> v;
    v[10]=0;
#else
	exit(1);
#endif
}

void GLError::ErrorMessage(const QString &msg )
{
    std::cerr << qPrintable(msg) << std::endl;
    QMessageBox::warning(NULL,"Error",msg,QMessageBox::Ok);
#ifdef DEBUG
    std::vector<int> v;
    v[10]=0;
#else
    exit(1);
#endif
}
void GLError::WarningMessage( const QString &msg,QWidget* parent )
{
	qDebug()<<msg;
	QMessageBox::warning(parent,"Warning",msg,
		QMessageBox::Ok, QMessageBox::NoButton);
}

}//end of namespace