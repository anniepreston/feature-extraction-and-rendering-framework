#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif

#include "GLError.h"
#include "GLShaderStorageBufferObject.h"
//https://www.opengl.org/wiki/Shader_Storage_Buffer_Object
//https://www.opengl.org/wiki/Interface_Block_%28GLSL%29#Shader_storage_blocks
//http://www.geeks3d.com/20140704/tutorial-introduction-to-opengl-4-3-shader-storage-buffers-objects-ssbo-demo/

namespace lily{

	/*
	GLShaderStorageBufferObject::GLShaderStorageBufferObject(void)
		:m_bindingPnt(-1), m_name("Untitled"),//, m_bActive(false),
		GLBufferObject(GL_SHADER_STORAGE_BUFFER, GL_DYNAMIC_DRAW)
	{
	}

	GLShaderStorageBufferObject::GLShaderStorageBufferObject(GLuint bindPt, const char* name)
		: m_bindingPnt(bindPt), m_name(name),
		GLBufferObject(GL_SHADER_STORAGE_BUFFER, GL_DYNAMIC_DRAW)
	{
	}
	*/

	
	GLShaderStorageBufferObject::GLShaderStorageBufferObject(
		  const std::string& name/*="Untitled SBBO"*/, GLenum bufferUsage/*=GL_STATIC_DRAW*/)
		: GLBufferObject(GL_SHADER_STORAGE_BUFFER, bufferUsage, name)
	{
		
	}

	/*
	void GLShaderStorageBufferObject::setBindingPoint(GLuint id)
	{
		m_bindingPnt = id;
		bindBufferObject();
		glBindBufferBase(m_target, m_bindingPnt, m_id);
		unbindBufferObject();
		stringstream ss;
		ss << __func__ << ": " << m_name 
		   << " connecting SSBO base to binding point failed!\n";
		GLError::glCheckError(ss.str());
	}

	GLuint GLShaderStorageBufferObject::getBinding() const
	{
		return m_bindingPnt;
	}
	*/
}
