#include "vec4i.h"
#include "vec3d.h"
#include "vec4f.h"
#include "vec4d.h"
//using namespace mathtool;
namespace lily{

vec4d::vec4d( const vec3d& v, double w )
{
    m_x = v.x(); m_y = v.y(); m_z = v.z();
    m_w = w;
}

vec4d::vec4d( const vec4i& v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
    m_w = (double)v.w();
}

vec4d& vec4d::operator=( const vec4i &v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
    m_w = (double)v.w();
    return *this;
}

vec4d& vec4d::operator=( const vec4f &v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
    m_w = (double)v.w();
    return *this;
}

}

