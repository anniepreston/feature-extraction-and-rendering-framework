#include <sstream>
#include <iostream>
#include "GLAttribute.h"
#include "GLError.h"

namespace lily{

GLAttribute::GLAttribute(GLuint shaderProgId,  const string& name,
	 GLenum type, GLuint size, GLuint stride/*=0*/, 
	 GLuint offset/*=0*/, bool normalized/*=false*/,bool warning/*=false*/,
	 const GLVertexBufferObjectRef vbo/*=NULL*/)
     :m_shaderProgId(shaderProgId), m_name(name),
	  m_type(type),m_nComponents(size), m_stride(stride),m_offset(offset),
	  m_normalized(normalized),m_enableWarning(warning), m_vbo(vbo)
{
}

GLAttribute::~GLAttribute(void)
{
}
//for GL 3.3 core version.
#if defined(__APPLE__) || defined(MACOSX)
void GLAttribute::enable()
{
    GLint index=-1;
    index = glGetAttribLocation( m_shaderProgId, m_name.data());
    if (index==-1)
    {
        std::stringstream ss;
        ss <<"GLAttribute::enable3_3(): index==-1, cannot find "<<m_name<<"\n";
        std::string msg=ss.str();
        GLError::ErrorMessage(msg);
    }else if(m_vbo && m_vbo->getVertexCount()>0){
        m_vbo->bindBufferObject();
        GLError::glCheckError("GLAttribute::enable() m_vbo.bind() failed!");
    }
    glEnableVertexAttribArray(index);
    //http://www.opengl.org/wiki/Vertex_Buffer_Object#Vertex_format/
    //For floating-point attributes, you must use glVertexAttribPointer.
    //For integer (both signed and unsigned), you must use glVertexAttribIPointer.
    //And for double-precision attributes, where available, you must use glVertexAttribLPointer.
    if(m_type == GL_INT || m_type == GL_UNSIGNED_BYTE
                    || m_type == GL_UNSIGNED_INT){
        //We need glVertexAttribIPointer( with I) to match your shader
        //int/uint attribute.
        glVertexAttribIPointer(index, m_nComponents, m_type,m_stride,\
                               reinterpret_cast<const GLvoid*>(m_offset));
    }
//    else if (m_type == GL_DOUBLE)
//    {
//        glVertexAttribLPointer(index, m_nComponents, m_type,m_stride,\
//                               reinterpret_cast<const GLvoid*>(m_offset));
//    }
    else{
        glVertexAttribPointer(index, m_nComponents, m_type,m_normalized,m_stride,
                              reinterpret_cast<const GLvoid*>(m_offset));
    }

    if(m_vbo && m_vbo->getVertexCount()>0){
        m_vbo->unbindBufferObject();
        GLError::glCheckError("GLAttribute::enable() m_vbo.unbind() failed!");
    }

    GLError::glCheckError(__func__);
}
#else
//For GL version 4.4
void GLAttribute::enable()
{
	GLint attrbIndex=-1;
	attrbIndex = glGetAttribLocation( m_shaderProgId, m_name.data());

	if (attrbIndex==-1)
	{
		std::stringstream ss;
		ss <<__func__<<": index==-1, cannot find "<<m_name<<"\n";
		std::string msg=ss.str();
		GLError::ErrorMessage(msg);

	}else if (!m_vbo)
	{
		std::stringstream ss;
		ss <<__func__<<": m_vbo==NULL, there is no associated VBO with the attribute "
		   <<m_name<<".\nCall attach() or specify a VBO at the constructor.";
		std::string msg=ss.str();
		GLError::ErrorMessage(msg);
	}else if (m_vbo->getVertexCount()<=0)
	{
		std::stringstream ss;
		ss <<__func__<<": m_vbo->getVertexCount()<=0, No vertex attribute content in the associated VBO with the attribute "
		   <<m_name<<".\nCall GLVertexBufferObject::upload() to upload vertex attribute content to GPU";
		std::string msg=ss.str();
		GLError::ErrorMessage(msg);
	}
	//glBindVertexBuffer(0, buff, baseOffset, sizeof(Vertex));
	//http://www.opengl.org/wiki/Vertex_Buffer_Object#Vertex_format/
	//http://www.sinanc.org/blog/?p=450
	//if there is any GLArrayBufferObject associate with the attribute,
	//associate it with the attribute. It assumes that one attribute
	//is associated with one array buffer object. Structure of Array is assumed.
	if(m_type == GL_INT || m_type == GL_UNSIGNED_BYTE
       || m_type == GL_UNSIGNED_INT){
        //We need glVertexAttribIFormat( with I) to match your shader 
        //int/uint attribute.
		glVertexAttribIFormat(attrbIndex, m_nComponents, m_type, m_offset);
    }
	else if (m_type == GL_DOUBLE)
	{
		glVertexAttribLFormat(attrbIndex, m_nComponents, m_type, m_offset);
	}
    else{
		glVertexAttribFormat(attrbIndex, m_nComponents, m_type, m_normalized, m_offset);
    }
	glVertexAttribBinding(attrbIndex, m_vbo->getBindingIndex());
	glEnableVertexAttribArray(attrbIndex);
	glBindVertexBuffer(m_vbo->getBindingIndex(), m_vbo->getId(), 0, m_stride);
	GLError::glCheckError(__func__);
}
#endif

void GLAttribute::disable()
{
	GLint index=-1;
	index = glGetAttribLocation( m_shaderProgId, m_name.data());
	if (index==-1)
	{
		if (m_enableWarning)
		{
			std::stringstream ss;
			ss <<__func__<<": Attribute "<<m_name<<" is not active.\n";
			std::string msg=ss.str();
			cerr << msg;
		}
		std::stringstream ss2;
		ss2 <<__func__<<": Something weird happens while attempting to disable attribute "
			<< m_name <<". It's not found/active at current status and also triggers an OpenGL internal error."
			<<"You've probably declared one of GLVertexArrayObject as global GLVertexBufferObjectRef, "
			<<"try to use GLVertexBufferObject* or declare GLVertexBufferObjectRef inside a class or "
			<<"use GLVertexBufferObjectRef as global variable but use its compatibility mode to specify vertex format.\n";
		std::string msg2 = ss2.str();
		GLError::glCheckError(msg2.data());
	}else{
		std::stringstream ss;
		ss << __func__ <<": Disabling vertexAttribArray:"<<m_name<<" failed!\n";
		std::string msg = ss.str();

		glDisableVertexAttribArray(index);
		m_vbo->releaseBindingIndex();
		GLError::glCheckError(msg);
	}
}

}//end of namespace
