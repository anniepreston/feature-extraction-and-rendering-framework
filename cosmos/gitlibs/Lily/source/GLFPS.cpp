#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif
#include <stdlib.h>
#include <GL/freeglut.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <QFont>
#include <QGLWidget>
#include "GLUtilities.h"
#include "GLFont.h"
#include "GLFPS.h"
using namespace std;
using namespace lily;

GLFPS::GLFPS(void)
    :m_font(QFont("Times", 32)),m_color(QColor(255, 223, 127))
{
	m_timer.start();
}

GLFPS::~GLFPS(void)
{
}

void GLFPS::startFPS()
{
    m_start = m_timer.elapsed();
}

float GLFPS::endFPS()
{
    float fps = 0.0f;
    int end  = m_timer.elapsed();
    if (end != m_start)
    {
        fps = 1000.0f / (float)(end - m_start);
    }
    printf("Time=%d ms/frame, FPS=%f\n",(end - m_start),fps);
    return fps;
}

float GLFPS::showFPS(int scrWidth, int scrHeight, QGLWidget *widget)
{
    float fpss = endFPS();
    QFontMetrics fm(m_font);
    const int fontHeight = fm.height();//14;
    float color[4]={1.f, 1.f, 1.f, 1.f};
    void *font = GLUT_BITMAP_HELVETICA_18;//GLUT_BITMAP_TIMES_ROMAN_24;//GLUT_BITMAP_HELVETICA_18;//GLUT_BITMAP_8_BY_13;
    stringstream ss;
    ss << "FPS: ";
    ss << std::fixed << std::setprecision(3)<<fpss <<" "<<scrWidth<<"x"<<scrHeight;

    if (!widget){
        glBlendFunc(GL_ONE_MINUS_DST_COLOR, GL_ZERO); // invert color
        glEnable(GL_BLEND);
        GLUtilities::glBegin2DCoords(scrWidth, scrHeight);
             GLFont::drawString2D(ss.str().c_str(), -scrWidth*0.5f+2, scrHeight*0.5f-15, color, font);
        GLUtilities::glEnd2DCoords();
        glDisable(GL_BLEND);
    }else{
        widget->setFont(m_font);
        widget->qglColor(m_color);
        widget->renderText(1, fontHeight,QString(ss.str().c_str()));
   }

   return fpss;
}

void GLFPS::setFont( const QFont& f )
{
    m_font = f;
}

void GLFPS::setColor( const QColor& c )
{
    m_color = c;
}