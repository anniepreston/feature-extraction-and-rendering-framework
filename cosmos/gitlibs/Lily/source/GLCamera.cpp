//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//UPDATED: 2014-05-06
#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif
#include "GLCamera.h"
#include <GL/glu.h>
#include <GL/freeglut.h>
#include "constant.h"
#include "GLError.h"
#include "mathtool.h"

namespace lily{

GLCamera::GLCamera(void)
		:m_posDest(0.0f,0.0f,0.0f), m_backward(0.0f,0.0f,-1.0f),
		  m_up(0.0f, 1.0f, 0.0f),m_right(1.0f,0.0f,0.0f),m_speed(0.2f)
		,m_flipped(1), m_inertiaXYZ(1.0f,1.0f,1.0f),m_targetSphericalAngleDest(PI,0.0f)
		,m_inertiaLookAt(1.0f), m_lookAtMode(FREE_FORM), m_iStep(0),m_bIsRoaming(false)
		,m_staringPos(0.0f)
{
	m_posLag = m_posDest;
	m_targetSphericalAngleLag = m_targetSphericalAngleDest;
}

void GLCamera::toggleRoaming( bool val )
{
	m_bIsRoaming = val;
}

void GLCamera::setLookAt( const vec3f& position, const vec3f& target,
						  const vec3f& up/*=vec3f(0.0f,1.0f,0.0f)*/ )
{
	m_target = target;
	m_backward = position - target;
	m_backward.normalize();
	//Extra care to take when forward vector is on the Y axis.
	if (fabsf(m_backward.x()) < 0.00001f//FLOAT_EPSILON
         && fabsf(m_backward.z()) < 0.00001f)//FLOAT_EPSILON)
	{
		m_flipped *= -1.0f;
		if (m_backward.y() > 0)//looking upward
		{
			m_up = vec3f(0.0f, 0.0f, 1.0f);
		}
		else//looking downward
		{
			m_up = vec3f(0.0f, 0.0f, -1.0f);
		}
	}else{
		// in general, up vector is straight up
		m_up = up;
		m_up.normalize();
	}
	//compute right vector in left handed system
	m_right = m_up.cross(m_backward);
	m_right.normalize();
	//re-calculate the orthonormal up vector.
	m_up = m_backward.cross(m_right);
	m_up.normalize();
	//Create the Viewing matrix in row major order,
	m_viewMtx = mat4(m_right.x(),    m_right.y(),	  m_right.z(),    -(m_right.dot(position)),\
					  m_up.x(),		  m_up.y(),	      m_up.z(),       -(m_up.dot(position)),\
					  m_backward.x(), m_backward.y(), m_backward.z(), -(m_backward.dot(position)),\
					  0,		  0,		0,						  1);
}

mat4 GLCamera::setFrustum( float l, float r, float b, float t, float n, float f )
{
	m_projMtx = mat4::createPerpProjMatrix(l,r,b,t,n,f);
	return m_projMtx;
}

mat4 GLCamera::setFrustum( float fovY, float aspect, float front, float back )
{
	m_projMtx = mat4::createPerpProjMatrix(fovY, aspect, front, back);
	return m_projMtx;
}

mat4 GLCamera::setOrthoFrustum( float l, float r, float b, float t, float n, float f )
{
	m_projMtx = mat4::createOrthoProjMatrix(l,r,b,t,n,f);
	return m_projMtx;
}

void GLCamera::moveForward( float stepsize)
{
	m_posDest += (-m_backward*stepsize);
}

void GLCamera::moveRight( float stepsize)
{
	m_posDest += (m_right * stepsize);
}

void GLCamera::moveUp( float stepsize)
{
	m_posDest += (m_up * stepsize);
}

void GLCamera::forwardTime()
{
	m_posLag += (m_posDest - m_posLag)*m_inertiaXYZ;
	m_targetSphericalAngleLag +=
					(m_targetSphericalAngleDest - m_targetSphericalAngleLag)*m_inertiaLookAt;
	float yt = sin(m_targetSphericalAngleLag[1]);
	float xt = cos(m_targetSphericalAngleLag[1])*sin(m_targetSphericalAngleLag[0]);//
	float zt = cos(m_targetSphericalAngleLag[1])*cos(m_targetSphericalAngleLag[0]);
	vec3f viewDirLag(xt,yt,zt);
	m_target = m_posLag + viewDirLag;

	if (m_bIsRoaming)
	{
		if (m_bLoop)
		{//enable loop. Once camera finished traveling the entire path, it will
			//restart again from the very begining.
			m_iStep = (m_iStep+1) % camera_path.getOutputPtArrayRef().size();
		}else{
			//or else stay at the end, waiting for user to exit to interactive mode.
			m_iStep = (GLuint)min(size_t(m_iStep+1),camera_path.getOutputPtArrayRef().size()-1);
		}
		setDestPosition(camera_path.getOutputPtArrayRef()[m_iStep]);
		setTranslateInertia(vec3f(1.0f));
		if (m_lookAtMode == FOCUS_OBJECT)
		{//focus on a object
			setLookAt(getCurrentPosition(), m_staringPos);

		}else if (m_lookAtMode == FOLLOW_PATH)
		{//look along the current tangent
			vec3f next_pos =
							camera_path.getOutputPtArrayRef()[(m_iStep+1)%camera_path.getOutputPtArrayRef().size()];
			setLookAt(getCurrentPosition(), next_pos);

		}else if (m_lookAtMode == FREE_FORM)
		{//using the interpolated camera viewing direction.
			setLookAt(getCurrentPosition(), getCurrentPosition()+getCameraDirs()[m_iStep]);
		}
	}else{
		setLookAt(getCurrentPosition(), getTarget());// , m_up);
	}
}

void GLCamera::forwardTime(GLuint step)
{

	m_posLag += (m_posDest - m_posLag)*m_inertiaXYZ;
	m_targetSphericalAngleLag +=
					(m_targetSphericalAngleDest - m_targetSphericalAngleLag)*m_inertiaLookAt;
	float yt = sin(m_targetSphericalAngleLag[1]);
	float xt = cos(m_targetSphericalAngleLag[1])*sin(m_targetSphericalAngleLag[0]);//
	float zt = cos(m_targetSphericalAngleLag[1])*cos(m_targetSphericalAngleLag[0]);
	vec3f viewDirLag(xt,yt,zt);
	m_target = m_posLag + viewDirLag;

	if (m_bIsRoaming)
	{
		//m_iStep = (m_iStep+1) % camera_path.getOutputPtArrayRef().size();
		setDestPosition(camera_path.getOutputPtArrayRef()[step]);
		setTranslateInertia(vec3f(1.0f));
		if (m_lookAtMode == FOCUS_OBJECT)
		{//focus on a object
			setLookAt(getCurrentPosition(), m_staringPos);

		}else if (m_lookAtMode == FOLLOW_PATH)
		{//look along the current tangent
			vec3f next_pos =
							camera_path.getOutputPtArrayRef()[(step+1)%camera_path.getOutputPtArrayRef().size()];
			setLookAt(getCurrentPosition(), next_pos);

		}else if (m_lookAtMode == FREE_FORM)
		{//using the interpolated camera viewing direction.
			vec3f& dir = getCameraDirs()[step];
			setLookAt(getCurrentPosition(), getCurrentPosition()+dir);
		}
	}else{
		setLookAt(getCurrentPosition(), getTarget());
	}
}
void GLCamera::mouseMotion( int x, int y,int mouseButton )
{
	if (mouseButton == GLUT_LEFT_BUTTON)
	{
	}else if (mouseButton == GLUT_MIDDLE_BUTTON)
	{
	}else if (mouseButton == GLUT_RIGHT_BUTTON)
	{
        /*Currently working solution
	 *Spherical coordinates to control target.
	 */
		m_targetSphericalAngleDest[0] += ((m_prevX-x)*DEG2RAD)*0.5f;
		m_targetSphericalAngleDest[1] += ((m_prevY-y)*DEG2RAD)*0.5f;
	}
	m_prevX = x;
	m_prevY = y;
}

void GLCamera::mouseButton(int x, int y, int mouseButton)
{
	m_prevX = x; m_prevY = y;
}

void GLCamera::SpaceballMotion(int x, int y, int z)
{
	if (x || y || z)
	{
		lily::vec3f e_dir(x, y, z);//direction in eye/camera space
		e_dir.normalize();
		float scalef = 1.0;
		vec3f curPos = getCurrentPosition();
		vec3f curDst = getDestPosition();
		lily::vec3f w_dir(0.0f);
		w_dir += m_right * e_dir.x();
		w_dir += m_up * e_dir.y();
		w_dir += m_backward * e_dir.z();

		m_posDest = (m_posLag + w_dir * scalef);
	}
}

void GLCamera::SpaceballRotation(int rx, int ry, int rz)
{
	/**Currently working solution
	 *Spherical coordinates to control target.
	 */
	if (rx || ry || rz)
	{
		float scalef = 0.5f;
		m_targetSphericalAngleDest[0] = m_targetSphericalAngleLag[0] + ry * scalef * DEG2RAD;
		m_targetSphericalAngleDest[1] = m_targetSphericalAngleLag[1] + rx * scalef * DEG2RAD;
	}
}

mat4 GLCamera::rotated( float angle, const vec3f& axis )
{
	mat4 rotMtx;
	rotMtx = rotMtx.rotated(angle, axis);
	m_backward = rotMtx * m_backward;
	m_right  = rotMtx * m_right;
	m_up	= rotMtx * m_up;
	setLookAt(m_posDest, m_posDest-m_backward, m_up);
	return getViewingMatrix();
}

mat4 GLCamera::pitchd( float angle )//x
{
	return rotated(angle, m_right);
}

mat4 GLCamera::yawd( float angle )//y
{
	return rotated(angle, m_up);
}

mat4 GLCamera::rolld( float angle )
{
	return rotated(angle, m_backward);
}

mat4 GLCamera::rotate( float angle, const vec3f& axis )
{
	mat4 rotMtx;
	rotMtx = rotMtx.rotate(angle, axis);
	m_backward = rotMtx * m_backward;
	m_right = rotMtx * m_right;
	m_up	= rotMtx * m_up;
	setLookAt(m_posDest, m_posDest-m_backward, m_up);
	return getViewingMatrix();
}

lily::mat4 GLCamera::rotate( const mat4& rotMtx )
{
	m_backward = rotMtx * m_backward;
	m_right = rotMtx * m_right;
	m_up	= rotMtx * m_up;
	setLookAt(m_posDest, m_posDest-m_backward, m_up);
	return getViewingMatrix();
}

mat4 GLCamera::pitch( float angle )
{
	return rotate(angle, m_right);
}

mat4 GLCamera::yaw( float angle )
{
	return rotate(angle, m_up);
}

mat4 GLCamera::roll( float angle )
{
	return rotate(angle, m_backward);
}

void GLCamera::addKeyframe()
{ 
	camera_path.addControlPt(getCurrentPosition());
	vec3f dir = (getTarget()-getCurrentPosition());
	dir.normalize();
	camera_keydirs.push_back(dir);
}

void GLCamera::generatePath()
{
	camera_path.createCurve();
	cout << "# of points on path="<<camera_path.getOutputPtArrayRef().size()<<endl;
	int res = getInterpRes();
	float delta_t = 1.0f/float(res+1);
	size_t nCtlPt = camera_path.getControlPtArrayRef().size();
	cout << "nCtlPt="<<nCtlPt<<endl;
	camera_interpDirs.clear();
	for (size_t i = 0 ; i < nCtlPt - 1; i++)
	{
		//for each interval [P_{i}, P_{i+1}]
		vec3f& dir1 = camera_keydirs[i];
		vec3f& dir2 = camera_keydirs[i+1];
		for (float t = 0 ; t <= 1.0f ; t += delta_t)
		{
			camera_interpDirs.push_back(slerp(dir1, dir2, t));
		}
	}
}

void GLCamera::save( const string& file, GLuint iomode/*=0*/ )
{
	ofstream ofs(file, (ios_base::openmode)iomode);
	if (!ofs)
	{
		string msg=file+" not exists.";
		GLError::ErrorMessage(msg);
	}

	camera_path.save(ofs, iomode);

	if(iomode == ios::binary)
	{
		GLuint size = (GLuint) camera_keydirs.size();
		ofs.write(reinterpret_cast<char*>(&size), sizeof(GLuint));
		ofs.write(reinterpret_cast<char*>((float*)(camera_keydirs.data()))
				  , size* sizeof(vec3f));

		size = (GLuint) camera_interpDirs.size();
		ofs.write(reinterpret_cast<char*>(&size), sizeof(GLuint));
		ofs.write(reinterpret_cast<char*>((float*)(camera_interpDirs.data()))
				  , size* sizeof(vec3f));

	}else
	{	//1. Save tangent knot array
		ofs << camera_keydirs.size() << "\n";
		for (int i = 0 ; i <  camera_keydirs.size() ; i++)
		{
			ofs << camera_keydirs[i];
		}
		//2. Save tangent vector array
		ofs << camera_interpDirs.size() << "\n";
		for (int i = 0 ; i <  camera_interpDirs.size() ; i++)
		{
			ofs << camera_interpDirs[i];
		}
	}
	ofs.close();
}

void GLCamera::load( const string& file, GLuint iomode/*=0*/ )
{
	ifstream ifs(file, ios::in | (ios_base::openmode) iomode);
	if (!ifs)
	{
		string msg=file+" not exists.";
		GLError::ErrorMessage(msg);
	}
	camera_path.load(ifs, iomode);

	camera_keydirs.clear();
	camera_interpDirs.clear();
	GLuint size=0;
	if (iomode == ios::binary)
	{
		ifs.read(reinterpret_cast<char*>(&size), sizeof(GLuint));
		camera_keydirs.resize(size);
		cout << "# of camera key dirs="<<size<<endl;
		ifs.read(reinterpret_cast<char*>(camera_keydirs.data())
				 ,sizeof(vec3f)*size);
		ifs.read(reinterpret_cast<char*>(&size), sizeof(GLuint));
		cout << "# of camera interp dirs="<<size<<endl;
		camera_interpDirs.resize(size);
		ifs.read(reinterpret_cast<char*>(camera_interpDirs.data())
				 ,sizeof(vec3f)*size);
	}
	else
	{
		//1. Load control points.
		ifs >> size;
		camera_keydirs.resize(size);
		for (GLuint i = 0 ; i < size; i++)
		{
			ifs >> camera_keydirs[i];
		}
		//2. Load points on curve.
		ifs >> size;
		camera_interpDirs.resize(size);
		for (GLuint i = 0 ; i < size; i++)
		{
			ifs >> camera_interpDirs[i];
		}
	}
	ifs.close();
}

void GLCamera::draw()
{
	camera_path.draw();
	std::vector<vec3f>& pathPts = camera_path.getOutputPtArrayRef();
	size_t nPts = pathPts.size();
	if (m_bDrawViewDirs)
	{
		glBegin(GL_LINES);
		for (size_t i=0 ; i < nPts ; i++ )
		{
			vec3f& start = pathPts[i];
			vec3f& dir = camera_interpDirs[i];
			vec3f end = start+dir*0.1f;
			glColor3f(1,0,0);
			glVertex3fv((float*)start);
			glVertex3fv((float*)end);
		}
		glEnd();
	}
}

void GLCamera::clearCameraPath()
{
	camera_path.clear();
	camera_keydirs.clear();
	camera_interpDirs.clear();
}

}//end of namespace.
