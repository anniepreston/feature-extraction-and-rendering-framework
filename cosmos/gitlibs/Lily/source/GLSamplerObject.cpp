#include "GLSamplerObject.h"
#include "GLError.h"

namespace lily{

GLSamplerObject::GLSamplerObject( GLint filter/*=GL_LINEAR*/, GLint edgeSampling/*=GL_CLAMP_TO_EDGE*/ )
{
	glGenSamplers(1, &m_samplerId);
	setFiltering(filter);
	setEdgeSampling(edgeSampling);
	GLError::glCheckError(__func__);
}

void GLSamplerObject::setFiltering( GLint filter )
{
	glSamplerParameteri(m_samplerId, GL_TEXTURE_MAG_FILTER, filter);
	glSamplerParameteri(m_samplerId, GL_TEXTURE_MIN_FILTER, filter);
}

void GLSamplerObject::setEdgeSampling( GLint sampling )
{
	glSamplerParameteri(m_samplerId, GL_TEXTURE_WRAP_S, sampling);
	glSamplerParameteri(m_samplerId, GL_TEXTURE_WRAP_T, sampling);
}
GLSamplerObject::~GLSamplerObject(void)
{
	if (m_samplerId){
		glDeleteSamplers(1, &m_samplerId);
		m_samplerId = 0;
	}
}
void GLSamplerObject::bind(GLuint texUnitId)
{
	glBindSampler(texUnitId, m_samplerId);
	GLError::glCheckError(__func__);
}

void GLSamplerObject::unbind(GLuint texUnitId)
{
	glBindSampler(texUnitId, 0);
	GLError::glCheckError(__func__);
}

void GLSamplerObject::setAnisotropicFiltering( float val )
{
	glSamplerParameteri(m_samplerId, GL_TEXTURE_MAX_ANISOTROPY_EXT, val);
}

void GLSamplerObject::setComparisionMode( GLint comp_fun )
{
	glSamplerParameteri(m_samplerId, GL_TEXTURE_COMPARE_MODE ,GL_COMPARE_REF_TO_TEXTURE);
	glSamplerParameteri(m_samplerId, GL_TEXTURE_COMPARE_FUNC, comp_fun);
}


}//end of namespace lily
