#include <iostream>
#include <sstream>
#include "GLError.h"
#include "GLVertexBufferObject.h"

namespace lily{


GLVertexBufferObject::GLVertexBufferObject(GLenum usage/*=GL_STATIC_DRAW*/)
	:GLBufferObject(GL_ARRAY_BUFFER,usage),
	 m_vtxCount(0)
{
}

void GLVertexBufferObject::upload(size_t totalSizeInBytes, 
						size_t vtxCount, const GLvoid* data )
{
	m_vtxCount = vtxCount;
	GLBufferObject::upload(totalSizeInBytes,data);
}

GLVertexBufferObject::~GLVertexBufferObject()
{
	releaseBindingIndex();
	/*
    if(m_bindingIndex >= g_availBindingIndex.size())
        return;

    if(!g_availBindingIndex[m_bindingIndex])
        g_availBindingIndex[m_bindingIndex]=1;
    else{
        std::stringstream ss;
        ss << __func__
            <<": There is inconsistency at binding index. \nMaybe some of your GLVertexBufferObject creation does not use GLBufferObject::getNexAvaibleBindingIndex() to get available binding index."
            <<m_bindingIndex<<std::endl;
        std::string msg=ss.str();
        std::cerr << msg;
        GLError::ErrorMessage(msg);
    }
	*/
}

}