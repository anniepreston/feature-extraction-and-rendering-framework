#include "vec2f.h"
#include "vec2i.h"
#include "vec2d.h"
using namespace lily;

vec2d::vec2d( const vec2i& v )
{
	m_x = double(v.x());
    m_y = double(v.y());
}

vec2d& vec2d::operator=( const vec2i &v )
{
	m_x = double(v.x());
	m_y = double(v.y());
	return *this;
};

vec2d& lily::vec2d::operator=( const vec2f &v )
{
	m_x = double(v.x());
	m_y = double(v.y());
	return *this;
}
