#include "vec3d.h"
#include "vec3i.h"
#include "vec3f.h"
//using namespace mathtool;
namespace lily{

vec3d::vec3d( const vec3i& v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
}

vec3d::vec3d( const vec3f& v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
}

vec3d& lily::vec3d::operator=( const vec3i &v )
{
    m_x = (double)v.x();
    m_y = (double)v.y();
    m_z = (double)v.z();
    return *this;
}

}

