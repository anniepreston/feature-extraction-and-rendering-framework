#include "vec2i.h"
#include "vec2f.h"
//using namespace mathtool;
namespace lily{

vec2i::vec2i( const vec2f &v )
{
	m_x = (int)v.x();
	m_y = (int)v.y();
}

vec2i& lily::vec2i::operator=( const vec2f &v )
{
	m_x = (int)v.x();
	m_y = (int)v.y();
	return *this;
}

vec2i vec2i::index1D_to_index2D_uint64( size_t index1D,const vec2i &size2d )
{
	vec2i pxy;
	pxy.setY(index1D / size_t(size2d.y()));
	pxy.setX(index1D % size_t(size2d.x()));
	return pxy;
}

vec2i vec2i::index1D_to_index2D( int index1D,const vec2i &size2d )
{
	vec2i pxy;
	pxy.setY(index1D / size2d.y());
	pxy.setX(index1D % size2d.x());
	return pxy;
}

size_t vec2i::index2D_to_index1D(const vec2i &idx2D, const vec2i &size2D)
{
	size_t ret = size_t(idx2D.y()) * size2D.x() + idx2D.x();
	return ret;
}

}
