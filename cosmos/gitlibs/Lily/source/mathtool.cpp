#include "mathtool.h"
namespace lily{

vec2f fminf(const vec2f &a,const vec2f &b )
{
    return vec2f(fminf(a.x(),b.x()),
                 fminf(a.y(),b.y()));
}

vec2f fmaxf(const vec2f &a,const vec2f &b )
{
    return vec2f(fmaxf(a.x(),b.x()),
                 fmaxf(a.y(),b.y()));
}

vec3f fminf(const vec3f &a,const vec3f &b )
{
    return vec3f(fminf(a.x(),b.x()),
                 fminf(a.y(),b.y()),
                 fminf(a.z(),b.z()));
}

vec3f fmaxf(const vec3f &a,const vec3f &b )
{
    return vec3f(fmaxf(a.x(),b.x()),
                 fmaxf(a.y(),b.y()),
                 fmaxf(a.z(),b.z()));
}

vec3i mini(const vec3i &a,const vec3i &b )
{
    return vec3i(mini(a.x(),b.x()),
                 mini(a.y(),b.y()),
                 mini(a.z(),b.z()));
}

vec3i maxi(const vec3i &a,const vec3i &b )
{
    return vec3i(maxi(a.x(),b.x()),
                 maxi(a.y(),b.y()),
                 maxi(a.z(),b.z()));
}

vec3i minvi(const vec3i &a,const vec3i &b )
{
    return vec3i(min(a.x(),b.x()),
                 min(a.y(),b.y()),
                 min(a.z(),b.z()));
}

vec3i maxvi(const vec3i &a,const vec3i &b )
{
    return vec3i(max(a.x(),b.x()),
                 max(a.y(),b.y()),
                 max(a.z(),b.z()));
}
int int_compare(const void *pa,const void *pb)
{
    int *a = (int *)pa;
    int *b = (int *)pb;

    if (*a == *b) {
        return 0;
    } else {
        if (*a < *b)
            return -1;
        else
            return 1;
    }
}
}
