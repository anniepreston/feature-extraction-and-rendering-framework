#include "GLRenderBufferObject.h"

using namespace lily;

GLRenderbufferObject::GLRenderbufferObject(void)
    :m_width(10), m_height(10)
{
    glGenRenderbuffersEXT(1, &m_rboId);
    bind();
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, m_width, m_height);
    unbind();
}
GLRenderbufferObject::GLRenderbufferObject(GLsizei width, GLsizei height)
    :m_width(width), m_height(height)
{
    glGenRenderbuffersEXT(1, &m_rboId);
    bind();
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, m_width, m_height);
    unbind();
}

GLRenderbufferObject::~GLRenderbufferObject( void )
{
    deleteBuffer();
}

void GLRenderbufferObject::resize( GLsizei width, GLsizei height )
{
    //delete existing buffer, if it exists.
    deleteBuffer();
    m_width = width;
    m_height= height;
    glGenRenderbuffersEXT(1, &m_rboId);
    bind();
    glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, m_width, m_height);
    unbind();
}

void GLRenderbufferObject::bind()
{
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, m_rboId);
}

void GLRenderbufferObject::unbind()
{
    glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
}

GLuint GLRenderbufferObject::getBufferId() const
{
    return m_rboId;
}
void GLRenderbufferObject::deleteBuffer()
{
    if (m_rboId){
        glDeleteRenderbuffersEXT(1, &m_rboId);
        m_rboId = 0;
        m_width = 10;
        m_height= 10;
    }
}

GLsizei GLRenderbufferObject::getWidth() const
{
    return m_width;
}

GLsizei GLRenderbufferObject::getHeight() const
{
    return m_height;
}