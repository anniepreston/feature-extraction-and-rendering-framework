#include "ConfigManager.h"
//#include <helper_math.h> //Added since CUDA 5.0
using namespace lily;

ConfigManager::ConfigManager( const std::string& configFilePath )
    :m_configFilePath(configFilePath)
{
	m_ViewTranslate = vec3f(0.0f,0.0f,0.0f);
	m_ViewRotation = vec3f(0.0f,0.0f,0.0f);
	m_scale=1.f;
	m_bAxis = true;
	m_bDrawBBox = true;
	m_bCpuRender	= false;//
	m_blockSize		= vec3i(8,8,0);//dim3(16,16);//dim3(32,16);////dim3(12,12);//
	m_imageWidth = 512;
	m_imageHeight = 512;

	//load default setting
	QSettings settings(m_configFilePath.data(),QSettings::IniFormat);
	settings.beginGroup("Hardware");
	m_bCpuRender = settings.value("CPU").toBool();
	m_blockSize[0] = settings.value("BlockSizeX").toUInt();
	m_blockSize[1] = settings.value("BlockSizeY").toUInt();
	m_blockSize[2] = settings.value("BlockSizeZ").toUInt();
	settings.endGroup();

	settings.beginGroup("ViewSettings");
	m_imageWidth	         = settings.value("imageWidth").toInt();
	m_imageHeight	         = settings.value("imageHeight").toInt();
	m_freezeView	         = settings.value("freezeView").toBool();
	m_scale			         = settings.value("scale").toFloat();
	m_ViewRotation[0]         = settings.value("rotX").toFloat();
	m_ViewRotation[1]         = settings.value("rotY").toFloat();
	m_ViewTranslate[0]        = settings.value("transX").toFloat();
	m_ViewTranslate[1]        = settings.value("transY").toFloat();
	m_ViewTranslate[2]        = settings.value("transZ").toFloat();
	settings.endGroup();

	settings.beginGroup("GUI");
	m_bAxis			= settings.value("ShowAxis").toBool();
	m_bDrawBBox		= settings.value("ShowBoundingBox").toBool();
	settings.endGroup();

	settings.beginGroup("Lighting");
// 	m_lightParam.Kamb = settings.value("ambient").toFloat();
// 	m_lightParam.Kdif = settings.value("diffuse").toFloat();
// 	m_lightParam.Kspe = settings.value("specular").toFloat();
// 	m_lightParam.Kspe = settings.value("shininess").toFloat();
	settings.endGroup();

	settings.beginGroup("DataSettings");
    m_isomodelfile = settings.value("isomodel").toString().toLocal8Bit().constData();
	settings.endGroup();//end of data settings

    settings.endArray();

	settings.beginGroup("Environment");//begin environment

	int size = settings.beginReadArray("SkySphereImage");
	for(int i = 0; i < size ; ++i){
		settings.setArrayIndex(i);
		m_skySphereImgPath.push_back(std::string(settings.value("color").toString().toLocal8Bit().constData()));
	}
	settings.endArray();

	settings.endGroup();//end of environment
}

ConfigManager::~ConfigManager(void)
{
	//Save Setting.
	QSettings settings(m_configFilePath.data(),QSettings::IniFormat);
	settings.beginGroup("Hardware");
	settings.setValue("CPU",m_bCpuRender);
	settings.endGroup();

	settings.beginGroup("Algorithm");
	settings.endGroup();

	settings.beginGroup("GUI");
	settings.setValue("ShowAxis",m_bAxis);
	settings.setValue("ShowBoundingBox",m_bDrawBBox);
	settings.endGroup();

	settings.beginGroup("ViewSettings");
	settings.setValue("imageWidth",m_imageWidth);
	settings.setValue("imageHeight",m_imageHeight);
	settings.setValue("freezeView",m_freezeView);
	settings.setValue("scale",	(double)m_scale);
	settings.setValue("rotX",	(double)m_ViewRotation.x());
	settings.setValue("rotY",	(double)m_ViewRotation.y());
	settings.setValue("transX",	(double)m_ViewTranslate.x());
	settings.setValue("transY",	(double)m_ViewTranslate.y());
	settings.setValue("transZ",	(double)m_ViewTranslate.z());
	settings.endGroup();

	settings.beginGroup("Lighting");
// 	settings.setValue("ambient",	(double)m_lightParam.Kamb);
// 	settings.setValue("diffuse",	(double)m_lightParam.Kdif);
// 	settings.setValue("specular",	(double)m_lightParam.Kspe);
// 	settings.setValue("shininess",	(double)m_lightParam.Kspe);
	settings.endGroup();

	settings.beginGroup("DataSettings");
    settings.setValue("isomodel", QString(m_isomodelfile.data()) );
	settings.endGroup();

	settings.beginGroup("Environment");//begin environment

	settings.endGroup();//end of environment
}
