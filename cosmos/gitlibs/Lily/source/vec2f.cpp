#include "vec2f.h"
#include "vec2i.h"
using namespace lily;

vec2f::vec2f( const vec2i& v )
{
	m_x = (float)v.x(); m_y = (float)v.y();
}

vec2f& vec2f::operator=( const vec2i &v )
{
	m_x = (float)v.x();
	m_y = (float)v.y();
	return *this;
};
