//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-05-23
//UPDATED: 2015-05-23

#ifndef _GLATOMICCOUNTER_H_
#define _GLATOMICCOUNTER_H_
#include <memory>
#include <string>
#include "vec2f.h"
#include "vec3d.h"
#include "mat4.h"
#include "GLBufferObject.h"

namespace lily
{
	//reference
	//http://www.lighthouse3d.com/tutorials/opengl-atomic-counters/
	//
	class GLAtomicCounter : public GLBufferObject
	{
		public:
			GLAtomicCounter(const std::string& name = "Untitled Atomic Counter");
			//allocate nCounter of atomic counters with initV as initial value.
			void alloc(int nCounter, GLuint initV=0);
			void reset(GLuint initV = 0);
			//read the value of atomic counters which are stored in buffer ptr.
			//count: returns how many counter values are returned. No value will
			//be returned if count=NULL.
			void readCounters(GLuint* ptr, int* count=NULL);
			//map the address of counter storage buffer to host side.
			void* map();
			//unmap the address of counter storage buffer.
			void unmap();
			//return the number of atomic counters.
			int  size() const { return m_count; }
			void printLimitation();
		protected:
			void upload(size_t totalSizeInBytes, const GLvoid* data);
			void upload(size_t offset, size_t totalSizeInBytes, const GLvoid* data);
			void* map(GLenum usage);
		protected:
				std::string m_name;
				int m_count;//# of atomic counters
	};
	typedef std::shared_ptr < GLAtomicCounter >
		GLAtomicCounterRef;
}

#endif

