#ifndef _GL_CONTEXT_H_
#define _GL_CONTEXT_H_
#include "mat4.h"
#include <vector>
#include <stack>

namespace lily{

struct GLLights{
    vec4f m_pos;
    vec4f m_color;
    GLLights(const vec4f& pos  = vec4f(0.0f,0.0f,0.0f,1.0f),
             const vec4f& color= vec4f(1.0f,1.0f,1.0f,1.0f) )

        :m_pos(pos), m_color(color){}
};
class GLContext
{
public:
    typedef enum{GL_ModelViewMatrix, GL_ProjectionMatrix, GL_NormalMatrix} MatrixMode;

    GLContext(void);
    ~GLContext(void);
public:
    static mat4 g_MVM;//model view matrix
    static mat4 g_PjM;//projection matrix
    static mat3 g_NM;//normal matrix
    static std::vector<GLLights> g_lights;


    static void glMatrixMode(MatrixMode mm){g_matrixMode = mm;}
    static void glPushMatrix();
    static void glPopMatrix();
    
protected:
    static MatrixMode g_matrixMode;
    static std::stack<mat4> g_MVMstack;
    static std::stack<mat4> g_PjMstack;
    static std::stack<mat3> g_NMstack;
};

}
#endif
