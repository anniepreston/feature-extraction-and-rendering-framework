//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2014-02-15
//UPDATED: 2014-02-15
#ifndef _GLCOMPUTESHADER_H_
#define _GLCOMPUTESHADER_H_

#include <string>

#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif

#include "GLShader.h"

namespace lily{
class vec2f;
class vec3f;
class vec4f;
class vec3d;

class GLComputeShader: public GLShader{
public:
	GLComputeShader(const std::string& shaderName="Untitled Compute Shader", bool useCoreProfile=false);
	void setComputeShaderStr(std::string& compShaderStr){m_computeShaderProg = compShaderStr;}

	void UseShaders(int num_group_x, int num_group_y, int num_group_z);
	bool refresh();

	bool loadComputeShaderFile(const string& fileName);

	GLuint CreateShaders(void);
	void DestroyShaders(void);
private:
	void UseShaders();
	void loadVertexShaderFile(const std::string& fileName);
	void loadFragShaderFile(const std::string& fileName);
	void loadGeomShaderFile(const std::string& fileName);
	void setVertexShaderStr(std::string& vtxShaderStr);
	void setFragShaderStr(std::string& fragShaderStr);
	void setGeomShaderStr(std::string& geoShaderStr);
protected:
	GLuint      m_computeShaderId;
	std::string m_computeShaderProg;
	std::string m_computeShaderFileName;
};

typedef std::shared_ptr<GLComputeShader> GLComputeShaderRef;

}

#endif
