//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-04-16
//UPDATED: 2015-05-03

#ifndef _GLSHADERSTORAGEBUFFEROBJECT_H_
#define _GLSHADERSTORAGEBUFFEROBJECT_H_
#include <memory>
#include <string>
#include "vec2f.h"
#include "vec3d.h"
#include "mat4.h"
#include "GLBufferObject.h"

namespace lily
{
	//reference
	//http://www.geeks3d.com/20140704/tutorial-introduction-to-opengl-4-3-shader-storage-buffers-objects-ssbo-demo/
	//https://www.opengl.org/registry/specs/ARB/shader_storage_buffer_object.txt
	//https://www.opengl.org/wiki/Interface_Block_%28GLSL%29#Shader_binding_index_setting

	class GLShaderStorageBufferObject : public GLBufferObject
	{
		public:
			GLShaderStorageBufferObject(const std::string& name="Untitled SBBO", GLenum bufferUsage=GL_STATIC_DRAW);
	};
	typedef std::shared_ptr <GLShaderStorageBufferObject> GLShaderStorageBufferObjectRef;
}

#endif

