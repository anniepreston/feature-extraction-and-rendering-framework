//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2015-03-10
//UPDATED: 2015-05-03

#ifndef _GLUNIFORMBLOCKBUFFEROBJECT_H_
#define _GLUNIFORMBLOCKBUFFEROBJECT_H_
#include <memory>
#include <string>
#include "vec2f.h"
#include "vec3d.h"
#include "mat4.h"
#include "GLBufferObject.h"

namespace lily
{
	//reference
	//http://www.lighthouse3d.com/tutorials/glsl-core-tutorial/3490-2/
	//
	class GLUniformBlockBufferObject : public GLBufferObject
	{
		public:
			GLUniformBlockBufferObject( const std::string& name="Untitled UBBO", GLenum bufferUsage=GL_STATIC_DRAW);
	protected:
			std::string m_name;
	};
	typedef std::shared_ptr < GLUniformBlockBufferObject >
		GLUniformBlockBufferObjectRef;
}

#endif

