#ifndef _GL_CUBIC_HERMITE_SPLINE_H_
#define _GL_CUBIC_HERMITE_SPLINE_H_
#pragma once
#include "GLSpline.h"

class GLCubicHermiteSpline :
    public GLSpline
{
public:
    GLCubicHermiteSpline(void);
    ~GLCubicHermiteSpline(void);

    virtual void createCurve();
    virtual void draw(GLfloat lineWidth=1.0 );
	void enableDrawCurve(bool val){ m_drawSplineCurve = val;}
    void enableDrawTangentKnot(bool v){ m_drawTangentKnot = v;}
    int  whichTangentKnotSelected( const vec3f& mousePos, vec3f& retPt );
    void setSelTangentKnotIdx(unsigned int val) { m_selTanKnotIdx = val; }
    int  getSelTangentKnotIdx(){ return m_selTanKnotIdx;}
    std::vector<vec3f>& getTangentKnotArrayRef() { return m_tangentKnotArray; }
    void setDone(bool val){ m_done = val;}
    bool getDone() { return m_done ;}
	void clear();
    //Save curve path to designated file.
    //iomode: 0 : text format
    //ios::binary: binary format
    void save(const string& file, GLuint iomode=0) ;
	void save( ofstream& ofs, GLuint iomode/*=0*/);
	//Load curve path from designated file.
    //iomode: 0 : text format
    //ios::binary: binary format
    void load(const string& file, GLuint iomode=0);
	void load( ifstream& ifs, GLuint iomode/*=0*/ );
protected:
    void drawTangentKnot();
    std::vector<vec3f> m_tangentVecArray;
    std::vector<vec3f> m_tangentKnotArray;
    int m_selTanKnotIdx;
    bool m_done;
    bool m_drawTangentKnot;
	bool m_drawSplineCurve;
};

#endif

