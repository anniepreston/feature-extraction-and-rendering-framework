//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2013-10-23
//UPDATED: 2015-05-03

#ifndef _GL_ARRAY_BUFFER_OBJECT_H_
#define _GL_ARRAY_BUFFER_OBJECT_H_

#include <memory>
#include "GLBufferObject.h"

namespace lily{

class GLVertexBufferObject : public GLBufferObject
{
public:
	//GLVertexBufferObject is a linear buffer that stores the contents of vertex attributes.
	//usage:
	// GL_STATIC_DRAW_ARB,GL_STATIC_READ_ARB,GL_STATIC_COPY_ARB
	// GL_DYNAMIC_DRAW_ARB,GL_DYNAMIC_READ_ARB,GL_DYNAMIC_COPY_ARB
	// GL_STREAM_DRAW_ARB,GL_STREAM_READ_ARB,GL_STREAM_COPY_ARB
	GLVertexBufferObject(GLenum usage=GL_STATIC_DRAW);
	~GLVertexBufferObject();
	

	size_t getVertexCount() const { return m_vtxCount; }
	void   setVertexCount(size_t n){m_vtxCount = n;}
	//Upload the contents in its build-in array to GPU.
	void upload(size_t totalSizeInBytes, size_t vtxCount, const GLvoid* data);

private:
	size_t m_vtxCount;
};//end of namespace

typedef std::shared_ptr<GLVertexBufferObject> GLVertexBufferObjectRef;

}
#endif