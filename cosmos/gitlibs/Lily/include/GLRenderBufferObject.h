#ifndef _GLRENDERBUFFEROBJECT_H_
#define _GLRENDERBUFFEROBJECT_H_
#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif

#include <memory>

namespace lily{

    class GLRenderbufferObject
    {
        public:
            GLRenderbufferObject(void);
            GLRenderbufferObject(GLsizei width, GLsizei height);
            ~GLRenderbufferObject(void);
            void    resize(GLsizei width, GLsizei height);
            void    bind();
            void    unbind();
            GLuint  getBufferId() const;
            GLsizei getWidth() const;
            GLsizei getHeight() const;
        private:
            void deleteBuffer();
            GLuint m_rboId;
            GLsizei m_width, m_height;
    };

    #ifdef BOOST_REFERENCE_COUNT
    typedef boost::shared_ptr<GLRenderbufferObject> GLRenderbufferObjectRef;
    #else
    typedef std::shared_ptr<GLRenderbufferObject> GLRenderbufferObjectRef;
    #endif
}

#endif