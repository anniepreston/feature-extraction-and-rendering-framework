#ifndef _GLFPS_H_
#define _GLFPS_H_
#pragma once
#include <QTime>
#include <qgl.h>
#include <memory>

namespace lily{
    class GLFPS
    {
        public:
            GLFPS(void);
            ~GLFPS(void);
            void  startFPS();
            float endFPS();
            float showFPS(int scrWidth, int scrHeight, QGLWidget *widget=NULL);
            void  setFont(const QFont& f);
            QFont getFont() const { return m_font;}
            void  setColor(const QColor& c);
            QColor getColor() const {return m_color;}
        private:
            QTime m_timer;
            int   m_start;
            QFont m_font;
            QColor m_color;
    };
	typedef std::shared_ptr<GLFPS> GLFPSRef;
}
#endif