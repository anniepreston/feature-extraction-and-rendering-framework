#ifndef _GL_ERROR_H_
#define  _GL_ERROR_H_
#pragma once
#include <string>
#include <QString>
#include <QWidget>
#include <DError.h>

namespace lily{
#define PRINT_GL_CAPABILITY(name, val){\
		glGetIntegerv((name), &(val));\
		printf(#name"=%d\n", (val));\
	}
class GLError
{
public:
	GLError(void);
	~GLError(void);
	static	void glCheckError(const std::string& call);
	static	void glCheckError(const char* call);
    //Clean current GL error status.
    static  void purgePreviousGLError();
	static  void ErrorMessage(const std::string &msg);
	static  void ErrorMessage(const QString &msg);
	static  void WarningMessage( const QString &msg, QWidget* parent=NULL );
};

}
#endif
