//AUTHOR: Jinrong Xie (stonexjr at gmail.com)
//CREATED: 2014-03-03
//UPDATED: 2014-04-30
#ifndef _GL_UTILITIES_H_
#define  _GL_UTILITIES_H_
#pragma once
#include <unordered_map>
#include <QImage>
#include "GLTexture2D.h"
#include "constant.h"
#include "vec2d.h"
#include "mat4.h"
#include "BBox2D.h"
namespace lily{
	class BBox;
	class vec4i;

	class GLUtilities
	{
		public:
			GLUtilities(void);
			~GLUtilities(void);
		static bool checkGLExtension(std::string extension_string);
		static void init();
		//begin 2D setting, origin of the coordinate system is at the lower left corner
		//of the screen by default.x-axis is pointing to the right , y-axis is pointing upwards.
		static void glBegin2D(int scr_width, int scr_height);
		static void glEnd2D();
		//begin 2D setting, origin of the coordinate system is at the center
		//of the screen by default. x-axis is pointing to the right , y-axis is
		//pointing upwards.
		static void glBegin2DCoords(int scrWidth, int scrHeight);
		static void glEnd2DCoords();

		static void glBegin3D(int scr_width, int scr_height);
		static void glEnd3D();

		static void glDrawQuad2Di(lily::BBox2D& bbox, bool fill);
		static void glDrawQuad2Di(int xstart, int ystart, int width, int height,bool fill);
		static void glDrawQuad2Df(lily::BBox2D& bbox,bool fill );
		static void glDrawQuad2Df( float xstart, float ystart, float width, float height,bool  fill );
		static void glDrawQuad2Df( float xstart, float ystart, float width, float height, float angle );
		static void glDrawWedge2Df( float xstart, float ystart, float width, float height, float skew_ratio, bool bFill  );
		static void glDrawQuadBeveled2Df( float xstart, float ystart, float width, float height, float bevel_ratio, bool bFill );
		static void glDrawBackgroundGradient(float w, float h,const float start[4],const float end[4]);
		//b: bounding box you want to draw
		//option: 0: wireframe model
		//        1: filled model
		static void freeResource();
		//return a pointer to the sub-block of of current frame buffer.
		//(x,y) is the frame buffer coordinates where x-axis points to the right
		//and y-axis points upwards.
		//fileName: full path name of image file to which the frame buffer is saved.
		//whichBuffer: Indicate which buffer to grab. By default it is GL_BACK
		//If framebuffer object is used, whichBuffer should refer to one of the
		//color attachment, i.e. GL_COLORATTACHMENT_0/1/2....
		static void grabFrameBuffer(int x, int y, int width, int height, 
									const std::string& fileName,
									GLenum whichBuffer=GL_BACK,
									GLenum pixelFormat=GL_BGRA,
									GLenum pixelType=GL_UNSIGNED_BYTE);

		static QImage grabFrameBuffer( int x, int y, int width, int height, 
									GLenum whichBuffer=GL_BACK,
									GLenum pixelFormat=GL_BGRA,
									GLenum pixelType=GL_UNSIGNED_BYTE);

		static void glGetViewPort(vec4i& viewport);

		//Same as gluUnProject() to get the object coordinates that correspond to those window coordinates
		//srcXY: screen coordinates directly returned by local windows system, where origin is at the top left corner.
		//Return: output parameter, storing the transformed coordinates in object space.
		static vec2d PlaneUnproject(const lily::vec2d& scrXY);
		static vec3f PlaneUnproject(const lily::vec3f& scrXYZ, const lily::mat4 &modelViewMatrix, const lily::mat4 projMatrix, const int viewport[4]);
		static vec3f unProject(const vec3i& srcXYZ, const mat4&  modelViewMtx, const mat4&  projMtx);

		typedef void (*drawCallBack)(void* params);
		//Apply supersampling to the renderCallBack.
		//qualityLevel: int value ranges from 1(equivalent to no supersampling) 
		//to 8 (highest quality).
		static void glSupersampling(drawCallBack renderCallBack, void* params, int qualityLevel=1);

		static float m_near_height;
		static float m_zNear;
		static float m_zFar;
		static float m_fov;
		static GLTexture2DRef m_bgTextureRef;
		static std::unordered_map<GLint, std::string>   m_openglInt2TypeName;
		static std::unordered_map< std::string, GLint> m_openglTypeName2Int;
	};

};
#endif