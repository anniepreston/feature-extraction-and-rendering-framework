#ifndef _CONFIGER_MANAGER_H_
#define  _CONFIGER_MANAGER_H_
#pragma once
#include <string>
#include <vector>
#include "vec3f.h"
#include "vec3i.h"
#include <QSettings>

class ConfigManager
{
public:
	explicit ConfigManager(const std::string& configFilePath);
	~ConfigManager(void);
    std::string m_configFilePath;
	bool m_bCpuRender;
	lily::vec3i m_blockSize;

	int	 m_imageWidth;
	int	 m_imageHeight;
	bool m_freezeView;
	float m_scale;
	lily::vec3f m_ViewRotation;
	lily::vec3f m_ViewTranslate;
	bool m_bAxis;
	bool m_bDrawBBox;

    std::string m_isomodelfile;
    std::vector<std::string> m_skySphereImgPath;

    //	LIGHTPARAM	m_lightParam;
};
#endif