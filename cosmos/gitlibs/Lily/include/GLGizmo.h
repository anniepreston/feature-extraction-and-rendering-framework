#ifndef _GL_GIZMO_H_
#define  _GL_GIZMO_H_
#pragma once

#if defined(__APPLE__) || defined(MACOSX)
#include <gl.h>
#include <gl3.h>
#include <OpenGL.h>
#else
#include <GL/glew.h>
#endif

namespace lily{
class BBox;
class BBox2D;

    class GLGizmo
    {
        public:
        GLGizmo(void);
        ~GLGizmo(void);

        static void glDrawAxis(float length=2, float lineWidth=1.0f,
				const char* xLable="X",const char* yLabel="Y",const char* zLabel="Z");

        static void glDrawGrid(float size=10.0f, float step=1.0f);
        //b: bounding box you want to draw
        //option: 0: wireframe model
        //        1: filled model
        static void glDrawBoundingBox(const lily::BBox& b, const GLenum fillmode=GL_LINE); 
        static void glDrawBoundingBox(const lily::BBox2D& b, const GLenum fillmode=GL_LINE); 
        static void glDrawBoundingBox2(const lily::BBox& b, const unsigned int fillmode,const int option=0);

    };
};
#endif