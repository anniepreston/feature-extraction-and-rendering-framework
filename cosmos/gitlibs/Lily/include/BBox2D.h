#ifndef _BBOX2D_H_
#define _BBOX2D_H_

#pragma once
#include "vec2f.h"
#include "constant.h"

namespace lily{

class BBox2D{
public:
    vec2f pMin;
    vec2f pMax;
    //float  m_dist
    BBox2D(void);
    BBox2D(const vec2f &p) : pMin(p), pMax(p) { }
    //c: center of the bbox,
    //r: radius of the bbox.
    BBox2D(const vec2f& c, float r);
    //lower and upper corner of the bbox.
    BBox2D(const vec2f& p1, const vec2f& p2);
    BBox2D Union(const vec2f& pt) const;
    BBox2D Union(const BBox2D& box) const;
    vec2f getDimension() const;
    bool Overlap(const vec2f& p ) const;
    bool Overlap(const BBox2D& box) const;
    bool Overlap(const BBox2D& box, float tolerance) const;
    //if a point is inside the box.
    bool Inside(const vec2f& pt) const;
    bool Inside(const BBox2D& bbox) const;
    void Expand(float delta);
    float SurfaceArea() const;
    int   MaximumExtent()  const;
    float MaximumExtentf() const;
    vec2f LerpPt(float tx, float ty) const;
    //returns the position of a point relative to the corners of the box,
    //where a point at the minimum corner has offset (0, 0, 0), a point at the maximum corner
    //has offset (1, 1, 1).
    vec2f Offset(const vec2f& p) const;
    vec2f Center() const;
    //Radius() only make sense when the box is assumed as cube.
    float BoxRadius() const
    {
        return MaximumExtentf()*0.5f;
    }

    float SphericalRadius() const
    {
        return BoxRadius() * SQRT_2;
    }

    friend BBox2D& operator << (BBox2D& bbox, const vec2f& v)
    {
        bbox = bbox.Union(v);
        return bbox;
    }

    friend BBox2D& operator << (BBox2D& bbox, const BBox2D& bbox2)
    {
        bbox = bbox.Union(bbox2);
        return bbox;
    }

    friend std::ostream& operator << (std::ostream& os,const BBox2D& b)
    {
        os<<"["<<b.pMin<<", "<<b.pMax<<"]\n";
        return os;
    }

};

};//end of namespace
#endif

