#version 420

layout(location = 0) in vec4 deflt;
layout(location = 1) in vec4 lo1;
layout(location = 2) in vec4 lo2;
layout(location = 3) in vec4 lmvir;
layout(location = 4) in vec4 vol;
layout(location = 5) in vec4 mvir;


uniform sampler2D p1;

uniform int am;
uniform int goff;
uniform int coff;

out VertexData {
    vec2 texCoord;
    vec3 normal;
} VertexOut;

void main()
{

//(float(mergdata[i][j].id) - float(minx)) /(float(maxx) -  float(minx));

    float min = -0.7;//-2.9;
    float max = 0.8;
    float t = 0;
    vec4 bah1 = vec4(0,0,0,0);
// set color (alpha for now)
    switch(coff){
    case 0:
     t = (deflt.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    case 1:
     t = (lo1.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    case 2:
     t = (lo2.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    case 3:
     t = (lmvir.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    case 4:
     t = (vol.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    case 5:
     t = (mvir.y - min)/ (max - min);
   bah1 =  texture(p1,vec2(t,0));

    VertexOut.texCoord = vec2(bah1.a,0);
    VertexOut.normal = bah1.xyz;
    break;

    }

    // set position
    switch(goff){
    case 0:
    gl_Position = deflt;
    break;
    case 1:
    gl_Position = lo1;
    break;
    case 2:
    gl_Position = lo2;
    break;
    case 3:
    gl_Position = lmvir;
    break;
    case 4:
    gl_Position = vol;
    break;
    case 5:
    gl_Position = mvir;
    break;
}
    }
