
#include "Axis.h"





View::View(const QString &name, QWidget *parent)
    : QWidget(parent)
{
    xmin = -1;
    xmax = 1;

    ymin = -1;
    ymax = 1;

    lxmin = -1;
    lxmax = 1;

    lymin = -1;
    lymax = 1;

     smin = -0.7;//-2.9;
     smax = 0.8;
     lable = "Axis (Units)";
 fontsize = 10.0f;

}

void View::onnewlab(QString &lab){

    lable = lab;
    repaint();
}

void View::onnewfontsize(float f){

    fontsize = f;
    repaint();
}


void View::resetView()
{

}

void messtr(QString &t){
    // add leading 0's
    if(t.length() < 2){
      t = "00"+t;
    }

    else if(t.length() < 3){
      t = "0"+t;
    }
    return;
}

void View::onNewDataset(QSet<int> &test){

    cset = test;
    repaint();


    bah.clear();
    foreach (const int &value, cset){
        QString t = QString::number(value);
        // add leading 0's
    messtr(t);

         bah.insert(bah.begin(),t);
    }
    bah.sort();

}

/*
void View::onNewData(std::vector<Series *> &test){

    series = test;
    repaint();
}
*/

void View::onnewmouse(float x, float y){

    mousex = x;
    mousey = y;
    repaint();
}

void View::onnewllimit(float min, float max){

    lxmin = min;
    lxmax = max;
    repaint();
}

void View::onnewlimit(float min, float max){

    xmin = min;
    xmax = max;
    repaint();
}


void View::setResetButtonEnabled()
{

}

void View::setupMatrix()
{

}

void View::togglePointerMode()
{

}

void View::toggleOpenGL()
{

}

void View::toggleAntialiasing()
{

}

void View::print()
{

}

void View::zoomIn(int level)
{

}

void View::zoomOut(int level)
{

}

void View::rotateLeft()
{

}

void View::rotateRight()
{

}

void View::paintEvent(QPaintEvent *event)
{
  QPainter painter;
  painter.begin(this);
painter.setRenderHint(QPainter::Antialiasing);

  QPen redline(QColor(139,0,0));
  QPen bline(QColor(0,0,0,128));
  QPen buline(QColor(0,0,139));
 QPen cross(QColor(0,0,0));




 QPen wpen(QColor(255,255,255,245));

 //QBrush bkg(QColor(83,83,83));

 QBrush bkg(Qt::transparent);


 //wpen.setStyle();
  int wh = this->height();
  int ww = this->width();
/*
  QLinearGradient gradient(0,0,0,wh*.09);
  gradient.setSpread(QGradient::RepeatSpread);
     gradient.setColorAt(0, QColor(80, 80, 80,230));
      gradient.setColorAt(.4, QColor(0, 0, 0,230));
          gradient.setColorAt(1, QColor(80, 80, 80,230));
*/

  if(ori != 0){ // not 0 horizontal

  redline.setWidthF(.01*wh);
  buline.setWidthF(.01*wh);
    bline.setWidthF(.007*wh);
}
  else{  // vertical
      redline.setWidthF(.01*ww);
      buline.setWidthF(.01*ww);
        bline.setWidthF(.007*ww);
  }

// draw bg
  painter.fillRect(0,0,ww,wh,bkg);


// draw main axis
  // painter.setPen(redline);
  // painter.drawLine(0,lineh,ww*mult,lineh);

if(ori !=0){   // horizontal
    smin = -0.9f;
    smax = 0.9f;
   painter.setPen(wpen);
   painter.drawLine(0,wh*.1,ww,wh*.1);

// draw slider
   painter.setPen(redline);
   painter.drawLine(mousex*ww,wh*.1,mousex*ww,0);

   //draw ticks
                    painter.setPen(wpen);
                    QFont font = painter.font();
                   // font.setPointSizeF(.15*wh);
                    font.setPointSizeF(fontsize);
                    painter.setFont(font);

                   // painter.drawText(QPoint(mousex*ww,.85*wh),QString::number(lerp(mousex,xmin,xmax)),'g',2);
                    float min = terpi(smin,xmin,xmax);
                    float max = terpi(smax,xmin,xmax);

                    if( min*ww >= 0.0f && min*ww  <= ww){ //set min
                        min = min;
                    }
                    else{ // min out of bounds
                        min = 0.0f;
                    }


                    if( max*ww >= 0.0f && max*ww  <= ww){ //set max
                        max = max;
                    }
                    else{ // min out of bounds
                        max = 1.0f;
                    }

                    // how many ticks do we draw?

                   float numtick = (max - min)*ww/(fontsize);
                   numtick = numtick/2;

                   int n = (int)numtick;
//qDebug() << n << " " << numtick ;
                   for(int i = 0; i < n; i++){ // ok draw them
                       float pos = min*ww + float(i)*(fontsize)*2.0;
                           pos = pos/ww;
                       float t = lerp((1.0f - pos),xmin,xmax);

//qDebug() << "pos " << pos << "min " << min <<"max " << max ;

                       t = (t - smin)*(lxmax-lxmin)/(smax -smin) + lxmin;

                        painter.drawLine(ww*pos,0.1*wh,ww*pos,0.2*wh);

                        painter.save();
                                       painter.translate(pos*ww,0.85*wh);
                                       painter.rotate(-90);

               painter.drawText(QPoint(0,0),QString::number(t,'g',2));
                painter.restore();
}
               // draw lable
             //  painter.save();
              //  painter.translate(.01*ww,0.7*wh);
              //  painter.rotate(-90);

                painter.drawText(QRect(QPoint(0.0*ww,0.70*wh),QPoint(ww,wh)),Qt::AlignHCenter, lable);
             //   painter.restore();
               // painter.rotate(90);




//(mousey - .85)*(lmax - lmin)/(.10-.85) +lmin
// ( t - spacemin)*(lmax -lmin)/(smax - smin) + lmin

                 //   painter.drawText(QPoint(0.01*ww,mousey*wh),QString::number(lerp((1.0f - mousey),xmin,xmax)));

      float t = lerp((1.0f - mousex),xmin,xmax);


      t = (t - smin)*(lxmax-lxmin)/(smax -smin) + lxmin;
painter.drawText(QPoint(mousex*ww,.80*wh),QString::number(t));

}
else { // vertical
    painter.setPen(wpen);
    painter.drawLine(ww*.9,0,ww*.9,wh);


    // draw slider
       painter.setPen(redline);
       painter.drawLine(ww*.9,wh*mousey,ww,wh*mousey);

       //draw ticks
                        painter.setPen(wpen);
                        QFont font = painter.font();
                       // font.setPointSizeF(.025*wh);
                        font.setPointSizeF(fontsize);
                        painter.setFont(font);


                        float min = terpi(smin,xmin,xmax);
                        float max = terpi(smax,xmin,xmax);

                        if( min*wh >= 0.0f && min*wh  <= wh){ //set min
                            min = min;
                        }
                        else{ // min out of bounds
                            min = 0.0f;
                        }


                        if( max*wh >= 0.0f && max*wh  <= wh){ //set max
                            max = max;
                        }
                        else{ // min out of bounds
                            max = 1.0f;
                        }

                        // how many ticks do we draw?

                       float numtick = (max - min)*wh/(fontsize);
                       numtick = numtick/2;

                       int n = (int)numtick;
 //qDebug() << n << " " << numtick ;
                       for(int i = 0; i < n; i++){ // ok draw them
                           float pos = min*wh + float(i)*(fontsize)*2.0;
                               pos = pos/wh;
                           float t = lerp((1.0f - pos),xmin,xmax);

//qDebug() << "pos " << pos << "min " << min <<"max " << max ;

                           t = (t - smin)*(lxmax-lxmin)/(smax -smin) + lxmin;

                            painter.drawLine(ww*.8,pos*wh,ww*.9,pos*wh);
                   painter.drawText(QPoint(0.15*ww,pos*wh),QString::number(t,'g',2));
}
                   // draw lable
                   painter.save();
                    painter.translate(.01*ww,0.7*wh);
                    painter.rotate(-90);

                    painter.drawText(QRect(QPoint(0,0),QPoint(ww,wh)),Qt::AlignHCenter, lable);
                    painter.restore();
                   // painter.rotate(90);




//(mousey - .85)*(lmax - lmin)/(.10-.85) +lmin
// ( t - spacemin)*(lmax -lmin)/(smax - smin) + lmin

                     //   painter.drawText(QPoint(0.01*ww,mousey*wh),QString::number(lerp((1.0f - mousey),xmin,xmax)));

          float t = lerp((1.0f - mousey),xmin,xmax);


          t = (t - smin)*(lxmax-lxmin)/(smax -smin) + lxmin;
  painter.drawText(QPoint(0.01*ww,mousey*wh),QString::number(t));



}


//painter.setPen(bline);

   //    painter.drawLine(mousex*ww,lineh2,mousex*ww,lineh);









}

void View::setOrientation(int ori)
{
    this->ori = ori;
    repaint();
}

void View::mousePressEvent(QMouseEvent *event)
{
mousex = float(float(event->x())/float(this->width()));
mousey = float(float(event->y())/float(this->height()));
 repaint();

}

void View::mouseMoveEvent(QMouseEvent *event)
{
  mousex = float(float(event->x())/float(this->width()));
    mousey = float(float(event->y())/float(this->height()));
 repaint();
}

void View::mouseReleaseEvent(QMouseEvent *event)
{

}

