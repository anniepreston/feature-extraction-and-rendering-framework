#ifndef SCIVISDATA_H
#define SCIVISDATA_H
#include <iostream>
#include <fstream>

struct SciVisParticles{
	int i;
	float x, y, z;
	float vx, vy, vz;
	float dot, variance;
	SciVisParticles(int _i = -1,
		float _x = 0.0f, float _y = 0.0f, float _z = 0.0f,
		float _vx = 0.0f, float _vy = 0.0f, float _vz = 0.0f,
		float _dot = 0.0f, float _variance = 0.0f) :
		i(_i), x(_x), y(_y), z(_z), vx(_vx), vy(_vy), vz(_vz), dot(_dot), variance(_variance)
	{
	}
	friend std::ostream& operator << (std::ostream& os, const SciVisParticles& p){
		os << p.i << " " << p.x << " " << p.y << " " << p.z << " "
			<< p.vx << " " << p.vy << " " << p.vz << " "
			<< p.dot << " " << p.variance << std::endl;
		return os;
	}

	bool operator != (const SciVisParticles& p){
		return i != p.i || x != p.x || y != p.y || z != p.z ||
			vx != p.vx || vy != p.vy || vz != p.vz || dot != p.dot || variance != p.variance;
	}
};

#endif