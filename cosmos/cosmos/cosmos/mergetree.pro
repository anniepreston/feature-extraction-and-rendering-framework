RESOURCES += \
    shaders.qrc \
    filename.qrc


HEADERS += mainwindow.h \
    pc.h \
    selectionbox.h \
    TF.h \
    TFEditor.h \
    Axis.h
SOURCES += main.cpp \
    pc.cpp \
    selectionbox.cpp \
    TF.cpp \
    TFEditor.cpp \
    Axis.cpp

SOURCES += mainwindow.cpp

QT += concurrent widgets
qtHaveModule(printsupport): QT += printsupport
qtHaveModule(opengl): QT += opengl


build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/graphicsview/chip
INSTALLS += target

DISTFILES +=





