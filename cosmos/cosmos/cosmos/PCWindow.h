
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLFunctions_4_2_Compatibility>
//#include <QGLFormat>
#include <QWidget>
#include <QMenuBar>
#include <QProgressBar>
#include <QStatusBar>
#include "TFEditor.h"

using namespace vv;

QT_BEGIN_NAMESPACE
//class QGraphicsScene;
class QSplitter;
class QProgressbar;
QT_END_NAMESPACE

class PCWindow : public QWidget // QWidget //QMainWindow
{
	Q_OBJECT
public:
	PCWindow(QWidget *parent);

	//QWidget *parent =0);//QMainWindow *parent = 0);

	//Controls *c;

private:
	void setupMatrix();
	void populateScene();

	//QGraphicsScene *scene;
	QRgb m_color;
	QSplitter *h1Splitter;
	QSplitter *h2Splitter;
};

#endif // MAINWINDOW_H