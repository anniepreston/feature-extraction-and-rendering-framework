#include "pc.h"
#include "selectionbox.h"



selectionbox::selectionbox(QWidget *parent) : QWidget(parent) {
	//QRubberBand(QRubberBand::Rectangle,parent) {
	//tell QSizeGrip to resize this widget instead of top-level window
	setWindowFlags(Qt::SubWindow);

	setMouseTracking(1);

	// setWindowFlags(Qt::ToolTip);
	// setAttribute(Qt::WA_TranslucentBackground,true);

	// setAttribute(Qt::WA_TranslucentBackground);
	// setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint );
	//setWindowOpacity(0.5);

	Resizepressed = -1;
	movepressed = 0;

	/*
	QHBoxLayout* layout = new QHBoxLayout(this);
	//  QGridLayout* layout = new QGridLayout(this);
	layout->setContentsMargins(0, 0, 0, 0);

	//this->setStyleSheet("QHBoxLayout {  background-color: red;  border-style: outset;          border-width: 2px;                    border-radius: 10px;                    border-color: beige;                   font: bold 14px;                    min-width: 10em;                    padding: 6px;                }"                    );
	QSizeGrip* grip1 = new QSizeGrip(this);
	QSizeGrip* grip2 = new QSizeGrip(this);
	/*   QSizeGrip* grip3 = new QSizeGrip(this);
	QSizeGrip* grip4 = new QSizeGrip(this);
	QSizeGrip* grip5 = new QSizeGrip(this);
	QSizeGrip* grip6 = new QSizeGrip(this);

	*/
	/*
	layout->addWidget(grip1, 0, Qt::AlignLeft | Qt::AlignBottom);
	layout->addWidget(grip2, 0, Qt::AlignRight | Qt::AlignBottom);
	/*
	layout->addWidget(grip3, 0, Qt::AlignRight | Qt::AlignVCenter);
	layout->addWidget(grip4, 0, Qt::AlignLeft | Qt::AlignVCenter);
	layout->addWidget(grip5, 0, Qt::AlignTop | Qt::AlignHCenter);
	layout->addWidget(grip6, 0, Qt::AlignBottom | Qt::AlignHCenter);
	*/


	//setAttribute(Qt::WA_NoSystemBackground);
	// setStyleSheet("background-color: transparent");

	// rubberband = new QRubberBand(QRubberBand::Rectangle, this);
	// rubberband->move(0, 0);
	// rubberband->show();
	// show();
}

/*
void selectionbox::resizeEvent(QResizeEvent *) {
// rubberband->resize(size());
update();
}
*/

void selectionbox::draw(QPainter &painter)
{
	//Q_UNUSED(event);


	QPen pen = QPen(bgcolor);
	pen.setWidth(5);
	pen.setStyle(Qt::DashLine);



	QBrush brush = QBrush(bgcolor);

	setAttribute(Qt::WA_OpaquePaintEvent);

	//painter.begin((QWidget*)this->parent());
	//painter.fillRect(0,0,width(),height(),brush);
	painter.eraseRect(this->geometry());
	painter.setPen(pen);
	painter.setOpacity(0.3);
	painter.setBrush(brush);
	painter.drawRect(this->geometry());

	//painter.end();
}


/*
void selectionbox::paintEvent(QPaintEvent *event)
{
Q_UNUSED(event);


/*
QPainter painter;
QPen pen = QPen(QColor(255,0,0,255));
pen.setWidth(5);
pen.setStyle(Qt::DashLine);

QBrush brush = QBrush(QColor(255,0,0,255));

//

painter.begin(this);
painter.setPen(pen);
painter.setOpacity(0.5);
painter.setBrush(brush);
painter.drawRect(event->rect());
painter.end();
*/
// do nothing
//}

void selectionbox::mousePressEvent(QMouseEvent *e)
{


	if (this->geometry().contains(this->geometry().topLeft() + e->pos()) && (e->buttons() & Qt::MiddleButton))
	{

		//  QRect r=this->geometry();



		//   r.moveCenter(r.center() + e->pos());
		//   this->setGeometry(r);
		//  this->move(r.topLeft() + e->pos());
		selection_offset = e->pos();
		movepressed = true;
		this->setCursor(Qt::SizeAllCursor);

	}
	else if (e->buttons() & Qt::LeftButton){
		selection_offset = e->pos();

		if (e->pos().x() >= 0 && e->pos().x() <= 0.05f*float(width())){// handle left
			Resizepressed = 0;

		}
		else if (e->pos().x() >= 0.95f*width() && e->pos().x() <= 1.0f*float(width())){// handle right
			Resizepressed = 1;
		}
		else if (e->pos().y() >= 0 && e->pos().y() <= 0.05f*float(height())) { // handle top
			Resizepressed = 2;
		}

		else if (e->pos().y() >= 0.95f*height() && e->pos().y() <= 1.0f*float(height())) { // handle  bottom
			Resizepressed = 3;
		}

		else{
			//do nothing
		}
	}
	else{
		// do nothing

	}
}

void selectionbox::mouseMoveEvent(QMouseEvent *e)
{
	if (movepressed)
	{
		QRect r = this->geometry();
		r.moveCenter(r.center() + e->pos() - selection_offset);
		this->setGeometry(r);
		//this->move(r.topLeft() + e->pos());
		//emit this->sendupdate();

	}
	else if (Resizepressed > -1){ // handle resize
		QRect r = this->geometry();
		qDebug() << e->pos() << " " << Resizepressed;
		switch (Resizepressed){

		case 0: // left
			if (e->pos().x() < width())
				r.setLeft(r.left() + e->pos().x() - selection_offset.x());
			break;

		case 1: // right
			if (e->pos().x() > 0)
				r.setRight(r.right() + e->pos().x() - selection_offset.x());
			selection_offset = e->pos();
			break;

		case 2: //top
			if (e->pos().y() < height())
				r.setTop(r.top() + e->pos().y() - selection_offset.y());
			break;

		case 3: //bottom
			if (e->pos().y() > 0)
				r.setBottom(r.bottom() + e->pos().y() - selection_offset.y());
			selection_offset = e->pos();
			break;

		}
		this->setGeometry(r);
		// emit this->sendupdate();

	}

	// show resize curser
	else if ((e->pos().x() >= 0 && e->pos().x() <= 0.05f*float(width())) || (e->pos().x() >= 0.95f*width() && e->pos().x() <= 1.05f*float(width()))){
		this->setCursor(Qt::SizeHorCursor);
	}

	else if ((e->pos().y() >= 0 && e->pos().y() <= 0.05f*float(height())) || (e->pos().y() >= 0.95f*height() && e->pos().y() <= 1.05f*float(height()))){
		this->setCursor(Qt::SizeVerCursor);
	}

	else{
		// restore curser
		this->setCursor(Qt::ArrowCursor);
	}

	((PC*)(this->parent()))->repaint();

}

void selectionbox::mouseReleaseEvent(QMouseEvent *e)
{
	if (movepressed){
		this->setCursor(Qt::ArrowCursor);
		emit this->sendupdate();
		movepressed = false;
	}

	if (Resizepressed > -1){
		emit this->sendupdate();
		Resizepressed = -1;
	}
}

void selectionbox::setbgcolor(QColor color)
{
	bgcolor = color;

}