
#ifndef PCMAINWINDOW_H
#define PCMAINWINDOW_H

//#include <QMainWindow>
#include <QOpenGLFunctions_4_2_Core>
#include <QOpenGLFunctions_4_2_Compatibility>
//#include <QGLFormat>
#include <QWidget>
#include <QMenuBar>
#include <QProgressBar>
#include <QStatusBar>
#include "TFEditor.h"

using namespace vv;

QT_BEGIN_NAMESPACE
//class QGraphicsScene;
class QSplitter;
class QProgressbar;
QT_END_NAMESPACE

class PCMainWindow : public QWidget //QMainWindow
{
    Q_OBJECT
public:
    PCMainWindow(QWidget *parent = 0);
    typedef std::vector<int> intList;
    //QWidget *parent =0);//QMainWindow *parent = 0);

    //Controls *c;

public slots:
    void sendNewAidsMW(intList);
    void particles_received(intList);

private:
    void setupMatrix();
    void populateScene();

    //QGraphicsScene *scene;
    QRgb m_color;
    QSplitter *h1Splitter;
    QSplitter *h2Splitter;

signals:
    void newAidsLoadedMW(intList);
    void send_particles_to_pc(intList);

};

#endif // PCMAINWINDOW_H
