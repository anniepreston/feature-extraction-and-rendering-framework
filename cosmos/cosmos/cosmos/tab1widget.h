#ifndef TAB1WIDGET_H
#define TAB1WIDGET_H

#include <QWidget>
#include "ui_tabonewidget.h"

class Tab1Widget : public QWidget
{
    Q_OBJECT

public:
    Tab1Widget(QWidget *parent = 0);
    ~Tab1Widget();

private:
    Ui::tabonewidget ui;
};

%NAMESPACE_END%#endif // TAB1WIDGET_H
