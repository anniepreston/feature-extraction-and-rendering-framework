
#include "mtcontrol.h"





MTC::MTC(QWidget *parent)
	: QWidget(parent)
{

	//fontsize = 10.0f;

	QLabel * pr = new QLabel(QString("Selection Controls"));
	QVBoxLayout *  outlayout = new QVBoxLayout;

	QHBoxLayout *stLayout = new QHBoxLayout;
	loadstbt = new QToolButton;
	savestbt = new QToolButton;

	bgbt = new QToolButton;

	loadstbt->setText(tr("Clear"));
	connect(loadstbt, SIGNAL(clicked()), this, SLOT(clearsel()));
	savestbt->setText(tr("Show"));
	savestbt->setCheckable(true);
	savestbt->setChecked(false);
	connect(savestbt, SIGNAL(toggled(bool)), this, SLOT(finalizesel(bool)));

	stLayout->addWidget(savestbt);
	stLayout->addWidget(loadstbt);



	outlayout->addWidget(pr);
	outlayout->addLayout(stLayout);

	bgbt->setText(tr("Hide Unselected"));
	bgbt->setCheckable(true);
	bgbt->setChecked(true);
	connect(bgbt, SIGNAL(toggled(bool)), this, SLOT(setbg(bool)));
	outlayout->addWidget(bgbt);
	setLayout(outlayout);

}

void MTC::clearsel(){
	emit this->sendclear();
}

void MTC::finalizesel(bool show){
	if (show){
		savestbt->setText(tr("Hide"));
		savestbt->setChecked(show);
	}
	else{
		savestbt->setText(tr("Show"));
		savestbt->setChecked(show);
	}
	emit this->setshowsel(show);
}


void MTC::showsel(bool show){
	if (show){
		savestbt->setText(tr("Hide"));
		savestbt->setChecked(show);
	}
	else{
		savestbt->setText(tr("Show"));
		savestbt->setChecked(show);
	}
}

void MTC::setbg(bool show){
	if (show){
		bgbt->setText(tr("Hide Unselected"));
		bgbt->setChecked(show);
	}
	else{
		bgbt->setText(tr("Show Unselected"));
		bgbt->setChecked(show);
	}
	emit this->setshowunsel(show);
}


