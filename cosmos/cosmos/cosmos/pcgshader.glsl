#version 420

layout(lines) in;
layout (line_strip, max_vertices=99) out;

uniform mat4 mvp_matrix;
uniform sampler3D p1;


uniform int xsize;
uniform int ysize;
uniform int zsize;
uniform int aaxis;

uniform vec4 ap;

uniform vec3 bmin;
uniform vec3 bmax;
uniform vec3 vmin;
uniform vec3 vmax;
uniform vec2 vol;

uniform float lv;
uniform float lh;



in VertexData {
    vec2 texCoord;
    vec3 normal;
} VertexIn[];

out VertexData {
    vec2 texCoord;
    vec3 normal;
} VertexOut;

 void main()
{
  for(int i = 0; i < gl_in.length(); i++)
  {

      // Calculate vertex position in screen space
    //    if(a_texcoord.w > 0){
        // gl_Position = mvp_matrix * vec4(a_position.xyz,1f);


      if(gl_in[i].gl_Position.x < 0){
      return;
      }

              int  ii =int(gl_in[i].gl_Position.w);

          for(int j = 0; j < 99; j++){

             // int index = ii * 4 * 99 + j * 4 + 0;
   int index = ii * 99 + j  + 0;
                  int ix = index/(ysize*zsize);
                  index -= ix*ysize*zsize;

                  int iy = index/zsize;
                  index -= iy*zsize;

                  int iz = index;

                    vec3 bah1 = vec3(float(ix)/float(xsize),float(iy)/float(ysize),float(iz)/float(zsize)); // vec3 (xsize,ysize,zsize);
                 // vec2 bah1 = vec2(float(ii)/float(xsize),float(j)/float(ysize));
                //  vec3 bah1 = vec3(float(j)/float(xsize),float(ii)/float(ysize),1.0);
             vec4 final = texture(p1,bah1);//texture3D(poxx, vec3(ix,iy,iz));

/*
 if(float(j) != float(final.y)){
     continue;
 }
 */
//final.x = .5;//j/99;

             int valid = 1;

if (final.x < 0.0){
 valid = -1;
}
             float min = -0.7;//-2.9;
             float max = 0.8;

     vec4 orig = final;


          final = vec4(final.x * (max - min) + min,final.y/98.0 ,final.z,1.0);




     switch(aaxis){
        case 0:
         VertexOut.normal = vec3(orig.x,0,0);
        break;
        case 1:
         VertexOut.normal = vec3(orig.y,0,0);
         break;
     case 2:
         VertexOut.normal = vec3(orig.z,0,0);
         break;
     case 3:
         VertexOut.normal = vec3(orig.w,0,0);
     default:
     VertexOut.normal = vec3(orig.x,0,0);//vec2(0,0);//a_texcoord;vec3(ix,iy,iz);//
     }  // color


  float d = .9 + .9;

  /*
  switch(j){
     case 15:
          gl_Position =  vec4(ap.x,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//
          break;
  case 16:
       gl_Position =  vec4(ap.y,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//
       break;
  case 17:
       gl_Position =  vec4(ap.z,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//
     break;
  case 18:
       gl_Position =  vec4(ap.w,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//
       break;
  default:
       gl_Position =  vec4(ap.w,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//
       break;

  }
*/ //float(j)/float(98.0)
  gl_Position =  vec4(float(j)/float(98.0)*d - .9,final.x,gl_in[i].gl_Position.w/-1000000,1.0); // mvp_matrix * vec4(a_position.xyz,1f);//


          VertexOut.texCoord = vec2(0,0);

          // done with the vertex

     if(valid > 0){
              EmitVertex();
}


          }


  }
  EndPrimitive();

}
