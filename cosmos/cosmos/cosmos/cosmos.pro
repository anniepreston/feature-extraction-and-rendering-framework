QT += widgets opengl printsupport

TARGET = cosmos
TEMPLATE = app

CONFIG += c++11

DEFINES += ENABLE_CUDA_GL_INTEROP
DEFINES +=  __func__=__FUNCTION__

QMAKE_CXXFLAGS_RELEASE =-O3

SOURCES += \
    Axis.cpp \
    comosparticle.cpp \
    cosmology.cpp \
    glViewWidget.cpp \
    main.cpp \
    mtcontrol.cpp \
    pc.cpp \
    pcMainWindow.cpp \
    qcustomplot.cpp \
    selectionbox.cpp \
    TF.cpp \
    TFEditor.cpp \
    Voxelizer.cpp

HEADERS += \
    comosparticle.h \
    SciVisData.h \
    TF.h \
    Voxelizer.h \
    Axis.h \
    pc.h \
    qcustomplot.h \
    pcMainWindow.h \
    mtcontrol.h \
    selectionbox.h \
    TFEditor.h \
    glviewwidget.h \
    cosmology.h

FORMS += \
    cosmology.ui \
    tabonewidget.ui

INCLUDEPATH +=  $$PWD/../../gitlibs/Lily/include/ \
                $$PWD/../../gitlibs/Lily/freetype-2.5.2/include/ \
                $$PWD/../../giblibs/Lily/freeglut-2.8.1/include/ \
                $$PWD/../../gitlibs/Lily/glew-1.10.0/include/ \
                $$PWD/../../libs/VisKit_vs2013/UI/QColorPicker/ \
                $$PWD/../../libs/VisKit_vs2013/UI/QTFEditor/ \
                $$PWD/../../libs/VisKit_vs2013/camera/ \
                $$PWD/../../libs/VisKit_vs2013/Render/ \
                $$PWD/../../libs/VisKit_vs2013/shadermanager/ \
                $$PWD/../../libs/VisKit_vs2013/camera/ \
                /usr/local/cuda/include/ \
                /usr/local/cuda/samples/common/inc/ \
                ~/NVIDIA_CUDA-7.0_Samples/common/inc/

LIBS += -L$$PWD/../../gitlibs/Lily/glew-1.10.0/lib/ \
        -L$$PWD/../../gitlibs/Lily/lib/ \
        -L$$PWD/../../libs/VisKit_vs2013/UI/QColorPicker/ \
        -L$$PWD/../../libs/VisKit_vs2013/UI/QTFEditor/ \
        -L$$PWD/../../libs/VisKit_vs2013/camera/ \
        -L$$PWD/../../libs/VisKit_vs2013/Render/ \
        -L/usr/lib/x86_64-linux-gnu

LIBS += -lLily -lQTFEditor -lQColorPicker -lrender -lcamera -lcudart -lcuda -lGLEW -lGLU -lGL -lglut -lfreetype #-lz -lQtOpenGL

#------CUDA BUILD SCRIPT-------
CUDA_SOURCES += cuda_kernel.cu
CUDA_OBJECTS_DIR = ./
CUDA_DIR = /usr/local/cuda
QMAKE_LIBDIR += $$CUDA_DIR/lib64
include(cuda.pri)
#------END OF CUDA BUILD SCRIPT-------
