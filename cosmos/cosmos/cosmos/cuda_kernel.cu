#include "cuda_kernel.cuh"
#include <thrust/device_ptr.h>
#include <thrust/for_each.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/sort.h>
#include <iostream>

extern "C"
{
	thrust::device_vector<float> m_sortkey;

    void calcDepth(float3  *pos,
		//float   *keys,        // output
		uint    *indices,     // output
		float3   sortVector,
		int      numParticles){
		thrust::device_ptr<float3> d_pos(pos);
		//thrust::device_ptr<float> d_keys(keys);
		thrust::device_ptr<float> d_keys = m_sortkey.data();
		thrust::device_ptr<uint> d_indices(indices);

		thrust::for_each(
			thrust::make_zip_iterator(thrust::make_tuple(d_pos, d_keys)),
			thrust::make_zip_iterator(thrust::make_tuple(d_pos + numParticles, d_keys + numParticles)),
			calcDepth_functor(sortVector));

		thrust::sequence(d_indices, d_indices + numParticles);
		/*
		thrust::host_vector<float> h_sortkey = m_sortkey;
		for (int i = 0; i < h_sortkey.size(); i++)
		{
			std::cout << "skey" << i <<"=" << h_sortkey[i] << std::endl;
		}
		*/
	}

	void sortParticles(/*float *sortKeys,*/ uint *indices, uint numParticles)
	{
		float *sortKeys = thrust::raw_pointer_cast(m_sortkey.data());
		thrust::sort_by_key(
			thrust::device_ptr<float>(sortKeys),
			thrust::device_ptr<float>(sortKeys + numParticles),
			thrust::device_ptr<uint>(indices));
	}


	void allocSortkeyCUDA(uint numParticles)
	{
		m_sortkey.resize(numParticles, 0.0f);
	}

}
