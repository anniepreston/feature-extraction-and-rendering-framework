#ifndef MTCONTROL_H
#define MTCONTROL_H

#include <QtGui>
#include <QWidget>
#include <QMouseEvent>
#include <vector>
#include <QLabel>
#include <QToolButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QRadioButton>

#define terpi(t,a,b) (( t - a)/(b - a))
#define lerp(t, a, b) ( a + t * (b - a) )



class MTC : public QWidget
{
	Q_OBJECT
public:
	explicit MTC(QWidget *parent = 0);


protected:

	bool sel, unsel;
	QToolButton *loadstbt;
	QToolButton *savestbt;
	QToolButton *bgbt;

	public slots:
	// void zoomIn(int level = 1);
	void showsel(bool);
	//  void showunsel(bool show);


signals:

	void setshowsel(bool show);
	void setshowunsel(bool show);
	void sendclear();

	private slots:
	void clearsel();
	void finalizesel(bool);
	void setbg(bool);


};

#endif // MTCONTROL_H