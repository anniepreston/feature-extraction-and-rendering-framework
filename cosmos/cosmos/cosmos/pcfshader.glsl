#version 420

layout(location = 0) out vec4 out_colour;

uniform sampler2D p1;

uniform int am;
uniform int goff;
uniform int coff;

uniform mat4 mvp_matrix;

in VertexData{
	vec2 texCoord;
	vec3 normal;
} VertexIn;


//! [0]
void main()
{

	// calc vol mag
	//  float mag =  v_texcoord.x * v_texcoord.x + v_texcoord.y * v_texcoord.y + v_texcoord.z * v_texcoord.z;

	// Set fragment color from texture
	vec4 outc = vec4(1.0, 1.0, 1.0, VertexIn.texCoord.x);//mag/float(600*600+600*600+600*600))
	if (am == 0){
		// gl_FragColor = outc;
		out_colour = vec4(1.0, 1.0, 1.0, 0.5);//outc;
	}
	else if (am == 1){
		//  gl_FragColor = vec4(outc.xyz,1.0-((log(1.0-exp(-am))*cla+exp(-am))/-am));  // alpha = 1 - ((ln(1-e^-a)oldalpha + e^-a)/-a)
		out_colour = outc;// vec4(outc.xyz,1.0-((log(1.0-exp(-am))*cla+exp(-am))/-am));
	}
	else{

		out_colour = vec4(1.0, 0.0, 0.0, 0.5);
	}

}
//! [0]
