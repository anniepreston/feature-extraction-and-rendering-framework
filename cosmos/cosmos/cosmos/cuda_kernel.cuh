//#ifndef _CUDA_KERNEL_H_
//#define _CUDA_KERNEL_H_
#include <math_constants.h>
#include <vector_types.h>
#include <helper_math.h>
#include "thrust/tuple.h"
#include <thrust/device_vector.h>

typedef unsigned int uint;

struct calcDepth_functor
{
	float3 sortVector;

	__host__ __device__
		calcDepth_functor(float3 sort_vector) : sortVector(sort_vector) {}

	template <typename Tuple>
	__host__ __device__
		void operator()(Tuple t)
	{
		volatile float3 p = thrust::get<0>(t);
		float key = - dot(make_float3(p.x, p.y, p.z), sortVector); // project onto sort vector
		thrust::get<1>(t) = key;
	}
};

extern "C"
{
	void
		calcDepth(float3  *pos,
		//float   *keys,        // output
		uint    *indices,     // output
		float3   sortVector,
		int      numParticles);

	void sortParticles(/*float *sortKeys,*/ uint *indices, uint numParticles);
	void allocSortkeyCUDA(uint numParticles);
}
//#endif
