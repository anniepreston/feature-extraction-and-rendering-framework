#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "cosmology.h"
#include "glviewwidget.h"
#include <QMainWindow>
#include <QWidget>
#include <QOpenGLWidget>
//qcustomplot
#include <QGLFormat>
#include <QMenuBar>
#include <QProgressBar>
#include <QStatusBar>

class QAction;
class QLabel;
class QMenu;

QT_BEGIN_NAMESPACE
class QSplitter;
class QProgressBar; 
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QMainWindow *parent = 0);
	//new cosmos
	//cosmology* visWidget = new cosmology();
	//qcustomplot


private:
	//add these as needed
	//QActions
	QLabel *ambientLabel;
	QLabel *diffuseLabel;
	QLabel *displayCutoffLabel;
	QLabel *time_label;

	QLabel *colorMap;
	QLabel *visMap;
	QLabel *visSlider;
	QLabel *haloParticle;

	QAction *massAct;
	QAction *velAct;
	QAction *dotAct;
	QAction *varAct;

	//PC stuff
	
	//qsplitters
	//QSplitter *h1Splitter;
	//QSplitter *h2Splitter;
	QSplitter *vSplitter;

protected:

public slots:
	//fill these in

};

#endif //MAINWINDOW_H