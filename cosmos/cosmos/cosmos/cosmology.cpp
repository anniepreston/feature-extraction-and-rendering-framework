#include "cosmology.h"
#include <QFile>
#include <QHBoxLayout>
#include <QSplitter>
#include <QComboBox>
#include <QGroupBox>
#include <QToolButton>
#include <QCheckBox>
#include <QProgressBar>
#include <QStatusBar>

cosmology::cosmology(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	//setFocusPolicy(Qt::StrongFocus);
	QActionGroup* group = new QActionGroup(this);
	group->addAction(ui.actionSelection);
	group->addAction(ui.actionDeselect);
	group->addAction(ui.actionRotation);
	group->addAction(ui.actionFree_camera);
	//Annie's part
    int num_part = 100;

	//QMainWindow* AnnieStaff = new QMainWindow(NULL);

	this->statusBar()->showMessage(tr("Ready"));
	QProgressBar *loadbar = new QProgressBar();
	loadbar->setFixedSize(QSize(100, 20));
	this->statusBar()->addPermanentWidget(loadbar);
	time_label = new QLabel("t = 80, z = 0.5", this);
	time_label->setStyleSheet("QLabel { color : white; font: bold 16px}");
	this->statusBar()->addPermanentWidget(time_label);

	//link halo subset signal:
	//connect(ui.widget_row2, SIGNAL(newAidsLoadedMW(intList)), ui.m_glviewwidget, SLOT(haloSubset(intList)));

	/*
	PC *mergeTree = new PC();
	vSplitter = new QSplitter();
	vSplitter->setOrientation(Qt::Vertical);
	vSplitter->addWidget(this);
	vSplitter->addWidget(mergeTree);*/

	/*
	vSplitter = new QSplitter(AnnieStaff);
	vSplitter->setOrientation(Qt::Vertical);
	h2Splitter = new QSplitter(AnnieStaff);

	QPalette Pal(palette());
	Pal.setColor(QPalette::Background, Qt::darkGray);
	AnnieStaff->setAutoFillBackground(true);
	AnnieStaff->setPalette(Pal);

	QComboBox *attribBox = new QComboBox();
	attribBox->addItem(tr("velocity"));
	attribBox->addItem(tr("dot product"));
	attribBox->addItem(tr("variance"));

	QComboBox *displayAttribBox = new QComboBox();
	displayAttribBox->addItem(tr("velocity"));
	displayAttribBox->addItem(tr("dot product"));
	displayAttribBox->addItem(tr("variance"));

	QComboBox *halosOrParticles = new QComboBox();
	halosOrParticles->addItem(tr("halos"));
	halosOrParticles->addItem(tr("particles"));

	QSlider *cutoffSlider = new QSlider(Qt::Horizontal);
	cutoffSlider->setRange(0, 100);
	cutoffSlider->setSingleStep(1);
	cutoffSlider->setValue(50);

	QGroupBox *dataWidget = new QGroupBox(tr(""));
	QToolButton *haloButton = new QToolButton;
	haloButton->setAutoRaise(true);
	QToolButton *particleButton = new QToolButton;
	particleButton->setAutoRaise(true);
	QGridLayout *dataLayout = new QGridLayout;
	colorMap = new QLabel("map to color:");
	visMap = new QLabel("map to visibility:");
	visSlider = new QLabel("visibility cutoff:");
	haloParticle = new QLabel("data type:");
	dataLayout->addWidget(haloParticle, 0, 0);
	dataLayout->addWidget(halosOrParticles, 0, 1);
	dataLayout->addWidget(colorMap, 1, 0);
	dataLayout->addWidget(attribBox, 1, 1);
	dataLayout->addWidget(visMap, 2, 0);
	dataLayout->addWidget(displayAttribBox, 2, 1);
	dataLayout->addWidget(cutoffSlider, 3, 1);
	dataLayout->addWidget(visSlider, 3, 0);
	dataLayout->setColumnStretch(1, 0);
	dataWidget->setLayout(dataLayout);

	QGroupBox *lightingWidget = new QGroupBox(tr(""));

	ambientLabel = new QLabel("ambient:");
	QSlider *ambientSlider = new QSlider(Qt::Horizontal);
	ambientSlider->setRange(0, 150);
	ambientSlider->setSingleStep(1);
	ambientSlider->setValue(50);

	diffuseLabel = new QLabel("diffuse:");
	QSlider *diffuseSlider = new QSlider(Qt::Horizontal);
	diffuseSlider->setRange(0, 150);
	diffuseSlider->setSingleStep(1);
	diffuseSlider->setValue(50);

	QCheckBox *lightingCheckBox = new QCheckBox("Lighting On");

	QGridLayout *hboxLighting = new QGridLayout;
	hboxLighting->addWidget(ambientLabel, 1, 0);
	hboxLighting->addWidget(ambientSlider, 1, 1);
	hboxLighting->addWidget(diffuseLabel, 2, 0);
	hboxLighting->addWidget(diffuseSlider, 2, 1);

	hboxLighting->addWidget(lightingCheckBox, 0, 0);

	lightingWidget->setLayout(hboxLighting);

	QTabWidget *tabWidget = new QTabWidget;
	//tabWidget->addTab(tfEditor, tr("mapping"));
	tabWidget->setMinimumWidth(200);
	tabWidget->addTab(dataWidget, tr("data"));
	tabWidget->addTab(lightingWidget, tr("appearance"));
	//tabWidget->addTab(plotView, tr("plot view"));
	//mergeTree->setMinimumHeight(200);
	//mergeTree->setMinimumWidth(1000);

	AnnieStaff->statusBar()->showMessage(tr("Ready"));
	//QProgressBar *loadbar = new QProgressBar();
	//loadbar->setFixedSize(QSize(100, 20));
	AnnieStaff->statusBar()->addPermanentWidget(loadbar);
	time_label = new QLabel("t = 80, z = 0.5", NULL);
	time_label->setStyleSheet("QLabel { color : white; font: bold 16px}");
	AnnieStaff->statusBar()->addPermanentWidget(time_label);

	//do this on updating graph:
	/*
		std::vector<lily::vec3f> vel_data = ui.m_glviewwidget->getCurrentVel();
		if (vel_data = NULL)
		{
		std::cout << "data not read yet\n" << std::endl;
		}*/

	//connect combobox:
	connect(ui.comboBox, SIGNAL(currentIndexChanged(int)), ui.m_glviewwidget, SLOT(changeAttribute(int)));
	connect(ui.comboBox_2, SIGNAL(currentIndexChanged(int)), ui.m_glviewwidget, SLOT(changeDataType(int)));
	
	//timestep slider:
	//connect(ui.timeStep, SIGNAL(sliderMoved(int)), ui.m_glviewwidget, SLOT(receiveTime(int)));
	connect(ui.timeComboBox, SIGNAL(currentIndexChanged(int)), ui.m_glviewwidget, SLOT(receiveTime(int)));
	connect(ui.timeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateTime(int)));

	//read in particle data from step 80:
	std::vector<SciVisParticles> particles;
	std::string filename = "./cosmoVis_data_files/SciVisParticleOutput_80.bin";
	std::ifstream ifs;
	ifs.open(filename, std::ios::binary);
	if (!ifs)
	{
		cerr << "Cannot open file: " << filename << std::endl;
	}
	size_t count = 0;
	ifs.read(reinterpret_cast<char*>(&count), sizeof(count));
	particles.resize(count);
	ifs.read(reinterpret_cast<char*>(particles.data()),
		particles.size()*sizeof(SciVisParticles));
	ifs.close();
	num_part = particles.size();

	for (int i = 0; i < num_part; i++)
	{
		x.push_back(sqrtf(particles[i].vx*particles[i].vx + particles[i].vy*particles[i].vy + particles[i].vz*particles[i].vz));
		y.push_back(particles[i].dot);
		//var.push_back(particles[i].variance);
	}


	ui.plotViewWidget->addGraph();
	ui.plotViewWidget->graph(0)->setData(x, y);
	ui.plotViewWidget->setInteraction(QCP::iSelectAxes, true);
	ui.plotViewWidget->yAxis->setLabel("v dot product");
	ui.plotViewWidget->xAxis->setLabel("velocity magnitude (km/s)");
	ui.plotViewWidget->xAxis->setRange(0, 100);
	ui.plotViewWidget->yAxis->setRange(0, 1);
	ui.plotViewWidget->xAxis->setLabelColor(QColor(200, 200, 200, 255));
	ui.plotViewWidget->yAxis->setLabelColor(QColor(200, 200, 200, 255));
	ui.plotViewWidget->xAxis->setTickLabelColor(QColor(200, 200, 200, 255));
	ui.plotViewWidget->yAxis->setTickLabelColor(QColor(200, 200, 200, 255));
	ui.plotViewWidget->xAxis->setSelectableParts(QCPAxis::spAxisLabel);
	ui.plotViewWidget->yAxis->setSelectableParts(QCPAxis::spAxisLabel);
	ui.plotViewWidget->setBackground(QBrush(QColor(50, 50, 50, 255)));
	ui.plotViewWidget->axisRect()->setBackground(Qt::black);
	ui.plotViewWidget->axisRect()->setRangeDrag(Qt::Vertical | Qt::Horizontal);
	ui.plotViewWidget->axisRect()->setRangeZoom(Qt::Vertical | Qt::Horizontal);
	ui.plotViewWidget->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, Qt::white, 5));
	ui.plotViewWidget->graph(0)->setLineStyle(QCPGraph::lsNone);
	ui.plotViewWidget->setInteraction(QCP::iRangeDrag, true);
	ui.plotViewWidget->setInteraction(QCP::iRangeZoom, true);
	ui.plotViewWidget->replot();

	//pop-up axis menus:
	connect(ui.plotViewWidget->yAxis, SIGNAL(selectionChanged(QCPAxis::SelectableParts)), this, SLOT(contextMenuRequest(QCPAxis::SelectableParts)));
	connect(ui.plotViewWidget->xAxis, SIGNAL(selectionChanged(QCPAxis::SelectableParts)), this, SLOT(contextMenuRequest(QCPAxis::SelectableParts)));

	//screen-space selection (halos):
	//connect(ui.m_glviewwidget, SIGNAL(particles_3D_selected(intList)), ui.widget_row2, SLOT(particles_received(intList)));
}
	//----

cosmology::~cosmology()
{
}

void cosmology::keyPressEvent(QKeyEvent* event)
{
}

void cosmology::contextMenuRequest(QCPAxis::SelectableParts part)
{
	QString axis;

	if (qobject_cast<QCPAxis*>(sender()) == ui.plotViewWidget->yAxis){ axis = "y"; }

	else if (qobject_cast<QCPAxis*>(sender()) == ui.plotViewWidget->xAxis){ axis = "x"; }

	QMenu *menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	//custom signals/slots for update data actions
	velAct = new QAction(tr("velocity"), this);
	dotAct = new QAction(tr("dot product"), this);
	varAct = new QAction(tr("variance"), this);
	QSignalMapper* plotSignalMapper = new QSignalMapper(this);
	connect(velAct, SIGNAL(triggered()), plotSignalMapper, SLOT(map()));
	connect(dotAct, SIGNAL(triggered()), plotSignalMapper, SLOT(map()));
	connect(varAct, SIGNAL(triggered()), plotSignalMapper, SLOT(map()));
	QString velocity = axis + "velocity";
	QString dot = axis + "dot product";
	QString variance = axis + "variance";
	plotSignalMapper->setMapping(velAct, velocity);
	plotSignalMapper->setMapping(dotAct, dot);
	plotSignalMapper->setMapping(varAct, variance);
	connect(plotSignalMapper, SIGNAL(mapped(QString)), this, SLOT(axisMenu(QString)));

	menu->addAction(velAct);
	menu->addAction(dotAct);
	menu->addAction(varAct);

	QPoint pos = this->mapFromGlobal(QCursor::pos());
	menu->popup(ui.plotViewWidget->mapToGlobal(pos));
}

void cosmology::axisMenu(QString data)
{
	QVector<double> vel;
	QVector<double> var;
	QVector<double> dot;

	std::vector<SciVisParticles> particles;
	std::string filename = "./cosmoVis_data_files/SciVisParticleOutput_80.bin";
	std::ifstream ifs;
	ifs.open(filename, std::ios::binary);
	if (!ifs)
	{
		cerr << "Cannot open file: " << filename << std::endl;
	}
	size_t count = 0;
	ifs.read(reinterpret_cast<char*>(&count), sizeof(count));
	particles.resize(count);
	ifs.read(reinterpret_cast<char*>(particles.data()),
		particles.size()*sizeof(SciVisParticles));
	ifs.close();
	num_part = particles.size();

	for (int i = 0; i < num_part; i++)
	{
		vel.push_back(sqrtf(particles[i].vx*particles[i].vx + particles[i].vy*particles[i].vy + particles[i].vz*particles[i].vz));
		dot.push_back(particles[i].dot);
		var.push_back(particles[i].variance);
	}

	int axis;
	if (data[0] == 'x'){ axis = 0; }
	else if (data[0] == 'y'){ axis = 1; }
	QString test_string = data.remove(0, 1);

	QVector<double> new_data(num_part);
	for (int i = 0; i < num_part; i++){

		if (test_string == "velocity"){
			/*
			new_data[i] = sqrtf(pow(ui.m_glviewwidget->getCurrentVel()[i].x(), 2) +
				pow(ui.m_glviewwidget->getCurrentVel()[i].y(), 2) +
				pow(ui.m_glviewwidget->getCurrentVel()[i].z(), 2));*/
			new_data[i] = vel[i];
		}
		else if (test_string == "dot"){
			//new_data[i] = ui.m_glviewwidget->getCurrentMS()[i].x();
			new_data[i] = dot[i];
		}
		else if (test_string == "variance"){
			//new_data[i] = ui.m_glviewwidget->getCurrentMS()[i].y();
			new_data[i] = var[i];
		}
	}
	if (axis == 0){
		ui.plotViewWidget->graph(0)->setData(new_data, y);
		ui.plotViewWidget->xAxis->setLabel(test_string);
	}
	else if (axis == 1){
		ui.plotViewWidget->graph(0)->setData(x, new_data);
		ui.plotViewWidget->yAxis->setLabel(test_string);
	}
	ui.plotViewWidget->replot();
}

void cosmology::updateTime(int timestep)
{
	QString new_label = "t = " + QString::number(timestep) + ", z = 0.5";
	time_label->setText(new_label);
}
