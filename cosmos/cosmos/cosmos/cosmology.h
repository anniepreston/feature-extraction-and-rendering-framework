#ifndef COSMOLOGY_H
#define COSMOLOGY_H

#include "../../gitlibs/Lily/glew-1.10.0/include/GL/glew.h"

#include <QMainWindow>
#include <qsplitter.h>
#include "ui_cosmology.h"
#include "Axis.h" //TESTING

class cosmology : public QMainWindow
{
	Q_OBJECT

public:
	cosmology(QWidget *parent = 0);
	~cosmology();
	virtual void keyPressEvent(QKeyEvent* event);
	//virtual void mouseDoubleClickEvent(QMouseEvent *event);
	//for plotting:
	QVector<double> x;
	QVector<double> y;
private:
	Ui::cosmologyClass ui;
	QActionGroup* group;
	//Annie's part

	QLabel *time_label;
	QSplitter *vSplitter;

	QAction *velAct;
	QAction *dotAct;
	QAction *varAct;
    int num_part;

public slots:
void contextMenuRequest(QCPAxis::SelectableParts);
void axisMenu(QString data);
void updateTime(int timestep);

};

#endif // COSMOLOGY_H
