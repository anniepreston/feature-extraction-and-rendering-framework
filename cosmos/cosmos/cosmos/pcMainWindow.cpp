#include "pcMainWindow.h"
#include "pc.h"
#include <QIcon>
#include "Axis.h"
#include "mtcontrol.h"

#include <QHBoxLayout>
#include <QSplitter>

PCMainWindow::PCMainWindow(QWidget* parent)
    : QWidget(parent)
	//MainWindow::MainWindow(QWidget *parent)
	//    : QWidget(parent)
{
	QIcon test = QIcon(":/styles/icon.png");
	this->setWindowIcon(test);
	/*
	QMenu *filemenu = this->menuBar()->addMenu(tr("&File"));


	QAction *loadfiles = new QAction("L&oad Files", this);
	QAction *processfiles = new QAction("&Preprocess Files", this);
	QAction *loadstate = new QAction("&Load State", this);
	QAction *savestate = new QAction("&Save State", this);

	filemenu->addAction(loadfiles);

	filemenu->addAction(processfiles);

	filemenu->addAction(loadstate);
	filemenu->addAction(savestate);

	this->statusBar()->showMessage(tr("Ready"));
	QProgressBar *loadbar = new QProgressBar();
	loadbar->setFixedSize(QSize(100, 20));
	this->statusBar()->addPermanentWidget(loadbar);
	*/

	//loadbar->setValue(50);

	this->setMinimumWidth(1200);
	//this->setGeometry(0,0,1600,1200);
	//this->setGeometry(0,0,300,400);
	QPalette Pal(palette());
	// set black background
	Pal.setColor(QPalette::Background, Qt::darkGray);
	this->setAutoFillBackground(true);
	this->setPalette(Pal);


	populateScene();
	
	//  h1Splitter = new QSplitter;
	h2Splitter = new QSplitter;
	h2Splitter->setOrientation(Qt::Vertical);

	//testing?
	TFEditor * tfe = new TFEditor();

	// pc stuff
	QSlider *timeselect = new QSlider;
	timeselect->setMinimum(0);
	timeselect->setMaximum(88);
	timeselect->setValue(88);
	timeselect->setTickPosition(QSlider::TicksAbove);
	timeselect->setOrientation(Qt::Horizontal);


	PC *pc = new PC;//(this,glWidget);
	MTC *mtc = new MTC;
	//  PC *pc = new PC;

	connect(pc, SIGNAL(newAidsLoaded(intList)), this, SLOT(sendNewAidsMW(intList)));
	connect(this, SIGNAL(send_particles_to_pc(intList)), pc, SLOT(onNewAids(intList)));

	// h2Splitter->addWidget(pc); // par c v

	View *xaxis = new View("X Axis");
	View *yaxis = new View("Y Axis");
	xaxis->setOrientation(0);
	yaxis->setOrientation(1);

	xaxis->setMaximumWidth(width()*.1);
	yaxis->setMaximumHeight(height()*.1);

	QWidget *fu = new QWidget;
	//  QSplitter *   hSplitter = new QSplitter;
	QGridLayout *gridlo = new QGridLayout;

	//gridlo->addWidget(xaxis, 0, 0);
	//gridlo->addWidget(pc, 0, 1);
	//gridlo->addWidget(yaxis, 1, 1);
	gridlo->addWidget(timeselect, 0, 1);
	gridlo->addWidget(xaxis, 1, 0);
	gridlo->addWidget(pc, 1, 1);
	gridlo-> addWidget(yaxis, 2, 1);
	gridlo->addWidget(mtc, 2, 0);
	fu->setLayout(gridlo);

	h2Splitter->addWidget(fu);

	// pc->show();

	//  pTab->addTab(pc, "Merger Tree");

	//connect(processfiles, SIGNAL(triggered()), pc, SLOT(prepfiles()));

	//  connect(tfEditor, SIGNAL(changeTFTex()), pc, SLOT(tf_received_test()));

	//connect(tfe, SIGNAL(tfChanged(TF&)), pc, SLOT(onnewtf(TF&)));
	// connect x axis
	connect(pc, SIGNAL(newxlab(QString&)), yaxis, SLOT(onnewlab(QString&)));
	connect(pc, SIGNAL(newxlimit(float, float)), yaxis, SLOT(onnewlimit(float, float)));
	connect(pc, SIGNAL(newxllimit(float, float)), yaxis, SLOT(onnewllimit(float, float)));

	// connect y axis
	connect(pc, SIGNAL(newylab(QString&)), xaxis, SLOT(onnewlab(QString&)));
	connect(pc, SIGNAL(newylimit(float, float)), xaxis, SLOT(onnewlimit(float, float)));
	connect(pc, SIGNAL(newyllimit(float, float)), xaxis, SLOT(onnewllimit(float, float)));

	connect(pc, SIGNAL(setshowsel(bool)), mtc, SLOT(showsel(bool)));
	connect(mtc, SIGNAL(setshowsel(bool)), pc, SLOT(showsel(bool)));
	connect(mtc, SIGNAL(setshowunsel(bool)), pc, SLOT(showbg(bool)));
	connect(mtc, SIGNAL(sendclear()), pc, SLOT(clearsel()));

	/*
	connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),pc,SLOT(onNewData(std::vector<Series*>&)));

	connect(c,SIGNAL(newmin(QRgb&)),pc,SLOT(onnewmin(QRgb&)));
	connect(c,SIGNAL(newmax(QRgb&)),pc,SLOT(onnewmax(QRgb&)));

	connect(c,SIGNAL(newSampleset(int)),pc,SLOT(onnewsamp(int)));

	connect(c,SIGNAL(newopa(int)),pc,SLOT(onnewopa(int)));

	connect(view,SIGNAL(newActivated(int,int,float,float)),pc,SLOT(onNewActivate(int,int,float,float)));

	connect(widget,SIGNAL(newText(GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint)),pc,SLOT(onNewText(GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint)));

	connect(pc,SIGNAL(newaxis(int)),widget,SLOT(onnewaxis(int)));

	//  connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),view,SLOT(onNewData(std::vector<Series*>&)));

	connect(c,SIGNAL(newmaxx(float)),pc,SLOT(onnewmaxx(float)));
	connect(c,SIGNAL(newmaxy(float)),pc,SLOT(onnewmaxy(float)));
	connect(c,SIGNAL(newmaxz(float)),pc,SLOT(onnewmaxz(float)));

	connect(c,SIGNAL(newminx(float)),pc,SLOT(onnewminx(float)));
	connect(c,SIGNAL(newminy(float)),pc,SLOT(onnewminy(float)));
	connect(c,SIGNAL(newminz(float)),pc,SLOT(onnewminz(float)));

	connect(c,SIGNAL(newmaxvx(float)),pc,SLOT(onnewmaxvx(float)));
	connect(c,SIGNAL(newmaxvy(float)),pc,SLOT(onnewmaxvy(float)));
	connect(c,SIGNAL(newmaxvz(float)),pc,SLOT(onnewmaxvz(float)));

	connect(c,SIGNAL(newminvx(float)),pc,SLOT(onnewminvx(float)));
	connect(c,SIGNAL(newminvy(float)),pc,SLOT(onnewminvy(float)));
	connect(c,SIGNAL(newminvz(float)),pc,SLOT(onnewminvz(float)));

	connect(c,SIGNAL(newmaxv(float)),pc,SLOT(onnewmaxv(float)));
	connect(c,SIGNAL(newminv(float)),pc,SLOT(onnewminv(float)));
	connect(c,SIGNAL(newalpha(int)),pc,SLOT(onnewalpha(int)));

	connect(c,SIGNAL(newactive(int,int,float,float)),pc,SLOT(onNewActivate(int,int,float,float)));
	connect(c,SIGNAL(newbg(QRgb&)),pc,SLOT(onnewbg(QRgb&)));
	*/
	connect(timeselect, SIGNAL(sliderMoved(int)), pc, SLOT(onNewTime(int)));

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(h2Splitter);

	setLayout(layout);

    /*
	QWidget *dummy = new QWidget();
	dummy->setLayout(layout);
	setCentralWidget(dummy);
    */

	//  this->setCentralWidget(layout);
	//docking stuff
	/*
	QDockWidget *dock = new QDockWidget(tr("Prespective"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(widget);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);


	dock = new QDockWidget(tr("Controls"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(c);
	this->addDockWidget(Qt::LeftDockWidgetArea, dock);


	dock = new QDockWidget(tr("Temporal"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(view);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);


	dock = new QDockWidget(tr("Parallel Coordinates"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(pc);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);
	*/
	setWindowTitle(tr("CosmoVis"));
}

void PCMainWindow::populateScene()
{
	//scene = new QGraphicsScene;

	/*  QImage image(":/qt4logo.png");

	// Populate scene
	int xx = 0;
	int nitems = 0;
	for (int i = -11000; i < 11000; i += 110) {
	++xx;
	int yy = 0;
	for (int j = -7000; j < 7000; j += 70) {
	++yy;
	qreal x = (i + 11000) / 22000.0;
	qreal y = (j + 7000) / 14000.0;

	QColor color(image.pixel(int(image.width() * x), int(image.height() * y)));
	QGraphicsItem *item = new Series(color, xx, yy);
	item->setPos(QPointF(i, j));
	scene->addItem(item);

	++nitems;
	}
	}*/

}

void PCMainWindow::sendNewAidsMW(intList aids)
{
	emit newAidsLoadedMW(aids);
}

void PCMainWindow::particles_received(intList ids)
{
	emit send_particles_to_pc(ids);
}
