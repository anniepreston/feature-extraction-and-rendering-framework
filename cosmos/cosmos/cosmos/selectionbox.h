#ifndef SELECTIONBOX_H
#define SELECTIONBOX_H

#include <QWidget>
#include <QObject>
#include <QRubberBand>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QSizeGrip>
#include <QPainter>
#include <QPaintEvent>
#include <QDebug>



class selectionbox : public QWidget
{
	Q_OBJECT
public:
	explicit selectionbox(QWidget* parent = 0);
	void draw(QPainter &painter);
	void setbgcolor(QColor color);

protected:
	void mousePressEvent(QMouseEvent *e);
	void mouseMoveEvent(QMouseEvent *e);
	void mouseReleaseEvent(QMouseEvent *e);

	public slots:
signals :

	void sendupdate();

private:

	// QRubberBand* rubberband;
	bool movepressed;

	int Resizepressed;
	QColor bgcolor;

	QPoint selection_offset;

	// void resizeEvent(QResizeEvent *);

	//  void paintEvent(QPaintEvent *event);



};
#endif // SELECTIONBOX_H