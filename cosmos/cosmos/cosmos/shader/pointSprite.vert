#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 1) in vec3 in_vel;
layout(location = 2) in int  in_id;
layout(location = 3) in vec2 in_ms;

uniform mat4 mvm;
uniform mat4 prj;
uniform vec2 window_size;
uniform float screen_diagnoal;
//point size fall off speed w.r.t its distance to the camera.
uniform float distFalloff;
uniform float input_pointsize;

out V2F{
	flat int id;
	vec3 vel;
	float dist;
	vec2 ms;
}v2f;

const float dist_max = 2.0;

void main(void)
{
	vec4 e_pos = mvm * vec4(in_pos, 1.0);
	gl_Position = prj * e_pos;
	v2f.id  = in_id;
	v2f.vel = in_vel;
	v2f.ms = in_ms;
	float d = length(e_pos.xyz);
	//TESTING bs:
	if (in_id < 0)
	{
		d += 0.000001;
	}
	if (in_vel.x < -1000){
		d += 0.000001;
	}
	if (in_ms.x < -2)
	{
		d += 0.00001;
	}
	v2f.dist = d;
	float point_scale = exp(-d*d*distFalloff);//pow(1.0 - dist / dist_max, 4.0);
	gl_PointSize = 16.0 * point_scale * (screen_diagnoal / 2048.0) * input_pointsize; 
}