#version 430

in V2F{
	flat int id;
	vec3 vel;
	float dist;
	vec2 ms;
}v2f;

uniform sampler1D texTF;
uniform vec2 selRecMin;
uniform vec2 selRecMax;
//alpha fall off speed.
uniform float afalloff;
uniform int attribIndex;

layout(binding = 0)
buffer SelectStatusArray
{
	int seldata[];
};


out vec4 out_color;

float gaussion_r = 2.0;

void main(void)
{
	float mag;
	if (attribIndex == 0 || attribIndex == -1)
	{
		mag = length(v2f.vel) / 300.;//compute velocity magnitude. &&& annie's hack
	}
	else if (attribIndex == 1)
	{
		mag = v2f.ms.x;
	}
	else
	{
		mag = v2f.ms.y;
	}
	vec4 rgba = texture(texTF, mag);
	if (all(greaterThan(gl_FragCoord.xy, selRecMin))
		&& all(lessThan(gl_FragCoord.xy, selRecMax)))
	{
		rgba.rgb = vec3(1.0);
	}
	if (seldata[v2f.id]>0)
	{
		rgba = vec4(1.0);
	}
	if (v2f.vel.x == -1.0 && v2f.vel.y  == -1.0 && v2f.vel.y == -1.0)
	{
		rgba = vec4(1.0);
	}
	
	//compute alpha fall-off.
	const float r_max = length(vec2(0.5));
	float r = length(gl_PointCoord.st - vec2(0.5));
	rgba.a *= exp(-r*r * afalloff); //float a = pow((1.0 - r/r_max),6);
	//rgba.rgb *= rgba.a;//Front-to-Back Blending
	
	out_color = rgba;
	//out_color = vec4(0.0);
}