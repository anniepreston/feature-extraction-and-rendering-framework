#version 430

uniform int stage;
uniform vec2 selRecMin;
uniform vec2 selRecMax;
uniform int bSeleted;//0 or 1

layout(binding = 0)
buffer IndexArray
{
	int idxdata[];
};
layout(binding = 1)
buffer SelectStatusArray
{
	int seldata[];
};

layout(binding = 2, offset = 0)
uniform atomic_uint ac;

in V2F{
	flat int id;
}v2f;
//out vec4 out_color;

void main(void)
{
	int index = -1;
	if (all(greaterThan(gl_FragCoord.xy, selRecMin))
	&& all(lessThan(gl_FragCoord.xy, selRecMax)))
	{
		index = int(atomicCounterIncrement(ac));
	}

	if (stage==1 && index>=0)
	{
		//imageStore(texSelIndices, int(index), ivec4(v2f.id));
		idxdata[index] = v2f.id;
		seldata[v2f.id] = bSeleted;
	}

	//out_color = vec4(1);
}