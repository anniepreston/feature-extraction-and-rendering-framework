#version 430
layout(location = 0) in vec3 in_pos;
layout(location = 1) in int in_id;
uniform mat4 mvm;
uniform mat4 prj;
uniform float input_pointsize;

out V2F{
	flat int id;
}v2f;


void main(void)
{
	gl_Position  = prj * mvm * vec4(in_pos, 1.0);
	v2f.id = in_id;
	gl_PointSize = input_pointsize; 
}