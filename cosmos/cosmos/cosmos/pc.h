#ifndef PC_H
#define PC_H

//#include "Axis.h"

#include <QOpenGLWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector2D>
#include <QBasicTimer>
#include <QOpenGLShaderProgram>
#include <vector>
#include <QTableView>
#include <QToolTip>
#include <QStandardItem>
#include <QLabel>
#include <QVBoxLayout>
#include <QDialog>
#include <QAction>
#include <QMenu>
#include <QTime>
#include <map>


#include <QRubberBand>
#include "selectionbox.h"
#include "TF.h"

#include <QOpenGLBuffer>


#define terpi(t,a,b) (( t - a)/(b - a))
#define lerp(t, a, b) ( a + t * (b - a) )

struct HaloNode
{
	int id;
	int childid;
	std::vector<int> pids;
	std::vector<int> cids;
	std::vector<float> vars;
};

QT_BEGIN_NAMESPACE
class QToolTip;
class QTableView;
class QStandardItemModel;
QT_END_NAMESPACE

using namespace vv;

//class GeometryEngine;
class QGLPixelBuffer;

class PC : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	typedef std::vector<int> intList;
	//explicit PC(QWidget *parent = 0, QOpenGLWidget *shared = 0);
	explicit PC(QWidget *parent = 0);
	// std::vector<Series*> series;
	std::vector<int> activeids;
	std::vector<int> activeidx;
	std::map<int, int> mapper;

	std::vector<int> senderids;

	std::vector<GLfloat>  activeidsvbo;


	int activetime;
	std::vector< std::vector<HaloNode> > mergdata;
	int s1, s2, t1, t2, t3, t4, am;
	float xmin, xmax, ymin, ymax;
	float lv, lh;
	~PC();
	void snapshot();
	void oldmethod();

	void drawpath(int time, int id);
	void drawparents(int time, int id);
	void drawchilds(int time, int id);

	void fillvbo0();
	void fillvbos();

	//void populateScene();

protected:

	bool bg;

	GLuint dynamicTexture;
	QGLPixelBuffer *pbuffer;
	bool hasDynamicTextureUpdate;

	QPointF pixelPosToViewPos(const QPointF& p);
	QTime m_time;
	int m_lastTime;
	int m_mouseEventTime;
	int m_distExp;
	int m_frame;
	int mousedown;
	int axis1, axis2, axis3, axis4;
	float ap1, ap2, ap3, ap4;
	float mp1, mp2, mp3, mp4;

	QRubberBand * selection;

	selectionbox * sel;
	selectionbox * zsel;

	int xs, ys, zs;
	// mapping
	int col, ord;
	int goff;
	int coff;

	std::vector <GLfloat> dat;
	void *imgdat;

	float alpha;

	// table stuff
	QTableView *tv;
	QStandardItemModel *model;
	QDialog *opfiles;
	QDialog *ppfiles;

	// end table stuff

	int activeaxis;
	int samp, opa;
	QTimer *m_timer;
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
	void timerEvent(QTimerEvent *e) Q_DECL_OVERRIDE;

	void initializeGL() Q_DECL_OVERRIDE;
	void resizeGL(int w, int h) Q_DECL_OVERRIDE;
	void paintGL() Q_DECL_OVERRIDE;

	void initShaders();
	void initTextures();

	vv::TF  tf;

	public slots:

	void showbg(bool show);
	void showsel(bool show);
	void clearsel();

	void onnewtf(vv::TF& test);
	void updateselect();
	void onNewAids(intList aids);
	void onNewTime(int time);
	void setorder();
	void setcolor();

	void prepfiles();
	void onNewData();
	void onnewmin(QRgb &color);
	void onnewmax(QRgb &color);
	void onnewbg(QRgb &color);

	void onnewsamp(int test);
	void onnewopa(int test);
	void onnewalpha(int a);
	//    void onNewActivate(int s1,int s2,int t1,int t2, int t3, int t4);
	void onNewActivate(int s1, int s2, float lv, float lh);
	void onNewText(GLuint p1, GLuint p2, GLuint p3, GLuint p4, GLuint v1, GLuint v2, GLuint v3, GLuint v4);

	void onnewminx(float t);
	void onnewmaxx(float t);
	void onnewminvx(float t);
	void onnewmaxvx(float t);

	void onnewminy(float t);
	void onnewmaxy(float t);
	void onnewminvy(float t);
	void onnewmaxvy(float t);

	void onnewminz(float t);
	void onnewmaxz(float t);
	void onnewminvz(float t);
	void onnewmaxvz(float t);

	void onnewminv(float t);
	void onnewmaxv(float t);
	void ShowContextMenu(const QPoint&);

signals:

	//void newAidsLoaded(std::vector<int> &aids);
	void newAidsLoaded(intList);

	void newaxis(int a);

	void newxlimit(float min, float max);
	void newxllimit(float min, float max);
	void newxlab(QString &lab);
	void newxfontsize(float f);

	void newylimit(float min, float max);
	void newyllimit(float min, float max);
	void newylab(QString &lab);
	void newyfontsize(float f);
	void setshowsel(bool show);

private:

	QOpenGLBuffer arrayBuf;
	QBasicTimer timer;
	QOpenGLShaderProgram program;
	QOpenGLShaderProgram offscreen;
	// GeometryEngine geometries;
	float minx, maxx, miny, maxy, minz, maxz,
		vminx, vmaxx, vminy, vmaxy, vminz, vmaxz, vmin, vmax;
	GLuint texture, tft;
	//TrackBall m_trackBalls[3];

	QMatrix4x4 projection;
	float mousex, mousey;

	int gmousex, gmousey; //global pos

	int gomousex, gomousey; //old global pos for select

	int cgomousex, cgomousey; //old global pos for zoom

	QVector2D mousePressPosition;
	QVector3D rotationAxis;
	qreal angularSpeed;
	QQuaternion rotation;
	//QRgb *m_color;
	int xsize, ysize, zsize, count, count2;
	GLuint vboIds2[6];
	GLuint  activevbo[1];
	GLuint p1, p2, p3, p4, v1, v2, v3, v4;
	QRgb m_color;
	QRgb cl_color;
	QRgb cm_color;
};

#endif // PC_H