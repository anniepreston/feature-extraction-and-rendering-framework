#include <sstream>
#include <GLComputeShader.h>
#include <BBox.h>
#include <GLError.h>
#include <chrono>
#include "Voxelizer.h"

using namespace lily;

Voxelizer::Voxelizer()
{
}


Voxelizer::~Voxelizer()
{
}

void Voxelizer::voxelize(const std::vector<lily::vec3f>& particles, int w, int h, int d)
{
	if (!m_tboParticles)
	{
		m_tboParticles = GLTextureBufferObjectRef(new GLTextureBufferObject(GL_RGB32F));
		m_tboParticles->setName("TBO particle position");
		m_tboParticles->setImageAccess(GL_READ_ONLY);
	}
	m_tboParticles->upload(particles.size()*sizeof(vec3f), particles.size(), particles.data());
	//compute bbox of particles.
	

	std::vector<float> volume(w*h*d, 0.0f);
	if (!m_tex3d)
	{
		m_tex3d = GLTexture3DRef(new GLTexture3d(w, h, d, GL_R32F, GL_RED, GL_FLOAT, volume.data()));
		m_tex3d->setName("resampled texture 3d");
		m_tex3d->setImageAccess(GL_READ_WRITE);
		m_tex3d->setImageIsLayered(true);
	}
	BBox bbox;
	for (const vec3f &v: particles)
	{
		bbox << v;
	}

	string shaderFileName = "./shader/resampling.comp";
	GLComputeShader shader("resampling shader", true);
	shader.loadComputeShaderFile(shaderFileName);
	shader.CreateShaders();
	shader.SetSamplerUniform("particles", &*m_tboParticles);
	shader.SetImageUniform("img3d",&*m_tex3d);
	shader.SetFloat3Uniform("pMin", bbox.pMin);
	shader.SetFloat3Uniform("pMax", bbox.pMax);

	int group_x_size = 256;
	int group_y_size = 1;
	int group_z_size = 1;
	int num_group_x = (particles.size() + group_x_size - 1) / group_x_size;
	int num_group_y = 1;
	int num_group_z = 1;

	auto start_time = std::chrono::high_resolution_clock::now();
	shader.UseShaders(num_group_x, num_group_y, num_group_z);
	glFinish();
	auto end_time = std::chrono::high_resolution_clock::now();
	auto time = end_time - start_time;
	std::cout << "\n\nUsed " <<
		std::chrono::duration_cast<std::chrono::milliseconds>(time).count()
		<< "ms to voxelize "<<particles.size()<<" particles on GPU.\n\n";

	shader.ReleaseShader();

	stringstream ss;
	ss << "./resample_" << w << "x" << h << "x" << d << ".bin";

	//--smoothing step--
	m_tex3d = Smoothing(m_tex3d);

	//Dump to file.
	string outFileName = ss.str();

	m_tex3d->bindTexture();
	glGetTexImage(m_tex3d->getTarget(), 0, m_tex3d->getFormat(),
		m_tex3d->getType(), volume.data());

	GLError::glCheckError("***");
	m_tex3d->unbindTexture();

	ofstream ofs(outFileName, ios::binary);
	if (!ofs)
	{
		cerr << "Cannot open file: " << outFileName << endl;
	}
	ofs.write(reinterpret_cast<char*>(volume.data()), sizeof(float)*volume.size());
	ofs.close();
}

GLTexture3DRef Voxelizer::Smoothing(const GLTexture3DRef inTex3d)
{
	int w = inTex3d->getWidth();
	int h = inTex3d->getHeight();
	int d = inTex3d->getDepth();

	std::vector<float> volume(w*h*d, 0.0f);
	GLTexture3DRef
	outTex3d = GLTexture3DRef(new GLTexture3d(w, h, d, GL_R32F, GL_RED, GL_FLOAT, volume.data()));
	outTex3d->setName("smoothed texture 3d");
	outTex3d->setImageAccess(GL_READ_WRITE);
	outTex3d->setImageIsLayered(true);

	string shaderFileName = "./shader/gaussianSmoothing.comp";
	GLComputeShader shader("smoothing shader", true);
	shader.loadComputeShaderFile(shaderFileName);

	shader.CreateShaders();
	int kernelWidth = 12;
	shader.SetSamplerUniform("inImg3d", &*inTex3d);
	shader.SetImageUniform("outImg3d", &*outTex3d);
	shader.SetIntUniform("kernelWidth", kernelWidth);

	int group_x_size = 16;
	int group_y_size = 16;
	int group_z_size = 1;
	int num_group_x = (w + group_x_size - 1) / group_x_size;
	int num_group_y = (h + group_y_size - 1) / group_y_size;
	int num_group_z = (d + group_z_size - 1) / group_z_size;

	auto start_time = std::chrono::high_resolution_clock::now();
	shader.UseShaders(num_group_x, num_group_y, num_group_z);
	glFinish();
	auto end_time = std::chrono::high_resolution_clock::now();
	auto time = end_time - start_time;
	std::cout << "\n\nUsed " <<
		std::chrono::duration_cast<std::chrono::milliseconds>(time).count()
		<< "ms to smooth volume of size " <<w<<"x"<<h<<"x"<<d  << " on GPU.\n\n";
	shader.ReleaseShader();

	return outTex3d;
}
