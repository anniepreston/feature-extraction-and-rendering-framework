#include "../../gitlibs/Lily/glew-1.10.0/include/GL/glew.h"
#include "cosmology.h"
#include <QtWidgets/QApplication>
#include <GL/freeglut.h>
#include <iostream>

#include <cuda_runtime.h>
extern "C" //for cuda
//cudaError_t cuda_main();

struct sciVisData{
	int id;
	float x, y, z;
	float vx, vy, vz;
	float dot, variance;
};

std::vector<sciVisData> binArray;

void parseLine(istringstream& ss)
{
	string s;
	sciVisData current;

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.id = atoi(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.x = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.y = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.z = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.vx = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.vy = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.vz = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.dot = atof(s.c_str());

	if (!getline(ss, s, ' ')) qDebug() << "can't read";
	current.variance = atof(s.c_str());

	binArray.push_back(current);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

    //cudaError_t cuerr = cuda_main();
    //if (cuerr != cudaSuccess) std::cout << "CUDA ERROR: " << cudaGetErrorString( cuerr ) << std::endl;

	typedef std::vector<int> intList;
	qRegisterMetaType<intList>("intList");

	glutInit(&argc, argv);
	cosmology w;
	//MainWindow w;

	//FILE FORMAT CONVERSION
	/*
	QStringList nameFilter("SciVisParticleOutput*");
	QDir directory("./cosmoVis_data_files/");
	QStringList files = directory.entryList(nameFilter);

	QString slash = "/";
	for (int i = 0; i < files.size(); i++){

		files[i] = directory.absolutePath() + slash + files[i];
	}

	for (int i = 0; i < files.size(); i++){
		size_t line_count = 0;
		qDebug() << files[i];
		std::string outfile_name = files[i].toStdString() + ".bin";
		qDebug() << "writing binary file";
		ofstream outfile(outfile_name, ios::binary);
		std::cout << "outfile name" << outfile_name << std::endl;

		std::ifstream input(files[i].toLocal8Bit().data());

		if (!input)
		{
			qDebug() << "cannot open file";
			exit(1);
		}
		string s;
		getline(input, s); //skip column titles
		while (!input.eof())
		{
			if (!getline(input, s)) break;
			istringstream ss((s));
			parseLine(ss);
			line_count++;
		}
	
		std::cout << binArray[0].id << binArray[i].x << binArray[0].y << binArray[0].z << binArray[0].vx << binArray[0].vy << binArray[0].vz;

		outfile.write(reinterpret_cast<const char*>(&line_count), sizeof(line_count));
		outfile.write(reinterpret_cast<const char*>(binArray.data()), sizeof(sciVisData)*binArray.size());
		outfile.close();
		binArray.clear();
	}
	*/

	w.show();
	return a.exec();
}
