#include "C:/Users/vidi/Documents/Visual Studio 2013/Projects/cosmos/gitlibs/Lily/glew-1.10.0/include/GL/glew.h"
#include "mainwindow.h"
#include "glviewwidget.h"
#include "pc.h"
#include "cosmology.h"
#include <QtGui>
#include <QFile>
#include <QHBoxLayout>
#include <QSplitter>
#include <QComboBox>
#include <QGroupBox>
#include <QToolButton>
#include <QCheckBox>

//include pc.h

MainWindow::MainWindow(QMainWindow *parent) :
QMainWindow(parent)
{
	//create merge tree;
	PC *mergeTree = new PC();

	//createTFEditor();
	cosmology* visWidget = new cosmology();
	//GLViewWidget* visWidget = new GLViewWidget(this);

	//setCentralWidget(visWidget);

	this->statusBar()->showMessage(tr("Ready"));
	QProgressBar *loadbar = new QProgressBar();
	loadbar->setFixedSize(QSize(100, 20));
	this->statusBar()->addPermanentWidget(loadbar);
	time_label = new QLabel("t = 80, z = 0.5", this);
	time_label->setStyleSheet("QLabel { color : white; font: bold 16px}");
	this->statusBar()->addPermanentWidget(time_label);

	vSplitter = new QSplitter(parent = 0);
	vSplitter->setOrientation(Qt::Vertical);
	//h2Splitter = new QSplitter(parent = 0);

	/*
	QPalette Pal(palette());
	Pal.setColor(QPalette::Background, Qt::darkGray);
	this->setAutoFillBackground(true);
	this->setPalette(Pal);	

	QComboBox *attribBox = new QComboBox();
	attribBox->addItem(tr("velocity"));
	attribBox->addItem(tr("dot product"));
	attribBox->addItem(tr("variance"));

	QComboBox *displayAttribBox = new QComboBox();
	displayAttribBox->addItem(tr("velocity"));
	displayAttribBox->addItem(tr("dot product"));
	displayAttribBox->addItem(tr("variance"));

	QComboBox *halosOrParticles = new QComboBox();
	halosOrParticles->addItem(tr("halos"));
	halosOrParticles->addItem(tr("particles"));

	QSlider *cutoffSlider = new QSlider(Qt::Horizontal);
	cutoffSlider->setRange(0, 100);
	cutoffSlider->setSingleStep(1);
	cutoffSlider->setValue(50);

	QGroupBox *dataWidget = new QGroupBox(tr(""));
	QToolButton *haloButton = new QToolButton;
	haloButton->setAutoRaise(true);
	QToolButton *particleButton = new QToolButton;
	particleButton->setAutoRaise(true);
	QGridLayout *dataLayout = new QGridLayout;
	colorMap = new QLabel("map to color:");
	visMap = new QLabel("map to visibility:");
	visSlider = new QLabel("visibility cutoff:");
	haloParticle = new QLabel("data type:");
	dataLayout->addWidget(haloParticle, 0, 0);
	dataLayout->addWidget(halosOrParticles, 0, 1);
	dataLayout->addWidget(colorMap, 1, 0);
	dataLayout->addWidget(attribBox, 1, 1);
	dataLayout->addWidget(visMap, 2, 0);
	dataLayout->addWidget(displayAttribBox, 2, 1);
	dataLayout->addWidget(cutoffSlider, 3, 1);
	dataLayout->addWidget(visSlider, 3, 0);
	dataLayout->setColumnStretch(1, 0);
	dataWidget->setLayout(dataLayout);

	QGroupBox *lightingWidget = new QGroupBox(tr(""));

	ambientLabel = new QLabel("ambient:");
	QSlider *ambientSlider = new QSlider(Qt::Horizontal);
	ambientSlider->setRange(0, 150);
	ambientSlider->setSingleStep(1);
	ambientSlider->setValue(50);

	diffuseLabel = new QLabel("diffuse:");
	QSlider *diffuseSlider = new QSlider(Qt::Horizontal);
	diffuseSlider->setRange(0, 150);
	diffuseSlider->setSingleStep(1);
	diffuseSlider->setValue(50);

	QCheckBox *lightingCheckBox = new QCheckBox("Lighting On");

	QGridLayout *hboxLighting = new QGridLayout;
	hboxLighting->addWidget(ambientLabel, 1, 0);
	hboxLighting->addWidget(ambientSlider, 1, 1);
	hboxLighting->addWidget(diffuseLabel, 2, 0);
	hboxLighting->addWidget(diffuseSlider, 2, 1);

	hboxLighting->addWidget(lightingCheckBox, 0, 0);

	lightingWidget->setLayout(hboxLighting);

	QTabWidget *tabWidget = new QTabWidget;
	//tabWidget->addTab(tfEditor, tr("mapping"));
	tabWidget->setMinimumWidth(200);
	tabWidget->addTab(dataWidget, tr("data"));
	tabWidget->addTab(lightingWidget, tr("appearance"));*/
	//tabWidget->addTab(plotView, tr("plot view"));
	//mergeTree->setMinimumHeight(200);
	//mergeTree->setMinimumWidth(1000);

	this->statusBar()->showMessage(tr("Ready"));
	//QProgressBar *loadbar = new QProgressBar();
	//loadbar->setFixedSize(QSize(100, 20));
	this->statusBar()->addPermanentWidget(loadbar);
	time_label = new QLabel("t = 80, z = 0.5", this);
	time_label->setStyleSheet("QLabel { color : white; font: bold 16px}");
	this->statusBar()->addPermanentWidget(time_label);

	//h2Splitter->addWidget(tabWidget);
	//h2Splitter->addWidget(visWidget);
	vSplitter->addWidget(visWidget);
	vSplitter->addWidget(mergeTree);
	//vSplitter->addWidget(mergeTree);
	vSplitter->show();
	//visWidget->show();

	//setCentralWidget(vSplitter);
}
