#ifndef _GLVIEWWIDGET_H_
#define _GLVIEWWIDGET_H_
#include "../../gitlibs/Lily/glew-1.10.0/include/GL/glew.h"
#include <QMouseEvent>
#include <GLTexture2D.h>
#include <GLVertexBufferObject.h>
#include <GLIndexBufferObject.h>
#include <GLVertexArray.h>
#include <GLShader.h>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include "comosparticle.h"
#include <GLTexture1D.h>
#include <GLCamera.h>
#include <QTFEditor.h>
#include <QRenderEffEditor.h>
#include <QTimer>
#include <BBox2D.h>
#include "SciVisData.h"

class GLViewWidget: public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	GLViewWidget(QWidget *parent = 0);
	~GLViewWidget();
	typedef std::vector<int> intList;
	virtual void initializeGL();
	virtual void resizeGL(int w, int h);
	virtual void paintGL();
	virtual void mousePressEvent(QMouseEvent* event);
	virtual void mouseMoveEvent(QMouseEvent* event);
	virtual void mouseReleaseEvent(QMouseEvent* event);
	virtual void wheelEvent(QWheelEvent* event);
	virtual void keyPressEvent(QKeyEvent* event);
	virtual void mouseDoubleClickEvent(QMouseEvent *event);
	std::vector<lily::vec3f> getCurrentVel();
	std::vector<lily::vec2f> getCurrentMS();
	std::vector<lily::vec3f> match_positions;

    bool subsetOn;
    int time_index;

protected:
	void createVBO();
	void createShader();
	void drawGUI();
	void selection();

	std::string attribute;
    int attribIndex;
    int data_int;

signals:
	void particles_3D_selected(intList);

public slots:
	void slot_changeBGColor(const QColor& newbg);
	void slot_transfer1DChanged(float* colorMap, bool immediate);
	void slot_enablebBoundingBoxChanged(bool);
	void changeAttribute(int attribInput);
	void slot_spacingValueChanged(float v);
	void slot_alphaFallOffChanged(float v);
	void slot_forwardTime();

	void slot_toggle_selection(bool);
	void slot_toggle_deselection(bool);
	void slot_toggle_camera(bool);

	void haloSubset(intList);
	void receiveTime(int);
	void changeDataType(int);

protected:
	void loadCosmosParticle( const std::string filename, std::vector<ComosParticle> &particles);
	void loadSciVisParticles(const std::string filename, std::vector<SciVisParticles> &particles);
	void createTFEditor();
	void depthSort(const vec3f &sortvector);

protected:
	enum ActionMode
	{
		Selection, FreeCamera
	}m_actionMode;

	int  window_width, window_height;
	lily::GLTexture2DRef m_tex2d;
	lily::GLVertexBufferObjectRef m_vboId;
	lily::GLVertexBufferObjectRef m_vbopos;
	lily::GLVertexBufferObjectRef m_vbovel;
	lily::GLVertexBufferObjectRef m_vboms; //Annie
	lily::GLVertexArrayObjectRef  m_vao;
	lily::GLVertexArrayObjectRef  m_vaoSel;
	lily::GLIndexBufferObjectRef  m_ibo;
	lily::GLShaderRef m_shaderPointSprite;
	lily::GLShaderRef m_shaderSelection;
	lily::mat4 m_mvm, m_prj;
	lily::GLCamera m_camera;

	float m_pointsize;

	//tf
	//1D transfer function
	std::shared_ptr<QTFEditor> m_tfEditor;
	std::shared_ptr<QRenderEffEditor> m_renderEffEditor;
	lily::GLTexture1DRef m_tfTex;
	int m_colorMapRes;
	lily::vec3f m_bgColorf3;
	bool m_bdrawReferenceGrid;
	bool m_bDepthSort; //TESTING
	bool m_bPrintIbo;
	//
	QTimer *m_timer;
	int m_bSelected;
	lily::BBox2D m_selRec;//selection rectangle on screen.
	lily::GLAtomicCounterRef m_atomicCounter;
	lily::GLShaderStorageBufferObjectRef m_ssboSelIndices;
	lily::GLShaderStorageBufferObjectRef m_ssboSelStatus;
};

#endif // QMAINGLWIDGET_H
