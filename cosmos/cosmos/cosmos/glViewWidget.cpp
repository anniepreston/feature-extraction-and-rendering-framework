#include <iostream>
#include <vector>
#include <vec4f.h>
#include "GLGizmo.h"
#include "glviewwidget.h"
#include "QPainter"
#include "comosparticle.h"
#include "GL/freeglut.h"
#include <BBox.h>
#include "helper_math.h"
#include "cuda_kernel.cuh"
#include "cosmology.h"
#include "Voxelizer.h"
#include "cuda.h"
#include "cuda_runtime_api.h"

using namespace std;
using namespace lily;

const string vertShaderFile    = "./shader/pointSprite.vert";
const string fragShaderFile    = "./shader/pointSprite.frag";
const string selVertShaderFile = "./shader/selection.vert";
const string selFragShaderFile = "./shader/selection.frag";
const string camera_path_file  = "./camera_path.bin";

std::vector<GLint> selArray;
std::vector<GLuint> idArray;
std::vector<lily::vec3f> posArray;
std::vector<lily::vec3f> velArray;
std::vector<lily::vec2f> MSArray; //Annie: multistreaming features

GLViewWidget::GLViewWidget(QWidget *parent)
	: QOpenGLWidget(parent)
	, m_pointsize(1.0f), m_colorMapRes(1024), m_bdrawReferenceGrid(true)
	, m_bDepthSort(true), m_bPrintIbo(false)
{
	setFocusPolicy(Qt::StrongFocus);
	m_timer = new QTimer();
	connect(m_timer, SIGNAL(timeout()), this, SLOT(slot_forwardTime()));

    //moving initializations from header file:
    subsetOn = false;
    time_index = 80;
    attribIndex = -1;
    data_int = 0;
}

GLViewWidget::~GLViewWidget()
{

}
void GLViewWidget::mouseDoubleClickEvent(QMouseEvent *e) {
	cout << __func__ << endl;
	QWidget::mouseDoubleClickEvent(e);
	cosmology *mainwnd = dynamic_cast<cosmology*>(parent()->parent());
	if (mainwnd->isFullScreen()) {
		mainwnd->setWindowState(Qt::WindowMaximized);
	}
	else {
		mainwnd->setWindowState(Qt::WindowFullScreen);
	}
	repaint();
}
void GLViewWidget::initializeGL()
{
	makeCurrent();
	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		cerr << "glew initialization failed."
			<<glewGetErrorString(error)<<endl;
		exit(1);
	}
	
	int deviceCount = 0;
	cudaError err = cudaGetDeviceCount(&deviceCount);
	if (err != CUDA_SUCCESS)
	{
		cerr << "CUDA error:" << err << endl;
		exit(1);
	}
	cout << "CUDA device count=" << deviceCount << endl;

	float inertia = 1.0f;//[0,1]
	m_camera.setLookAt(vec3f(0, 0, 0), vec3f(0, 0, -1));
	m_camera.setRotationInertia(inertia);
	m_camera.setTranslateInertia(vec3f(inertia));
	m_camera.setInterpRes(100);
	ifstream ifs(camera_path_file, ios::binary);
	if (ifs)
	{//load camera path file if it exists.
		cout << "Load camera path file :" << camera_path_file << endl;
		m_camera.load(camera_path_file, ios::binary);
	}
	ifs.close();

	glClearColor(0, 0, 0, 1);
	createTFEditor();
	createShader();
	createVBO();
}

void GLViewWidget::resizeGL(int w, int h)
{
	makeCurrent();

	window_width  = w;
	window_height = h;
	glViewport(0, 0, w, h);
	float ext = 2.0f;
	float fov = 60.0f;
	float aspect = (float)w / (float)h;
	float n = 0.01f;
	float f = 100.0f;
	m_camera.setFrustum(fov, (float)w / h, n, f);

	m_prj = mat4::createPerpProjMatrix(fov, aspect, n, f);

	m_mvm.identity();
	m_mvm.translate(0, 0, -5.0f);

	if (!m_shaderPointSprite)
	{
		createShader();
	}
	vec2f scr_dim(w, h);
	m_shaderPointSprite->SetMatrixUniform("mvm", m_mvm);
	m_shaderPointSprite->SetMatrixUniform("prj", m_prj);
	m_shaderPointSprite->SetFloatUniform("screen_diagnoal", scr_dim.length());

	glMatrixMode(GL_PROJECTION);
	glLoadTransposeMatrixf(m_camera.getProjectionMatrix().get());
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GLViewWidget::paintGL()
{
	makeCurrent();
	qDebug() << "pgl test1";

	glClearColor(m_bgColorf3.r(), m_bgColorf3.g(), m_bgColorf3.b(), 0.0f);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	m_camera.forwardTime();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//We discard OpenGL build-in matrix stack and use our own mat4.
	m_mvm.identity();
	glLoadTransposeMatrixf(m_camera.getViewingMatrix().get());
	m_mvm = m_camera.getViewingMatrix() * m_mvm;

	if (m_bdrawReferenceGrid)
	{
		GLGizmo::glDrawGrid();
		GLGizmo::glDrawAxis(2.0f);
		GLGizmo::glDrawBoundingBox(lily::BBox(vec3f(-1.0f), vec3f(1.0f)));
	}
	glDepthMask(GL_FALSE);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_BLEND);
	//Back-to-Front Blending
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//Front-to-Back Blending
	//glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_ONE);
	glPointSize(m_pointsize);
	glEnable(GL_PROGRAM_POINT_SIZE);

	vec3f sortvector = m_camera.getTarget() - m_camera.getCurrentPosition();

	depthSort(sortvector);
	qDebug() << "pgl test2";
    cout << "m_selRec=" << m_selRec;
	m_shaderPointSprite->SetIntUniform("attribIndex", attribIndex);
	m_shaderPointSprite->SetFloat2Uniform("selRecMin", m_selRec.pMin);
	m_shaderPointSprite->SetFloat2Uniform("selRecMax", m_selRec.pMax);
	m_shaderPointSprite->SetFloat3Uniform("camera_pos", m_camera.getCurrentPosition());
	m_shaderPointSprite->SetMatrixUniform("mvm", m_mvm);
	qDebug() << "pgl test3";

	m_shaderPointSprite->UseShaders();
	m_ibo->draw();
	m_shaderPointSprite->ReleaseShader();
	glDepthMask(GL_TRUE);
	/*
	drawGUI();
	QPainter painter(this);
	painter.setPen(QPen(Qt::green, 3, Qt::SolidLine, Qt::RoundCap));
	painter.drawRect(QRect(10, 10, 30, 30));
	*/
}

void GLViewWidget::drawGUI()
{
	lily::GLGizmo::glDrawBoundingBox(m_selRec, GL_LINE);
}

void GLViewWidget::selection()
{
	makeCurrent();
	if (!m_atomicCounter)
	{
		m_atomicCounter = GLAtomicCounterRef(new GLAtomicCounter("selected particle count"));
		m_atomicCounter->alloc(1, 0);
		m_shaderSelection->SetAtomicCounterUniform("ac", m_atomicCounter);
	}

	if (!m_vaoSel)
	{
		m_vaoSel = GLVertexArrayObjectRef(new GLVertexArrayObject(GL_POINTS));
		GLint progId = m_shaderSelection->getProgramId();
		m_vaoSel->addAttribute(progId, "in_pos", GL_FLOAT, 3, sizeof(vec3f), 0, false, m_vbopos);
		m_vaoSel->addAttribute(progId, "in_id", GL_INT, 1, sizeof(GLint), 0, false, m_vboId);
		m_vaoSel->enable();
	}
	m_atomicCounter->reset(0);

	if (!m_ssboSelIndices){
		m_ssboSelIndices = lily::GLShaderStorageBufferObjectRef(new lily::GLShaderStorageBufferObject("selection index"));
		m_shaderSelection->SetShaderStorageBlockUniform("IndexArray", m_ssboSelIndices);
	}

	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glPointSize(1);
	glEnable(GL_PROGRAM_POINT_SIZE);
	glDepthMask(GL_FALSE);
	glEnable(GL_POINT_SPRITE);
	
	glPointSize(1.0);
	m_shaderSelection->SetIntUniform("bSeleted", m_bSelected);
	m_shaderSelection->SetIntUniform("stage", 0);
	m_shaderSelection->SetFloat2Uniform("selRecMin", m_selRec.pMin);
	m_shaderSelection->SetFloat2Uniform("selRecMax", m_selRec.pMax);
	m_shaderSelection->SetMatrixUniform("mvm", m_mvm);
	m_shaderSelection->SetMatrixUniform("prj", m_prj);
	m_shaderSelection->UseShaders();
	m_vaoSel->draw();
	m_shaderSelection->ReleaseShader();
	GLuint count = 0;
	m_atomicCounter->readCounters(&count);
	cout << count << " particle selected!\n";
	
	intList selected_indices;
	std::vector<int> selected_ids;
	if (count>0)
	{
		//allocate a GPU buffer of count*sizeof(GLint) bytes.
		m_ssboSelIndices->upload(count*sizeof(GLint), NULL);

		m_atomicCounter->reset(0);
		m_shaderSelection->SetIntUniform("stage", 1);
		m_shaderSelection->UseShaders();
		m_vaoSel->draw();
		m_shaderSelection->ReleaseShader();

		m_atomicCounter->readCounters(&count);
		cout << count << " selected particle indices result written!\n";
		GLint* ptr = (GLint*)m_ssboSelIndices->map(GL_READ_ONLY);
		//print out selected index
		for (int i = 0; i < count; i++)
		{
			selected_ids.push_back(ptr[i]);
			if (i < 10)
			{
				cout << "index[" << i << "]=" << ptr[i] << endl;
			}
		}
		for (int i = count - 1; i >= 0; i--)
		{
			if (i >= count - 10)
			{
				cout << "index[" << i << "]=" << ptr[i] << endl;
			}
		}
		m_ssboSelIndices->unmap();
	}
	glDepthMask(GL_TRUE);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

	//map to halo indices:
	/*
	QString halo_files[] = { "SciVisHaloOutput_0", "SciVisHaloOutput_1", "SciVisHaloOutput_2", "SciVisHaloOutput_3", "SciVisHaloOutput_4", "SciVisHaloOutput_5", "SciVisHaloOutput_6", "SciVisHaloOutput_7", "SciVisHaloOutput_8", "SciVisHaloOutput_9",
		"SciVisHaloOutput_10", "SciVisHaloOutput_11", "SciVisHaloOutput_12", "SciVisHaloOutput_13", "SciVisHaloOutput_14", "SciVisHaloOutput_15", "SciVisHaloOutput_16", "SciVisHaloOutput_17", "SciVisHaloOutput_18", "SciVisHaloOutput_19",
		"SciVisHaloOutput_20", "SciVisHaloOutput_21", "SciVisHaloOutput_22", "SciVisHaloOutput_23", "SciVisHaloOutput_24", "SciVisHaloOutput_25", "SciVisHaloOutput_26", "SciVisHaloOutput_27", "SciVisHaloOutput_28", "SciVisHaloOutput_29",
		"SciVisHaloOutput_30", "SciVisHaloOutput_31", "SciVisHaloOutput_32", "SciVisHaloOutput_33", "SciVisHaloOutput_34", "SciVisHaloOutput_35", "SciVisHaloOutput_36", "SciVisHaloOutput_37", "SciVisHaloOutput_38", "SciVisHaloOutput_39",
		"SciVisHaloOutput_40", "SciVisHaloOutput_41", "SciVisHaloOutput_42", "SciVisHaloOutput_43", "SciVisHaloOutput_44", "SciVisHaloOutput_45", "SciVisHaloOutput_46", "SciVisHaloOutput_47", "SciVisHaloOutput_48", "SciVisHaloOutput_49",
		"SciVisHaloOutput_50", "SciVisHaloOutput_51", "SciVisHaloOutput_52", "SciVisHaloOutput_53", "SciVisHaloOutput_54", "SciVisHaloOutput_55", "SciVisHaloOutput_56", "SciVisHaloOutput_57", "SciVisHaloOutput_58", "SciVisHaloOutput_59",
		"SciVisHaloOutput_60", "SciVisHaloOutput_61", "SciVisHaloOutput_62", "SciVisHaloOutput_63", "SciVisHaloOutput_64", "SciVisHaloOutput_65", "SciVisHaloOutput_66", "SciVisHaloOutput_67", "SciVisHaloOutput_68", "SciVisHaloOutput_69",
		"SciVisHaloOutput_70", "SciVisHaloOutput_71", "SciVisHaloOutput_72", "SciVisHaloOutput_73", "SciVisHaloOutput_74", "SciVisHaloOutput_75", "SciVisHaloOutput_76", "SciVisHaloOutput_77", "SciVisHaloOutput_78", "SciVisHaloOutput_79",
		"SciVisHaloOutput_80", "SciVisHaloOutput_81", "SciVisHaloOutput_82", "SciVisHaloOutput_83", "SciVisHaloOutput_84", "SciVisHaloOutput_85", "SciVisHaloOutput_86", "SciVisHaloOutput_87", "SciVisHaloOutput_88" };

	std::vector<SciVisParticles> particles;
	QString current_file;
	qDebug() << "data int:" << data_int;
	current_file = halo_files[time_index];
	std::string SVfilename = "./cosmoVis_data_files/" + current_file.toStdString() + ".bin";
	loadSciVisParticles(SVfilename, particles);

	for (int i = 0; i < selected_ids.size(); i++)
	{
		selected_indices.push_back(particles[selected_ids[i]].i);
		qDebug() << particles[selected_ids[i]].i;
	}
	emit particles_3D_selected(selected_indices);
	*/
}

//depth sort particle
void GLViewWidget::depthSort(const vec3f &sortvector)
{
	if (!m_bDepthSort)
	{
		return;
	}
	if (m_bPrintIbo)
	{
		uint* iboPtr = (uint*)m_ibo->map(GL_READ_ONLY);
		cout << "before depth sorting.\n";
		int count = min(10, m_vbopos->getVertexCount());
		for (int i = 0; i < count; i++)
		{
			cout << i << "=" << iboPtr[i] << endl;
		}
		m_ibo->unmap();
	}


	//m_pos.map();
	//m_indices.map();
	m_vbopos->registerToCudaResource(cudaGraphicsMapFlagsReadOnly);
	m_ibo->registerToCudaResource( cudaGraphicsMapFlagsWriteDiscard);
	float3* cuda_vbo_ptr = (float3*)m_vbopos->getCudaMappedPointer();
	uint* cuda_ibo_ptr   = (uint*)m_ibo->getCudaMappedPointer();
	// calculate depth
	calcDepth(cuda_vbo_ptr,//m_pos.getDevicePtr(), 
		//m_sortKeys.getDevicePtr(),
		cuda_ibo_ptr,//m_indices.getDevicePtr(),
		make_float3(sortvector.x(),sortvector.y(),sortvector.z()), 
		m_vbopos->getVertexCount());

	// radix sort
	sortParticles(
		//m_sortKeys.getDevicePtr(), 
		cuda_ibo_ptr,//m_indices.getDevicePtr(),
		m_vbopos->getVertexCount()//m_numParticles
		);
	/*
	*/

	//m_pos.unmap();
	//m_indices.unmap();
	m_ibo->unmapCudaPointer();
	m_ibo->unregisterToCudaResource();
	m_vbopos->unmapCudaPointer();
	m_vbopos->unregisterToCudaResource();
	if (m_bPrintIbo)
	{
		cout << "after depth sorting.\n";
		uint* iboPtr = (uint*)m_ibo->map(GL_READ_ONLY);
		int count = min(10, m_vbopos->getVertexCount());
		for (int i = 0; i < count; i++)
		{
			cout << i << "=" << iboPtr[i] << endl;
		}
		m_ibo->unmap();
	}

}

void GLViewWidget::loadCosmosParticle(
	const std::string filename,
	std::vector<ComosParticle> &particles)
{
	/*Debugging depth sorting.
	particles.resize(2);
	particles[0].x = 0.0;
	particles[0].y = 0.0;
	particles[0].z = 0.0;

	particles[1].x = 0.0;
	particles[1].y = 0.0;
	particles[1].z = -1.0;
	particles[1].vx = -1.0;
	return;
	*/
	//
	std::ifstream ifs;
	ifs.open(filename, std::ios::binary);
	if (!ifs)
	{
		cerr << "Cannot open file: " << filename << endl;
		exit(1);
	}
	size_t count = 0;
	ifs.read(reinterpret_cast<char*>(&count), sizeof(count));
	particles.resize(count);
	ifs.read(reinterpret_cast<char*>(particles.data()),
		particles.size()*sizeof(ComosParticle));
	ifs.close();

	for (int i = 0; i < 10; i++)
	{
		std::cout << particles[i];
	}
	cout << "Total of " << particles.size() << " particles\n";
	 
}

void GLViewWidget::loadSciVisParticles(
	const std::string filename,
	std::vector<SciVisParticles> &particles)
{
	std::ifstream ifs;
	ifs.open(filename, std::ios::binary);
	if (!ifs)
	{
		cerr << "Cannot open file: " << filename << std::endl;
	}

	size_t count = 0;
	ifs.read(reinterpret_cast<char*>(&count), sizeof(count));
	std::cout << "resizing particle array\n" << std::endl;
	std::cout << "count: " << count << std::endl;
	particles.resize(count);
	std::cout << "reading particles \n" << std::endl;
	ifs.read(reinterpret_cast<char*>(particles.data()),
		particles.size()*sizeof(SciVisParticles));
	ifs.close();
}

void GLViewWidget::createVBO()
{
	std::cout << "creating VBO: \n" << std::endl;
	
	/*
	std::string filename = "../../data/particleData_n.bin";
	std::vector<ComosParticle> particles;
	loadCosmosParticle(filename, particles);
	*/

	idArray.clear();
	posArray.clear();
	velArray.clear();
	MSArray.clear();

	std::vector<SciVisParticles> particles;
	QString particle_files[] = { "SciVisParticleOutput_0", "SciVisParticleOutput_1", "SciVisParticleOutput_2", "SciVisParticleOutput_3", "SciVisParticleOutput_4", "SciVisParticleOutput_5", "SciVisParticleOutput_6", "SciVisParticleOutput_7", "SciVisParticleOutput_8", "SciVisParticleOutput_9",
		"SciVisParticleOutput_10", "SciVisParticleOutput_11", "SciVisParticleOutput_12", "SciVisParticleOutput_13", "SciVisParticleOutput_14", "SciVisParticleOutput_15", "SciVisParticleOutput_16", "SciVisParticleOutput_17", "SciVisParticleOutput_18", "SciVisParticleOutput_19",
		"SciVisParticleOutput_20", "SciVisParticleOutput_21", "SciVisParticleOutput_22", "SciVisParticleOutput_23", "SciVisParticleOutput_24", "SciVisParticleOutput_25", "SciVisParticleOutput_26", "SciVisParticleOutput_27", "SciVisParticleOutput_28", "SciVisParticleOutput_29",
		"SciVisParticleOutput_30", "SciVisParticleOutput_31", "SciVisParticleOutput_32", "SciVisParticleOutput_33", "SciVisParticleOutput_34", "SciVisParticleOutput_35", "SciVisParticleOutput_36", "SciVisParticleOutput_37", "SciVisParticleOutput_38", "SciVisParticleOutput_39",
		"SciVisParticleOutput_40", "SciVisParticleOutput_41", "SciVisParticleOutput_42", "SciVisParticleOutput_43", "SciVisParticleOutput_44", "SciVisParticleOutput_45", "SciVisParticleOutput_46", "SciVisParticleOutput_47", "SciVisParticleOutput_48", "SciVisParticleOutput_49",
		"SciVisParticleOutput_50", "SciVisParticleOutput_51", "SciVisParticleOutput_52", "SciVisParticleOutput_53", "SciVisParticleOutput_54", "SciVisParticleOutput_55", "SciVisParticleOutput_56", "SciVisParticleOutput_57", "SciVisParticleOutput_58", "SciVisParticleOutput_59",
		"SciVisParticleOutput_60", "SciVisParticleOutput_61", "SciVisParticleOutput_62", "SciVisParticleOutput_63", "SciVisParticleOutput_64", "SciVisParticleOutput_65", "SciVisParticleOutput_66", "SciVisParticleOutput_67", "SciVisParticleOutput_68", "SciVisParticleOutput_69",
		"SciVisParticleOutput_70", "SciVisParticleOutput_71", "SciVisParticleOutput_72", "SciVisParticleOutput_73", "SciVisParticleOutput_74", "SciVisParticleOutput_75", "SciVisParticleOutput_76", "SciVisParticleOutput_77", "SciVisParticleOutput_78", "SciVisParticleOutput_79",
		"SciVisParticleOutput_80", "SciVisParticleOutput_81", "SciVisParticleOutput_82", "SciVisParticleOutput_83", "SciVisParticleOutput_84", "SciVisParticleOutput_85", "SciVisParticleOutput_86", "SciVisParticleOutput_87", "SciVisParticleOutput_88", "SciVisParticleOutput_89",
		"SciVisParticleOutput_90", "SciVisParticleOutput_91", "SciVisParticleOutput_92", "SciVisParticleOutput_93", "SciVisParticleOutput_94", "SciVisParticleOutput_95", "SciVisParticleOutput_96", "SciVisParticleOutput_97", "SciVisParticleOutput_98" };

	QString halo_files[] = { "SciVisHaloOutput_0", "SciVisHaloOutput_1", "SciVisHaloOutput_2", "SciVisHaloOutput_3", "SciVisHaloOutput_4", "SciVisHaloOutput_5", "SciVisHaloOutput_6", "SciVisHaloOutput_7", "SciVisHaloOutput_8", "SciVisHaloOutput_9",
		"SciVisHaloOutput_10", "SciVisHaloOutput_11", "SciVisHaloOutput_12", "SciVisHaloOutput_13", "SciVisHaloOutput_14", "SciVisHaloOutput_15", "SciVisHaloOutput_16", "SciVisHaloOutput_17", "SciVisHaloOutput_18", "SciVisHaloOutput_19",
		"SciVisHaloOutput_20", "SciVisHaloOutput_21", "SciVisHaloOutput_22", "SciVisHaloOutput_23", "SciVisHaloOutput_24", "SciVisHaloOutput_25", "SciVisHaloOutput_26", "SciVisHaloOutput_27", "SciVisHaloOutput_28", "SciVisHaloOutput_29",
		"SciVisHaloOutput_30", "SciVisHaloOutput_31", "SciVisHaloOutput_32", "SciVisHaloOutput_33", "SciVisHaloOutput_34", "SciVisHaloOutput_35", "SciVisHaloOutput_36", "SciVisHaloOutput_37", "SciVisHaloOutput_38", "SciVisHaloOutput_39",
		"SciVisHaloOutput_40", "SciVisHaloOutput_41", "SciVisHaloOutput_42", "SciVisHaloOutput_43", "SciVisHaloOutput_44", "SciVisHaloOutput_45", "SciVisHaloOutput_46", "SciVisHaloOutput_47", "SciVisHaloOutput_48", "SciVisHaloOutput_49",
		"SciVisHaloOutput_50", "SciVisHaloOutput_51", "SciVisHaloOutput_52", "SciVisHaloOutput_53", "SciVisHaloOutput_54", "SciVisHaloOutput_55", "SciVisHaloOutput_56", "SciVisHaloOutput_57", "SciVisHaloOutput_58", "SciVisHaloOutput_59",
		"SciVisHaloOutput_60", "SciVisHaloOutput_61", "SciVisHaloOutput_62", "SciVisHaloOutput_63", "SciVisHaloOutput_64", "SciVisHaloOutput_65", "SciVisHaloOutput_66", "SciVisHaloOutput_67", "SciVisHaloOutput_68", "SciVisHaloOutput_69",
		"SciVisHaloOutput_70", "SciVisHaloOutput_71", "SciVisHaloOutput_72", "SciVisHaloOutput_73", "SciVisHaloOutput_74", "SciVisHaloOutput_75", "SciVisHaloOutput_76", "SciVisHaloOutput_77", "SciVisHaloOutput_78", "SciVisHaloOutput_79",
		"SciVisHaloOutput_80", "SciVisHaloOutput_81", "SciVisHaloOutput_82", "SciVisHaloOutput_83", "SciVisHaloOutput_84", "SciVisHaloOutput_85", "SciVisHaloOutput_86", "SciVisHaloOutput_87", "SciVisHaloOutput_88" };

	QString current_file;
	qDebug() << "data int:" << data_int;
	if (data_int == 0)
	{
		current_file = particle_files[time_index + 10];
	}
	else if (data_int == 1)
	{
		current_file = halo_files[time_index];
	}
	
	//QString current_file = particle_files[time_index + 10];
	qDebug() << "data file" << current_file << "Loading";

	std::cout << "reading scivis particles" << std::endl;
	std::string SVfilename = "./cosmoVis_data_files/" + current_file.toStdString() + ".bin";
	loadSciVisParticles(SVfilename, particles);
	
	int count = particles.size();
	std::cout << "testing VBO count: " << count << std::endl;
	selArray.resize(count, 0);
	 idArray.resize(count);
	posArray.resize(count);
	velArray.resize(count);
	MSArray.resize(count); //annie: multistreaming

	allocSortkeyCUDA(count);

	for (int i = 0; i < count; i++)
	{
		posArray[i] = lily::vec3f((particles[i].x - 32)/32., (particles[i].y - 32)/32., (particles[i].z - 32)/32.);
		//qDebug() << (particles[i].x - 32) / 32. << (particles[i].y - 32) / 32. << (particles[i].z - 32) / 32.;
	}

	int extent = 32;
	int w =  extent;
	int h =  extent;
	int d =  extent;

	Voxelizer vo;
	vo.voxelize(posArray, w, h, d);

	for (int i = 0; i < count; i++) { idArray[i] = i; }	

	int test_match_count;

	for (int i = 0; i < count; i++)
	{
		int match_count = 0;
		if (subsetOn == false)
		{
			velArray[i] = lily::vec3f(particles[i].vx, particles[i].vy, particles[i].vz);
			//velArray[i] = vec3f(-1.0);
		}
		else if (subsetOn == true)
		{
			if (particles[i].vx > 100)
			{
				velArray[i] = lily::vec3f(-1.0);
			}
			else{
				velArray[i] = lily::vec3f(particles[i].vx, particles[i].vy, particles[i].vz);
			}
			/*
			for (int j = 0; j < match_positions.size(); j++)
			{
				float dist = sqrtf(pow((particles[i].x - 32.) / 32 - match_positions[j].x(), 2) +
					pow((particles[i].y - 32.) / 32 - match_positions[j].y(), 2) +
					pow((particles[i].z - 32.) / 32 - match_positions[j].z(), 2));
				if (dist < 0.1)
				{
					match_count++;
				}

			}
			if (match_count > 0)
			{
				//qDebug() << "vel set to -1!";
				test_match_count++;
			}
			else { velArray[i] = lily::vec3f(particles[i].vx, particles[i].vy, particles[i].vz); }
			*/
		}

	}

	for (int i = 0; i < count; i++){
		MSArray[i] = lily::vec2f(particles[i].dot, particles[i].variance); //Annie
	}

	if (!m_ssboSelStatus){
		m_ssboSelStatus = lily::GLShaderStorageBufferObjectRef(new lily::GLShaderStorageBufferObject("select status"));
		m_ssboSelStatus->upload(sizeof(int)*selArray.size(), selArray.data());
	}

	m_shaderPointSprite->SetShaderStorageBlockUniform("SelectStatusArray", m_ssboSelStatus);
	m_shaderSelection->SetShaderStorageBlockUniform("SelectStatusArray", m_ssboSelStatus);

	//if (!m_vboId)
	//{
	m_vboId = GLVertexBufferObjectRef(new GLVertexBufferObject());
	qDebug() << "uploading ID VBO";
	m_vboId->upload(idArray.size()*sizeof(GLint), idArray.size(), idArray.data());
	//}

	//if (!m_vbopos)
	//{
	m_vbopos = GLVertexBufferObjectRef(new GLVertexBufferObject());
	m_vbopos->upload(posArray.size()*sizeof(vec3f), posArray.size(), posArray.data());
	//}
	//if (!m_vbovel)
	//{
	m_vbovel = GLVertexBufferObjectRef(new GLVertexBufferObject());
	m_vbovel->upload(velArray.size()*sizeof(vec3f), velArray.size(), velArray.data());
	//}

	//if (!m_vboms) //Annie
	//{
	m_vboms = GLVertexBufferObjectRef(new GLVertexBufferObject());
	m_vboms->upload(MSArray.size()*sizeof(vec2f), MSArray.size(), MSArray.data());
	//}

	if (!m_shaderPointSprite)
	{
		createShader();
	}
	//if (!m_vao)
	//{
	m_vao = GLVertexArrayObjectRef(new GLVertexArrayObject(GL_POINTS));
	GLint progId = m_shaderPointSprite->getProgramId();
	m_vao->addAttribute(progId, "in_pos", GL_FLOAT, 3, sizeof(vec3f), 0, false, m_vbopos);
	m_vao->addAttribute(progId, "in_id", GL_INT, 1, sizeof(int), 0, false, m_vboId);
	m_vao->addAttribute(progId, "in_vel", GL_FLOAT, 3, sizeof(vec3f), 0, false, m_vbovel);
	m_vao->addAttribute(progId, "in_ms", GL_FLOAT, 2, sizeof(vec2f), 0, false, m_vboms);
	m_vao->enable();
	//}
	//if (!m_ibo)
	//{
	m_ibo = GLIndexBufferObjectRef(new GLIndexBufferObject(GL_POINTS));
	m_ibo->attachVAO(m_vao);
	for (int i = 0; i < count; i++)
	{
		*m_ibo << i;
	}
	m_ibo->enable();
	//}
	
	if (!m_tfEditor)
	{
		createTFEditor();
	}

	m_tfEditor->getHistogram()->clear();
	for (size_t i = 0; i < count; i++){

		
		m_tfEditor->incrementHistogram(velArray[i].length());
	}
	m_tfEditor->getQHistogram()->changeYT2Log();
	qDebug() << "done creating VBOs";
}

std::vector<lily::vec3f> GLViewWidget::getCurrentVel()
{
	return velArray;
}

std::vector<lily::vec2f> GLViewWidget::getCurrentMS()
{
	return MSArray;
}

void GLViewWidget::createShader()
{
	qDebug() << "test1";
	if (!m_shaderPointSprite)
	{
		m_shaderPointSprite = GLShaderRef(new GLShader("Point Sprite Shader", true));
		m_shaderPointSprite->loadVertexShaderFile(vertShaderFile);
		m_shaderPointSprite->loadFragShaderFile(fragShaderFile);
		m_shaderPointSprite->CreateShaders();
		qDebug() << "test3";
	}
	float distFalloff  = 2.0f;
	float alphaFalloff = 10.0f;

	qDebug() << "test4";

	m_shaderPointSprite->SetFloatUniform("distFalloff", distFalloff);
	m_shaderPointSprite->SetFloatUniform("afalloff", alphaFalloff);
	m_shaderPointSprite->SetFloatUniform("input_pointsize", 1.0f);

	if (!m_shaderSelection)
	{
		qDebug() << "test5";
		m_shaderSelection = GLShaderRef(new GLShader("Point Selection Shader", true));
		m_shaderSelection->loadVertexShaderFile(selVertShaderFile);
		m_shaderSelection->loadFragShaderFile(selFragShaderFile);
		m_shaderSelection->CreateShaders();
		qDebug() << "test6";
	}
	m_shaderSelection->SetFloatUniform("input_pointsize", 1.0f);
}

void GLViewWidget::mousePressEvent(QMouseEvent* event)
{
	if (event->buttons() == Qt::LeftButton)
	{
		if (m_actionMode == Selection)
		{
			m_selRec = lily::BBox2D();
			m_selRec << vec2f(event->x(), window_height - event->y());
		}else if (m_actionMode == FreeCamera){
			m_camera.mouseButton(event->x(), event->y(), GLUT_RIGHT_BUTTON);
		}
	}
	repaint();
}

void GLViewWidget::mouseMoveEvent(QMouseEvent* event)
{
	if (event->buttons() == Qt::LeftButton){

		cout << "left button move\n";
		if (m_actionMode == Selection)
		{
			m_selRec << vec2f(event->x(), window_height -  event->y());
		}else if (m_actionMode == FreeCamera){
			m_camera.mouseMotion(event->x(), event->y(), GLUT_RIGHT_BUTTON);
		}
	}
	if (event->buttons() == Qt::MidButton){
		cout << "mid button move\n";
	}
	if (event->buttons() == Qt::RightButton){
		cout << "right button move\n";
	}
	repaint();
}

void GLViewWidget::mouseReleaseEvent(QMouseEvent* event)
{
	//if (event->buttons() == Qt::LeftButton)
	{

		if (m_actionMode == Selection)
		{
			m_selRec << vec2f(event->x(), window_height - event->y());
			selection();
			cout << "Done selection!\n";
            m_selRec = lily::BBox2D();
		}
	}
    repaint();
}

void GLViewWidget::wheelEvent(QWheelEvent* event)
{

}

void GLViewWidget::createTFEditor()
{
	m_tfEditor = std::shared_ptr<QTFEditor>(new QTFEditor(m_colorMapRes, this));
	//m_tfEditor = std::shared_ptr<QTFEditor>(new QTFEditor(m_colorMapRes, ui.tab_3));
	m_tfEditor->move(0, 0);

	QColor bgkColor = m_tfEditor->getColorMap()->getBackgroundColor();
	glClearColor(bgkColor.redF(), bgkColor.greenF(), bgkColor.blueF(), bgkColor.alphaF());
	//histogram
	m_tfEditor->show();

	connect(m_tfEditor->getColorMap(), SIGNAL(bgColorChanged(const QColor&)),
			this, SLOT(slot_changeBGColor(const QColor&)));

	connect(m_tfEditor->getTFPanel(), SIGNAL(tfChanged(float*, bool)),
			this, SLOT(slot_transfer1DChanged(float*, bool)));

	QColor m_bgColor = m_tfEditor->getColorMap()->getBackgroundColor();
	m_bgColorf3 = lily::vec3f(m_bgColor.redF(), m_bgColor.greenF(), m_bgColor.blueF());

	cout << "*** Color Map Resolution=" << m_tfEditor->getTFColorMapResolution() << endl;
	if (!m_tfTex)
	{
		m_tfTex = lily::GLTexture1DRef(
			new lily::GLTexture1d(m_tfEditor->getTFColorMapResolution()
			, GL_RGBA32F_ARB, GL_RGBA, GL_FLOAT, m_tfEditor->getTFColorMap()));
		m_tfTex->setName("glviewwidget 1D tf texture");
		cout << "tranfer function resoltion=" << m_tfEditor->getTFColorMapResolution() << endl;
	}
	if (!m_shaderPointSprite)
	{
		createShader();
	}
	m_shaderPointSprite->SetSamplerUniform("texTF", &*m_tfTex);


	m_renderEffEditor = std::shared_ptr<QRenderEffEditor>(new QRenderEffEditor(this));
	m_renderEffEditor->setTotalSteps(500);
	m_renderEffEditor->move(0, m_tfEditor->frameGeometry().height());
	m_renderEffEditor->resize(400, 200);
	m_renderEffEditor->show();

	connect(&*m_renderEffEditor, SIGNAL(boundingBoxChanged(bool)), this, SLOT(slot_enablebBoundingBoxChanged(bool)));
	connect(&*m_renderEffEditor, SIGNAL(spacingValueChanged(float)), this, SLOT(slot_spacingValueChanged(float)));
	connect(&*m_renderEffEditor, SIGNAL(ambientValueChanged(float)), this, SLOT(slot_alphaFallOffChanged(float)));
	/*
	connect(&*m_renderEffEditor, SIGNAL(mainAxisChanged(bool)), this, SLOT(slot_mainAxisChanged(bool)));
	connect(&*m_renderEffEditor, SIGNAL(sideAxisChanged(bool)), this, SLOT(slot_sideAxisChanged(bool)));
	connect(&*m_renderEffEditor, SIGNAL(lightGroupChanged(bool)), this, SLOT(slot_lightEnableChanged(bool)));
	connect(&*m_renderEffEditor, SIGNAL(diffuseValueChanged(float)), this, SLOT(slot_diffuseValueChanged(float)));
	connect(&*m_renderEffEditor, SIGNAL(specularValueChanged(float)), this, SLOT(slot_specularValueChanged(float)));
	connect(&*m_renderEffEditor, SIGNAL(shininessValueChanged(float)), this, SLOT(slot_shininessValueChanged(float)));
	*/

}

void GLViewWidget::keyPressEvent(QKeyEvent* event)
{
	cout << __FUNCTION__ << endl;
	float stepsize = 0.01f;

	switch (event->key()){
		case Qt::Key_F10:{
			m_shaderPointSprite->refresh();
			break;
		}
		case Qt::Key_C:{
			break;
		}
		case Qt::Key_W:
			m_camera.moveForward(stepsize);
			break;
		case Qt::Key_S:
			m_camera.moveForward(-stepsize);
			break;
		case Qt::Key_D:
			m_camera.moveRight(stepsize);
			break;
		case Qt::Key_A:
			m_camera.moveRight(-stepsize);
			break;
		case Qt::Key_Space:
			m_camera.moveUp(stepsize);
			break;
		case Qt::Key_X:
			m_camera.moveUp(-stepsize);
			break;
		case Qt::Key_9:{
			m_pointsize = std::max(m_pointsize-1.0f, 1.0f);
			m_shaderPointSprite->SetFloatUniform("input_pointsize", m_pointsize);
			cout << "pointsize=" << m_pointsize << endl;
			break;
		}
		case Qt::Key_0:{
			m_pointsize = std::min(m_pointsize+1.0f, 200.0f);
			m_shaderPointSprite->SetFloatUniform("input_pointsize", m_pointsize);
			cout << "pointsize=" << m_pointsize << endl;
			break;
		}
		case Qt::Key_8:{
			m_bDepthSort = !m_bDepthSort;
			cout << "depth sort=" << m_bDepthSort << endl;
			break;
		}
		case Qt::Key_7:{
			m_bPrintIbo = !m_bPrintIbo;
			break;
		}
		case Qt::Key_1:{
			m_camera.addKeyframe();
			break;
		}
		case Qt::Key_2:{
			m_camera.toggleRoaming(!m_camera.IsRoaming());
			//bRoaming = !bRoaming;
			if (m_camera.IsRoaming()){//only when roaming is enabled, shall we create interpolated curve.
				//camera_path.createCurve();
				m_camera.generatePath();
				size_t nstep = m_camera.getCameraPath().getOutputPtArrayRef().size();
				if (nstep > 0)
				{//create a smooth transition from current position to the
					//start of the camera path.
					//set destination.
					vec3f curve_start = \
						m_camera.getCameraPath().getOutputPtArrayRef()[0];
					m_camera.setDestPosition(curve_start);
					int iStep = 0;
					m_camera.setStep(iStep);
				}
				m_timer->start(20);
			}else{
				//m_camera.setTranslateInertia(vec3f(inertia*0.5f));
				m_timer->stop();
			}
			break;
		}
		case Qt::Key_3:{
			m_camera.save(camera_path_file, ios::binary);
			break;
		}
		case Qt::Key_F3:
		{
			break;
		}
		case Qt::Key_F4:
		{
			break;
		}
		case Qt::Key_F5:
		{
			break;
		}
		case Qt::Key_F6:
		{
			break;
		}
		case Qt::Key_F7:
		{
			break;
		}
	}
	repaint();
}

void GLViewWidget::slot_changeBGColor(const QColor& newbg)
{
	makeCurrent();
	m_bgColorf3 = lily::vec3f(newbg.redF(), newbg.greenF(), newbg.blueF());
	cout << "background color:" << m_bgColorf3;
	glClearColor(m_bgColorf3.r(), m_bgColorf3.g(), m_bgColorf3.b(), 1.0f);
	repaint();
}

void GLViewWidget::slot_transfer1DChanged(float* colorMap, bool immediate)
{
	makeCurrent();
	cout << "tf texture width=" << m_tfTex->getWidth() << endl;
	m_tfTex->upload(m_tfTex->getWidth(), colorMap);
	repaint();
}

void GLViewWidget::slot_enablebBoundingBoxChanged(bool v)
{
	m_bdrawReferenceGrid = v;
	repaint();
}

void GLViewWidget::slot_spacingValueChanged(float v)
{
	v *= 1000.0f;
	m_shaderPointSprite->SetFloatUniform("distFalloff", v);
	cout << "distFalloff=" << v << endl;
	repaint();
}

void GLViewWidget::slot_alphaFallOffChanged(float v)
{
	v *= 100.0f;
	v = std::max(v, 6.0f);
	m_shaderPointSprite->SetFloatUniform("afalloff", v);
	cout << "afalloff=" << v << endl;
	repaint();
}

void GLViewWidget::slot_forwardTime()
{
	size_t nsteps = m_camera.getCameraPath().getOutputPtArrayRef().size();
	int istep = m_camera.getStep();
	if ( istep+1 >= nsteps){
		m_camera.setStep(0);//loop back to starting point.
	}
	repaint();
}

void GLViewWidget::slot_toggle_selection(bool v)
{
	m_actionMode = Selection;
	m_bSelected = v;
	cout <<__func__<< ":m_bSelected=" << m_bSelected << endl;
}

void GLViewWidget::slot_toggle_camera(bool v)
{
	m_actionMode = FreeCamera;
}

void GLViewWidget::slot_toggle_deselection(bool v)
{
	m_actionMode = Selection;
	m_bSelected = !v;
	cout <<__func__<< ":m_bSelected=" << m_bSelected << endl;
}

void GLViewWidget::haloSubset(intList aid)
{
	qDebug() << "number of aIDs received: " << aid.size();
	std::cout << "halo subset signal received!" << std::endl;
	//go through ID list and find positions
	 
	//read halo data for this timestep:
	std::vector<SciVisParticles> halos;
	std::string filename = "./cosmoVis_data_files/SciVisHaloOutput_50.bin";
	std::ifstream ifs;
	ifs.open(filename, std::ios::binary);
	if (!ifs)
	{
		cerr << "Cannot open file: " << filename << std::endl;
	}
	size_t count = 0;
	ifs.read(reinterpret_cast<char*>(&count), sizeof(count));
	halos.resize(count);
	ifs.read(reinterpret_cast<char*>(halos.data()),
		halos.size()*sizeof(SciVisParticles));
	ifs.close();

	//loop through halos> 
	qDebug() << "looking through " << count << "halos";
	int match_counter;
	match_positions.clear();
	/*
	for (int i = 0; i < count; i++)
	{
		//loop through IDs & save any corresponding positions
		for (int j = 0; j < aid.size(); j++)
		{
			if (halos[i].i == aid[j])
			{
				match_counter++;
				match_positions.push_back(vec3f(halos[i].x, halos[i].y, halos[i].z));
			}
			else
			{
			}
			//THIS IS THE CHEATING PART

		}

		if (i < 20)
		{
			match_positions.push_back(vec3f((halos[i].x - 32.)/32., (halos[i].y - 32.)/32., (halos[i].z - 32)/32.));
		}
	}*/
	//qDebug() << match_counter << " matches found:";
	subsetOn = true;
	
	m_vboId.reset();
	m_vbopos.reset();
	m_vbovel.reset();
	m_vboms.reset();
	m_vboId.~shared_ptr();
	m_vbopos.~shared_ptr();
	m_vbovel.~shared_ptr();
	m_vboms.~shared_ptr();
	m_vao.~shared_ptr();
	m_ibo.~shared_ptr();
	//FOR NOW, pretend matches were found:
	qDebug() << "ACTIVATING SUBSET";
	initializeGL();
	repaint();
}

void GLViewWidget::changeAttribute(int attribInput)
{
	std::string attribList[] = { "velocity", "dot product", "variance" };
	attribute = attribList[attribInput];
	attribIndex = attribInput;
	repaint();
}

void GLViewWidget::receiveTime(int timestep)
{
	time_index = timestep;
	//clear stuff:
	m_vboId.reset();
	m_vbopos.reset();
	m_vbovel.reset();
	m_vboms.reset();
	m_vboId.~shared_ptr();
	m_vbopos.~shared_ptr();
	m_vbovel.~shared_ptr();
	m_vboms.~shared_ptr();
	m_vao.~shared_ptr();
	m_ibo.~shared_ptr();

	qDebug() << "timestep received";
	initializeGL();
	repaint();
}

void GLViewWidget::changeDataType(int datatype)
{
	data_int = datatype;

	m_vboId.reset();
	m_vbopos.reset();
	m_vbovel.reset();
	m_vboms.reset();
	m_vboId.~shared_ptr();
	m_vbopos.~shared_ptr();
	m_vbovel.~shared_ptr();
	m_vboms.~shared_ptr();
	m_vao.~shared_ptr();
	m_ibo.~shared_ptr();

	qDebug() << "timestep received";
	initializeGL();
	repaint();
}
