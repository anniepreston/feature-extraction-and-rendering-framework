#ifndef _COMOS_PARTICLE_H_
#define _COMOS_PARTICLE_H_
#include <iostream>
#include <fstream>

struct ComosParticle{
	int i;
	float x, y, z;
	float vx, vy, vz;
	ComosParticle(int _i = -1,
		float _x = 0.0f, float _y = 0.0f, float _z = 0.0f,
		float _vx = 0.0f, float _vy = 0.0f, float _vz = 0.0f) :
		i(_i), x(_x), y(_y), z(_z), vx(_vx), vy(_vy), vz(_vz)
	{
	}
	friend std::ostream& operator << (std::ostream& os, const ComosParticle& p){
		os << p.i << " " << p.x << " " << p.y << " " << p.z << " "
			<< p.vx << " " << p.vy << " " << p.vz << std::endl;
		return os;
	}

	bool operator != (const ComosParticle& p){
		return i != p.i || x != p.x || y != p.y || z != p.z ||
			vx != p.vx || vy != p.vy || vz != p.vz;
	}
};

#endif