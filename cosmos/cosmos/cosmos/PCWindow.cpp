#include "PCWindow.h"
#include "pc.h"
#include <QIcon>
#include "Axis.h"


#include <QHBoxLayout>
#include <QSplitter>

//PCWindow::PCWindow(QMainWindow *parent)
	//: QMainWindow(parent)
PCWindow::PCWindow(QWidget *parent)
	: QWidget(parent)
	//MainWindow::MainWindow(QWidget *parent)
	//    : QWidget(parent)
{
	this->setWindowFlags(Qt::Widget);

	QIcon test = QIcon(":/styles/icon.png");
	this->setWindowIcon(test);

	//QMenu *filemenu = this->menuBar()->addMenu(tr("&File"));


	QAction *loadfiles = new QAction("L&oad Files", this);
	QAction *processfiles = new QAction("&Preprocess Files", this);
	QAction *loadstate = new QAction("&Load State", this);
	QAction *savestate = new QAction("&Save State", this);

	/*
	filemenu->addAction(loadfiles);

	filemenu->addAction(processfiles);

	filemenu->addAction(loadstate);
	filemenu->addAction(savestate);
	*/

	//this->statusBar()->showMessage(tr("Ready"));
	//QProgressBar *loadbar = new QProgressBar();
	//loadbar->setFixedSize(QSize(100, 20));
	//this->statusBar()->addPermanentWidget(loadbar);

	//loadbar->setValue(50);

	this->setMinimumWidth(1200);
	//this->setGeometry(0,0,1600,1200);
	//this->setGeometry(0,0,300,400);
	QPalette Pal(palette());
	// set black background
	Pal.setColor(QPalette::Background, Qt::darkGray);
	this->setAutoFillBackground(true);
	this->setPalette(Pal);


	populateScene();

	//  h1Splitter = new QSplitter;
	h2Splitter = new QSplitter;
	h2Splitter->setOrientation(Qt::Vertical);

	TFEditor * tfe = new TFEditor();

	h2Splitter->addWidget(tfe);

	// v1 splitter is right GL + co-ords etc
	// QSplitter *vSplitter = new QSplitter;

	// vSplitter->setOrientation(Qt::Vertical);
	//Main GL View
	/* View *view = new View("Main View");
	view->view()->setScene(scene);
	v1Splitter->addWidget(view);
	*/

	//widget stuff
	/*
	QGLFormat glFormat;
	glFormat.setVersion( 4, 2 );
	//  glFormat.setProfile( QGLFormat::CoreProfile ); // Requires >=Qt-4.8.0
	glFormat.setSampleBuffers( true );

	QGLFormat::setDefaultFormat(glFormat);
	if(glFormat.openGLVersionFlags() & QGLFormat::OpenGL_Version_1_5) qDebug() << "1.5 or higher";
	if(glFormat.openGLVersionFlags() & QGLFormat::OpenGL_Version_4_2) qDebug() << "4.2 or higher " << "Ver: " << glFormat.majorVersion() << "."<<glFormat.minorVersion();
	if(glFormat.openGLVersionFlags() & QGLFormat::OpenGL_Version_4_3) qDebug() << "4.3 or higher";
	qDebug() << QGLFormat::defaultFormat().majorVersion() << "." << QGLFormat::defaultFormat().minorVersion() ;

	QGLWidget * glWidget=new QGLWidget(glFormat,0);//QGLFormat::defaultFormat(),0);

	glWidget->makeCurrent();
	*/
	//  MainWidget *widget = new MainWidget(this,glWidget);

	//this->setCentralWidget(widget);
	//
	// QGLWidget * glWidget1=new QGLWidget(QGLFormat::defaultFormat(),this,glWidget);




	// vSplitter->addWidget(widget);




	// vSplitter->addWidget(h2Splitter);

	// vSplitter->addWidget(h2Splitter);
	/* vSplitter->resize(.9*this->width() ,this->height());
	h2Splitter->resize(vSplitter->width(),.4*this->height());

	// Controls *
	c = new Controls("Side Bar View");
	c->controls()->setScene(scene);
	h1Splitter->addWidget(c); //side bar
	*/  //h1Splitter->addWidget(vSplitter);

	//   QTabWidget* pTab = new QTabWidget(this);

	//widget->setGeometry(0,0,800,100);
	// pTab->setMinimumHeight(this->height()*.2);
	//  pTab->setGeometry(0,0,this->width()*.5,this->height()*.002);
	//this->setCentralWidget(pTab);

	//  vSplitter->addWidget(pTab);
	/*
	View *view = new View("Interpolator View",this);
	h2Splitter->addWidget(view); // int v

	pTab->addTab(view, "Temporal View");


	connect(loadfiles, SIGNAL(triggered()), c, SLOT(openfiles()));
	connect(processfiles, SIGNAL(triggered()), c, SLOT(prepfiles()));
	connect(loadstate, SIGNAL(triggered()), c, SLOT(loadstate()));
	connect(savestate, SIGNAL(triggered()), c, SLOT(savestate()));


	connect(c,SIGNAL(newstatusmes(QString)),this->statusBar(),SLOT(showMessage(QString)));

	connect(c,SIGNAL(newprog(int)),loadbar,SLOT(setValue(int)));

	connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),view,SLOT(onNewData(std::vector<Series*>&)));
	connect(c,SIGNAL(newDataLoadedset(QSet<int>&)),view,SLOT(onNewDataset(QSet<int>&)));
	connect(c,SIGNAL(newmouse(float,float)),view,SLOT(onnewmouse(float,float)));

	connect(view,SIGNAL(newActivated(int,int,float,float)),c,SLOT(onNewActivate(int,int,float,float)));
	connect(view,SIGNAL(newmouse(float,float)),c,SLOT(onnewmouse(float,float)));

	// widget stuff
	connect(widget,SIGNAL(newrot(QQuaternion,int)),c,SLOT(onnewrot(QQuaternion,int)));

	connect(c,SIGNAL(newrot(QQuaternion,int)),widget,SLOT(onnewrot(QQuaternion,int)));

	connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),widget,SLOT(onNewData(std::vector<Series*>&)));
	//connect(view,SIGNAL(newActivated(int,int,int,int,int,int)),widget,SLOT(onNewActivate(int,int,int,int,int,int)));
	connect(view,SIGNAL(newActivated(int,int,float,float)),widget,SLOT(onNewActivate(int,int,float,float)));
	connect(c,SIGNAL(newoffx(int)),widget,SLOT(onnewoffx(int)));
	connect(c,SIGNAL(newoffy(int)),widget,SLOT(onnewoffy(int)));
	connect(c,SIGNAL(newoffz(int)),widget,SLOT(onnewoffz(int)));

	connect(c,SIGNAL(newparsize(int)),widget,SLOT(onnewpsize(int)));
	connect(c,SIGNAL(newparstate(int)),widget,SLOT(onnewptype(int)));

	connect(c,SIGNAL(newmin(QRgb&)),widget,SLOT(onnewmin(QRgb&)));
	connect(c,SIGNAL(newmax(QRgb&)),widget,SLOT(onnewmax(QRgb&)));

	connect(c,SIGNAL(newmaxx(float)),widget,SLOT(onnewmaxx(float)));
	connect(c,SIGNAL(newmaxy(float)),widget,SLOT(onnewmaxy(float)));
	connect(c,SIGNAL(newmaxz(float)),widget,SLOT(onnewmaxz(float)));

	connect(c,SIGNAL(newminx(float)),widget,SLOT(onnewminx(float)));
	connect(c,SIGNAL(newminy(float)),widget,SLOT(onnewminy(float)));
	connect(c,SIGNAL(newminz(float)),widget,SLOT(onnewminz(float)));

	connect(c,SIGNAL(newmaxvx(float)),widget,SLOT(onnewmaxvx(float)));
	connect(c,SIGNAL(newmaxvy(float)),widget,SLOT(onnewmaxvy(float)));
	connect(c,SIGNAL(newmaxvz(float)),widget,SLOT(onnewmaxvz(float)));

	connect(c,SIGNAL(newminvx(float)),widget,SLOT(onnewminvx(float)));
	connect(c,SIGNAL(newminvy(float)),widget,SLOT(onnewminvy(float)));
	connect(c,SIGNAL(newminvz(float)),widget,SLOT(onnewminvz(float)));

	connect(c,SIGNAL(newmaxv(float)),widget,SLOT(onnewmaxv(float)));
	connect(c,SIGNAL(newminv(float)),widget,SLOT(onnewminv(float)));

	connect(c,SIGNAL(newactive(int,int,float,float)),widget,SLOT(onNewActivate(int,int,float,float)));
	*/
	// PC2 *pc = new PC2("bah");


	// pc stuff


	PC *pc = new PC;//(this,glWidget);

	//  PC *pc = new PC;


	// h2Splitter->addWidget(pc); // par c v

	View *xaxis = new View("X Axis");
	View *yaxis = new View("Y Axis");
	xaxis->setOrientation(0);
	yaxis->setOrientation(1);

	xaxis->setMaximumWidth(width()*.1);
	yaxis->setMaximumHeight(height()*.1);

	QWidget *fu = new QWidget;
	//  QSplitter *   hSplitter = new QSplitter;
	QGridLayout *gridlo = new QGridLayout;

	gridlo->addWidget(xaxis, 0, 0);
	gridlo->addWidget(pc, 0, 1);
	gridlo->addWidget(yaxis, 1, 1);
	fu->setLayout(gridlo);

	h2Splitter->addWidget(fu);

	// pc->show();

	//  pTab->addTab(pc, "Merger Tree");

	connect(processfiles, SIGNAL(triggered()), pc, SLOT(prepfiles()));

	//  connect(tfEditor, SIGNAL(changeTFTex()), pc, SLOT(tf_received_test()));

	connect(tfe, SIGNAL(tfChanged(TF&)), pc, SLOT(onnewtf(TF&)));
	// connect x axis
	connect(pc, SIGNAL(newxlab(QString&)), yaxis, SLOT(onnewlab(QString&)));
	connect(pc, SIGNAL(newxlimit(float, float)), yaxis, SLOT(onnewlimit(float, float)));
	connect(pc, SIGNAL(newxllimit(float, float)), yaxis, SLOT(onnewllimit(float, float)));

	// connect y axis
	connect(pc, SIGNAL(newylab(QString&)), xaxis, SLOT(onnewlab(QString&)));
	connect(pc, SIGNAL(newylimit(float, float)), xaxis, SLOT(onnewlimit(float, float)));
	connect(pc, SIGNAL(newyllimit(float, float)), xaxis, SLOT(onnewllimit(float, float)));

	/*
	connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),pc,SLOT(onNewData(std::vector<Series*>&)));

	connect(c,SIGNAL(newmin(QRgb&)),pc,SLOT(onnewmin(QRgb&)));
	connect(c,SIGNAL(newmax(QRgb&)),pc,SLOT(onnewmax(QRgb&)));

	connect(c,SIGNAL(newSampleset(int)),pc,SLOT(onnewsamp(int)));

	connect(c,SIGNAL(newopa(int)),pc,SLOT(onnewopa(int)));

	connect(view,SIGNAL(newActivated(int,int,float,float)),pc,SLOT(onNewActivate(int,int,float,float)));

	connect(widget,SIGNAL(newText(GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint)),pc,SLOT(onNewText(GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint, GLuint)));

	connect(pc,SIGNAL(newaxis(int)),widget,SLOT(onnewaxis(int)));

	//  connect(c,SIGNAL(newDataLoaded(std::vector<Series*>&)),view,SLOT(onNewData(std::vector<Series*>&)));

	connect(c,SIGNAL(newmaxx(float)),pc,SLOT(onnewmaxx(float)));
	connect(c,SIGNAL(newmaxy(float)),pc,SLOT(onnewmaxy(float)));
	connect(c,SIGNAL(newmaxz(float)),pc,SLOT(onnewmaxz(float)));

	connect(c,SIGNAL(newminx(float)),pc,SLOT(onnewminx(float)));
	connect(c,SIGNAL(newminy(float)),pc,SLOT(onnewminy(float)));
	connect(c,SIGNAL(newminz(float)),pc,SLOT(onnewminz(float)));

	connect(c,SIGNAL(newmaxvx(float)),pc,SLOT(onnewmaxvx(float)));
	connect(c,SIGNAL(newmaxvy(float)),pc,SLOT(onnewmaxvy(float)));
	connect(c,SIGNAL(newmaxvz(float)),pc,SLOT(onnewmaxvz(float)));

	connect(c,SIGNAL(newminvx(float)),pc,SLOT(onnewminvx(float)));
	connect(c,SIGNAL(newminvy(float)),pc,SLOT(onnewminvy(float)));
	connect(c,SIGNAL(newminvz(float)),pc,SLOT(onnewminvz(float)));

	connect(c,SIGNAL(newmaxv(float)),pc,SLOT(onnewmaxv(float)));
	connect(c,SIGNAL(newminv(float)),pc,SLOT(onnewminv(float)));
	connect(c,SIGNAL(newalpha(int)),pc,SLOT(onnewalpha(int)));

	connect(c,SIGNAL(newactive(int,int,float,float)),pc,SLOT(onNewActivate(int,int,float,float)));
	connect(c,SIGNAL(newbg(QRgb&)),pc,SLOT(onnewbg(QRgb&)));
	*/
	QGridLayout *layout = new QGridLayout;
	layout->addWidget(h2Splitter);

	setLayout(layout);

	//QWidget *dummy = new QWidget();
	//dummy->setLayout(layout);
	//setCentralWidget(dummy);

	//  this->setCentralWidget(layout);
	//docking stuff
	/*
	QDockWidget *dock = new QDockWidget(tr("Prespective"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(widget);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);


	dock = new QDockWidget(tr("Controls"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(c);
	this->addDockWidget(Qt::LeftDockWidgetArea, dock);


	dock = new QDockWidget(tr("Temporal"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(view);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);


	dock = new QDockWidget(tr("Parallel Coordinates"), this);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	// h2Splitter = new QSplitter(dock);
	dock->setWidget(pc);
	this->addDockWidget(Qt::RightDockWidgetArea, dock);
	*/
	setWindowTitle(tr("CosmoVis"));
}

void PCWindow::populateScene()
{
	//scene = new QGraphicsScene;

	/*  QImage image(":/qt4logo.png");

	// Populate scene
	int xx = 0;
	int nitems = 0;
	for (int i = -11000; i < 11000; i += 110) {
	++xx;
	int yy = 0;
	for (int j = -7000; j < 7000; j += 70) {
	++yy;
	qreal x = (i + 11000) / 22000.0;
	qreal y = (j + 7000) / 14000.0;

	QColor color(image.pixel(int(image.width() * x), int(image.height() * y)));
	QGraphicsItem *item = new Series(color, xx, yy);
	item->setPos(QPointF(i, j));
	scene->addItem(item);

	++nitems;
	}
	}*/

}