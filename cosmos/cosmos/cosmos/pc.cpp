
#include "pc.h"


#include <QMouseEvent>
#include <math.h>
#include <QGLPixelBuffer>

#include <fstream>
#include <sstream>

#include <iostream>
#include <string>


#include <QVector2D>
#include <QDir>
//#include <QStringList>



#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

struct VertexData
{
	QVector4D position;
	QVector4D texCoord;
};


void PC::onnewtf(TF &test){

	tf = test;

	makeCurrent();

	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();


	std::vector<GLfloat> transferFuncData;

	transferFuncData.push_back(0);
	transferFuncData.push_back(0);
	transferFuncData.push_back(0);
	transferFuncData.push_back(0);

	int test_res = tf.resolution();
	float * test_colormap = tf.colorMap();

	for (int i = 0; i < test_res; i++) //THIS PART MAY NOT BE NECESSARY HERE
	{
		transferFuncData.push_back(test_colormap[4 * i]);
		transferFuncData.push_back(test_colormap[4 * i + 1]);
		transferFuncData.push_back(test_colormap[4 * i + 2]);
		transferFuncData.push_back(test_colormap[4 * i + 3]);

		//    qDebug() << test_colormap[4*i+0] << " " << test_colormap[4*i+1] <<" " << test_colormap[4*i+2] << " " << test_colormap[4*i+3];
	}

	glPushAttrib(GL_TEXTURE_BIT);
	f->glDisable(GL_TEXTURE_3D);
	f->glEnable(GL_TEXTURE_2D);

	if (am != 0){
		f->glDeleteTextures(1, &tft);
	}
	f->glGenTextures(1, &tft);

	f->glActiveTexture(GL_TEXTURE0);

	f->glBindTexture(GL_TEXTURE_2D, tft);
	f->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, transferFuncData.size() / 4, 1, 0, GL_RGBA, GL_FLOAT, transferFuncData.data());
	f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	f->glBindTexture(GL_TEXTURE_2D, tft);

	glPopAttrib();

	am = 1;

	repaint();
}

void PC::prepfiles(){

	// let's load the data
	QStringList nameFilter("*.list.txt");
	QDir directory("/root/scivisdata/new/");
	QStringList files = directory.entryList(nameFilter);

	QString slash = "/";
	for (int i = 0; i < files.size(); i++){

		files[i] = directory.absolutePath() + slash + files[i];
		// qDebug() <<files[i];
	}

	for (int i = 0; i <files.size(); i++){

		std::ifstream file5(files[i].toLocal8Bit().data());


		FILE * pFile;
		QString fnam = QString(files[i].toLocal8Bit().data());
		fnam.append(".binary");

		pFile = fopen(fnam.toLocal8Bit().constData(), "wb");

		int kk = 0;
		while (file5)
		{

			std::string s5;

			if (!std::getline(file5, s5)) break;

			std::istringstream ss5(s5);
			// make a node

			HaloNode n;
			float vx = 0;
			float vy = 0;
			float vz = 0;
			int jj = 0;
			while (ss5 && jj < 23)
			{
				std::string s5;
				if (!std::getline(ss5, s5, ' ')) break;

				if (QString(s5.data()) != ""){
					//  qDebug() << QString(s5.data());
					if (jj == 1){ //set id
						// n.id =
						int id = QString(s5.data()).toInt();

						fwrite(&id, 4, 1, pFile);

						// 0


					}

					if (jj == 10){ //set mvir
						float val = QString(s5.data()).toFloat();
						fwrite(&val, 4, 1, pFile);
						float vlog = log10(val);
						fwrite(&vlog, 4, 1, pFile);
						// n.vars.push_back(val);
						// n.vars.push_back(log10(val));
						// 2, 3


					}

					if (jj == 3){
						// n.childid =
						int cid = QString(s5.data()).toInt();
						fwrite(&cid, 4, 1, pFile);
						// 1

					}

					if (jj == 20){ //vx
						vx = QString(s5.data()).toFloat();
					}
					if (jj == 21){ //vy
						vy = QString(s5.data()).toFloat();
					}
					if (jj == 22){ //vz
						vz = QString(s5.data()).toFloat();

						float vol = sqrt(vx*vx + vy*vy + vz*vz);
						fwrite(&vol, 4, 1, pFile);
						//n.vars.push_back(vol);
						// 4
					}

					jj++;
				}

			}

		}

		fclose(pFile);

	}







	//   emit this->newprog(int((float(currfile) * float(count*2) + float(k+r)) / (float(totalfiles) * float(count*2)) *100.0));





	/*
	tv = new QTableView;
	model = new QStandardItemModel;
	tv->setModel(model);

	model->setHorizontalHeaderItem(0, new QStandardItem(QString("RowID")));

	for(int i = 1; i < 100;i++){
	model->setHorizontalHeaderItem(i, new QStandardItem(QString("T").append(QString::number(i-1))));
	}


	QLabel * pr = new QLabel(QString("Merge Tree Table:"));
	QVBoxLayout *  ppLayout = new QVBoxLayout;

	ppLayout->addWidget(pr);
	ppLayout->addWidget(tv);

	ppfiles = new QDialog;
	ppfiles->setLayout(ppLayout);


	for(int k=0; k < mergdata[0].size(); k++){
	QList<QStandardItem*>  list;

	list.append(new QStandardItem(QString::number(k)));
	HaloNode n = mergdata[0][k];
	for(int j =0; j < 99; j++){

	if(j == 0 ){

	list.append(new QStandardItem(QString::number(n.id)));
	}

	else if(j == 1 ){

	list.append(new QStandardItem(QString::number(n.childid)));
	}
	else{
	list.append(new QStandardItem(QString::number(0)));
	}
	}
	model->appendRow(list);
	}


	//}

	ppfiles->activateWindow();

	ppfiles->setLayout(ppLayout);
	ppfiles->show();
	*/

}


void PC::snapshot(){


	QImage img = pbuffer->toImage();
	img.save("./test2.png");

}

//PC::PC(QWidget *parent, QOpenGLWidget *shared) :
PC::PC(QWidget *parent) :
QOpenGLWidget(parent)//QGLFormat::defaultFormat(),this,shared),
//,angularSpeed(0),
//m_distExp(600)
{

	this->setMinimumSize(20, 20);
	selection = 0;
	sel = 0;
	zsel = 0;

	/*  QOpenGLFormat pbufferFormat = format();
	pbufferFormat.setSampleBuffers(true);
	pbuffer = new QGLPixelBuffer(QSize(width(), height()), pbufferFormat, this);
	*/

	setMouseTracking(1);
	setFocusPolicy(Qt::ClickFocus);


	cl_color = QColor(255, 255, 255).rgba();

	cm_color = QColor(255, 255, 0).rgba();

	m_color = QColor(0, 0, 0).rgba();
	activetime = -1;
	samp = 32;
	opa = 255;
	mousedown = 0;
	activeaxis = 3;
	/*
	axis1= 0;
	axis2 = 1;
	axis3 = 2;
	axis4 = 3;

	ap1= -.9f;
	ap2= -.3f;
	ap3= .3f;
	ap4= .9f;

	mp1=0.05f;
	mp2=0.35f;
	mp3=0.65f;
	mp4=0.95f;
	*/
	minx = 0;
	maxx = 12;

	/*
	vminx = -600;
	vmaxx = 600;

	miny = 0;
	maxy = 12;

	vminy = -600;
	vmaxy = 600;
	*/
	vminz = 9000000;
	vmaxz = -1;


	minz = 9000000;
	maxz = -1;


	vmin = 9000000;
	vmax = -1;

	alpha = 0;
	xs = 0;

	goff = 0;
	coff = 0;

	this->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, SIGNAL(customContextMenuRequested(const QPoint&)),
		this, SLOT(ShowContextMenu(const QPoint&)));
	tft = 0;

	am = 0;

	xmin = -1;
	xmax = 1;
	ymin = -1;
	ymax = 1;

	//this->initializeGL();


}

PC::~PC()
{
	//  deleteTexture();
	glDeleteBuffers(6, vboIds2);
}

static void loadMatrix(const QMatrix4x4& m)
{
	// static to prevent glLoadMatrixf to fail on certain drivers
	static GLfloat mat[16];
	const float *data = m.constData();
	for (int index = 0; index < 16; ++index)
		mat[index] = data[index];
	glLoadMatrixf(mat);
}

void PC::showsel(bool show){
	if (sel != 0){
		if (show){
			sel->show();
		}
		else{
			sel->hide();
		}
		repaint();
	}
}

void PC::showbg(bool show){
	bg = show;
	repaint();
}

void PC::clearsel(){
	senderids.clear();
	activeids.clear();
	activeidx.clear();
	activeidsvbo.clear();

	// QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	//f->glDeleteBuffers(1,activevbo);
	repaint();
	qDebug() << activeidsvbo.size();
}

void PC::onNewAids(intList aids){

	// draw active ids

	qDebug() << aids.size() << "IDs received by mergetree!";

	activetime = 80;

	if (activetime > -1){

		// del buffer
		senderids.clear();
		activeids.clear();
		activeidx.clear();
		for (int j = 0; j < aids.size(); j++)
		{
			activeids.push_back(aids[j]);
		}

		QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
		f->glDeleteBuffers(1, activevbo);
		activeidsvbo.clear();
		// for(int i =0; i < mergdata[activetime].size(); i++){
		for (int i = 0; i < activeids.size(); i++){
			qDebug() << activeids[i];
			drawpath(activetime, mapper[activeids[i]]);
			activeidx.push_back(activetime);

		}

	}


	repaint();

}

void PC::onNewTime(int time){
	activetime = time;
	//activeids = aids;

	repaint();

}



void PC::setcolor(){

	QAction* action = (QAction *)sender();

	qDebug() << "Color Changed to mapping #: " << action->text();

	if (action->text().compare("ID") == 0){
		coff = 0;
	}
	if (action->text().compare("Layout 1") == 0){
		coff = 1;
	}

	if (action->text().compare("Layout 2") == 0){
		coff = 2;
	}

	if (action->text().compare("Mvir (log)") == 0){
		coff = 3;
	}

	if (action->text().compare("Mvir") == 0){
		coff = 5;
	}

	if (action->text().compare("Velocity") == 0){
		coff = 4;
	}

	if (action->text().compare("Default") == 0){
		coff = 0;
	}

}


void PC::setorder(){
	QAction* action = (QAction *)sender();

	qDebug() << "order Changed to mapping #: " << action->text();

	if (action->text().compare("ID") == 0){
		goff = 0;
		// fillvbo0();
		//  repaint();
	}
	if (action->text().compare("Layout 1") == 0){
		goff = 1;
		// fillvbo0();
		// repaint();
	}

	if (action->text().compare("Layout 2") == 0){
		goff = 2;
		// fillvbo0();
		// repaint();
	}

	if (action->text().compare("Velocity") == 0){
		goff = 4;
		// fillvbo0();
		// repaint();
	}

	if (action->text().compare("Mvir (log)") == 0){
		goff = 3;
		// fillvbo0();
		// repaint();
	}

	if (action->text().compare("Mvir") == 0){
		goff = 5;
		//  fillvbo0();
		// repaint();
	}

	QString xlab = "Time";

	QString ylab = "ID";


	// draw y-axis values
	float lmin = 0;
	float lmax = 1;
	QString units = QString(" ");

	switch (goff){
	case 0:

		ylab = "ID";
		lmin = minx;//0.0f;
		lmax = maxx;//8900.0f;
		break;
	case 1:
		lmin = 0.0f;
		lmax = 9000.0f;//2174267.0f;
		ylab = "Layout 1 ID";
		break;
	case 2: // ?
		lmin = 0.0f;
		lmax = 1.0f;

		ylab = "Layout 2 ID";
		break;

	case 3:
		units = QString(" Log10(Msun /h)");

		ylab = "Mvir (log)";
		lmin = vminz;//19066000000.0f;
		lmax = vmaxz;//227220000000000.0f;
		break;

	case 4:

		units = QString(" Km /s");
		ylab = "Velocity";
		lmin = vmin; //0.0f;
		lmax = vmax; // 2196.426f;
		break;

	case 5:
		ylab = "Mvir";
		units = QString(" Msun /h");
		lmin = minz;//10.28026f;
		lmax = maxz;//14.35645f;
		break;

	}

	emit this->newyllimit(lmin, lmax);
	emit this->newylab(ylab.append(units));
	emit this->newxlab(xlab);

	// rebuild selection vbo

	// del buffer
	senderids.clear();

	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	f->glDeleteBuffers(1, activevbo);
	activeidsvbo.clear();

	//for(int i =0; i < mergdata[activetime].size(); i++){
	for (int j = 0; j < activeids.size(); j++){

		drawpath(activeidx[j], mapper[activeids[j]]);
		//}
	}



	repaint();


}

void PC::ShowContextMenu(const QPoint& pos) // this is a slot
{
	// for most widgets
	QPoint globalPos = this->mapToGlobal(pos);


	QMenu myMenu;
	QMenu* orderby = myMenu.addMenu("Order V.Axis By");

	QMenu* colorby = myMenu.addMenu("Color By");

	QAction* oid = orderby->addAction("ID");
	QAction* olay = orderby->addAction("Layout 1");
	QAction* ov1 = orderby->addAction("Layout 2");
	QAction* ov4 = orderby->addAction("Mvir");
	QAction* ov3 = orderby->addAction("Mvir (log)");
	QAction* ov2 = orderby->addAction("Velocity");

	connect(oid, SIGNAL(triggered()), this, SLOT(setorder()));
	connect(olay, SIGNAL(triggered()), this, SLOT(setorder()));
	connect(ov1, SIGNAL(triggered()), this, SLOT(setorder()));
	connect(ov2, SIGNAL(triggered()), this, SLOT(setorder()));
	connect(ov3, SIGNAL(triggered()), this, SLOT(setorder()));
	connect(ov4, SIGNAL(triggered()), this, SLOT(setorder()));

	QAction* cv1 = colorby->addAction("Mvir");
	QAction* cv4 = colorby->addAction("Mvir (log)");
	QAction* cv2 = colorby->addAction("Velocity");
	QAction* cv3 = colorby->addAction("Default");

	connect(cv1, SIGNAL(triggered()), this, SLOT(setcolor()));
	connect(cv2, SIGNAL(triggered()), this, SLOT(setcolor()));
	connect(cv3, SIGNAL(triggered()), this, SLOT(setcolor()));
	connect(cv4, SIGNAL(triggered()), this, SLOT(setcolor()));

	myMenu.addAction("Cancel");


	// ...

	QAction* selectedItem = myMenu.exec(globalPos);
	if (selectedItem)
	{
		qDebug() << selectedItem;
	}
	else
	{
		// nothing was chosen
	}


}

void PC::onnewminx(float t){
	minx = t;
	repaint();
}

void PC::onnewmaxx(float t){
	maxx = t;
	repaint();
}


void PC::onnewminvx(float t){
	vminx = t;
	repaint();
}

void PC::onnewmaxvx(float t){
	vmaxx = t;
	repaint();
}

void PC::onnewminy(float t){
	miny = t;
	repaint();
}

void PC::onnewmaxy(float t){
	maxy = t;
	repaint();
}


void PC::onnewminvy(float t){
	vminy = t;
	repaint();
}

void PC::onnewmaxvy(float t){
	vmaxy = t;
	repaint();
}


void PC::onnewminz(float t){
	minz = t;
	repaint();
}

void PC::onnewmaxz(float t){
	maxz = t;
	repaint();
}


void PC::onnewminvz(float t){
	vminz = t;
	repaint();
}

void PC::onnewmaxvz(float t){
	vmaxz = t;
	repaint();
}

void PC::onnewminv(float t){
	vmin = t;
	repaint();
}

void PC::onnewmaxv(float t){
	vmax = t;
	repaint();
}


void PC::onnewmin(QRgb &color){

	cl_color = QColor(color).rgba();
	repaint();
}

void PC::onnewsamp(int test){

	samp = test;
	repaint();
}

void PC::onnewopa(int test){

	opa = test;
	repaint();
}

void PC::onnewalpha(int a){

	alpha = a;
	repaint();
}

void PC::onnewmax(QRgb &color){
	cm_color = QColor(color).rgba();
	repaint();
}

void PC::onnewbg(QRgb &color){
	m_color = QColor(color).rgba();
	repaint();
}


void PC::onNewData(){



	this->s1 = this->s2 = this->t1 = this->t2 = this->t3 = this->t4 = 0;
	this->lv = this->lh = 0.0f;
	//initializeGLFunctions();


	//! [0]
	// Generate 2 VBOs
	//  glGenBuffers(2, vboIds2);

	//! [0]

	// count = series[s1]->getsize();

	std::vector<GLfloat> vertices(count * 4);//*4);
	GLushort *indices;
	indices = new GLushort[count * 3];
	//GLushort indices[count * 3];

	for (int j = 0; j < count; j++){

		vertices[j * 4 + 0] = 0;  //type 0 for x 1 for y 2 for z 3 for vol
		vertices[(j * 4) + 3] = j; //index

		// forget indices doesn't matter
		indices[j * 3] = j * 3;
		indices[j * 3 + 1] = j * 3 + 1;
		indices[j * 3 + 2] = j * 3 + 2;
	}



	repaint();//repaint();


}


void PC::onNewText(GLuint p1, GLuint p2, GLuint p3, GLuint p4, GLuint v1, GLuint v2, GLuint v3, GLuint v4){
	//  this->p1 = p1;
	this->p2 = p2;
	this->p3 = p3;
	this->p4 = p4;

	this->v1 = v1;
	this->v2 = v2;
	this->v3 = v3;
	this->v4 = v4;
	repaint();
}

void PC::onNewActivate(int s1, int s2, float lv, float lh){

	if (s1 > -1 && s2 > -1){

		this->lv = lv;
		this->lh = lh;
	}
	// repaint();

	repaint();

	// snapshot();
}


QPointF PC::pixelPosToViewPos(const QPointF& p)
{
	return QPointF(2.0 * float(p.x()) / width() - 1.0,
		1.0 - 2.0 * float(p.y()) / height());
}

//! [0]

void PC::keyPressEvent(QKeyEvent *event)
{
	switch (event->key())
	{
	case Qt::Key_Escape:

		xmin = -1.0f;
		xmax = 1.0f;
		ymin = -1.0f;
		ymax = 1.0f;
		emit this->newxlimit(xmin, xmax);
		emit this->newylimit(ymin, ymax);
		repaint();
		break;
	}
	//qDebug() << event->key() << "  " << Qt::Key_Escape;
}

void PC::mousePressEvent(QMouseEvent *event)
{

	float x = float(event->x()) / float(width());
	float y = float(event->y()) / float(height());

	if (event->button() & Qt::MiddleButton) {
		this->cgomousex = event->x();//event->globalX();
		this->cgomousey = event->y();//event->globalY();

		if (zsel == 0){
			zsel = new selectionbox(this);
			zsel->setbgcolor(QColor(55, 215, 63));

			//  sel->setAttribute(Qt::WA_TranslucentBackground,true);
			// sel->setAttribute(Qt::WA_NoSystemBackground);
			//sel->setStyleSheet("background-color: transparent");
			// sel->setWindowFlags(Qt::ToolTip);
			//zsel->setAutoFillBackground(true);
			//zsel->setAttribute(Qt::WA_OpaquePaintEvent, true);
			//zsel->setWindowOpacity(0.5);
			zsel->setUpdatesEnabled(false);

			zsel->setGeometry(QRect(cgomousex, cgomousey, 1, 1));
			//  sel->move(QPoint(gomousex,gomousey));
			// sel->resize(1,1);
			zsel->show();
		}
	}
	else if (event->button() == Qt::LeftButton) {

		this->gomousex = event->x();//event->globalX();
		this->gomousey = event->y();//event->globalY();
		/*
		if (selection == 0)
		selection = new QRubberBand(QRubberBand::Rectangle, this);
		selection->setGeometry(QRect(gomousex,gomousey,1,1));
		selection->show();
		*/

		if (sel == 0){
			sel = new selectionbox(this);
			sel->setbgcolor(QColor(220, 116, 19));

			// del buffer
			senderids.clear();
			activeids.clear();
			activeidx.clear();
			QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
			f->glDeleteBuffers(1, activevbo);
			activeidsvbo.clear();
			connect(sel, SIGNAL(sendupdate()), this, SLOT(updateselect()));

			//  sel->setAttribute(Qt::WA_TranslucentBackground,true);
			// sel->setAttribute(Qt::WA_NoSystemBackground);
			//sel->setStyleSheet("background-color: transparent");
			// sel->setWindowFlags(Qt::ToolTip);
			//sel->setAutoFillBackground(true);
			//sel->setAttribute(Qt::WA_OpaquePaintEvent, true);
			//sel->setWindowOpacity(0.5);
			sel->setUpdatesEnabled(false);

			sel->setGeometry(QRect(gomousex, gomousey, 1, 1));
			//  sel->move(QPoint(gomousex,gomousey));
			// sel->resize(1,1);
			sel->show();
			emit this-> setshowsel(true);
		}



		// sel->setWindowOpacity(0.05);
		//sel->setWindowFlags(Qt::ToolTip);

		float pmin = -0.7;
		float pmax = 0.8;


		int activestep = -1;
		float d = .9f + .9f;
		/*
		for(int i =0; i < 99;i++){ //set active axis
		float aa = 0.05f + (0.90f)*float(i)/98.0f;

		if(mousex > aa - 0.001f && mousex < aa + 0.001f){
		activetime= i;
		activestep = i;
		activeids.clear(); //also clear ids
		activeidx.clear();
		//  qDebug() << "Active axis: " << i;
		break;
		}

		}


		if(activestep < 0){
		activestep = activetime;
		}


		if(activestep > -1){
		int i = activestep;
		for(int j =0; j < mergdata[i].size(); j++){
		float temp = 0;
		float temp2 = 0;
		float yt = 0;



		switch(goff){
		case 0:

		temp = (float(mergdata[i][j].id) - float(minx)) /(float(maxx) -  float(minx));

		yt = temp;
		//  temp2 = (float(mergdata[i][j].childid) - float(minx)) /(float(maxx) -  float(minx));

		break;
		case 1:

		temp = (float(j) / 9000.0f);

		yt = temp;
		// temp2 = (float(mergdata[i][j].cids[0]) / 9000.0f);

		break;

		case 2:
		temp = (float(j) / float(mergdata[i].size()));

		yt = temp;
		//temp2 = (float(mergdata[i][j].cids[0]) / float(mergdata[i+1].size()));
		break;
		case 3:
		temp = (float(mergdata[i][j].vars[1]) - float(vminz)) /(float(vmaxz) -  float(vminz));
		yt = temp;

		temp2 = (float(mergdata[i+1][mapper[mergdata[i][j].childid]].vars[1]) - float(vminz)) /(float(vmaxz) -  float(vminz));

		break;

		case 4:
		temp = (float(mergdata[i][j].vars[2]) - float(vmin)) /(float(vmax) -  float(vmin));
		yt = temp;

		temp2 = (float(mergdata[i+1][mapper[mergdata[i][j].childid]].vars[2]) - float(vmin)) /(float(vmax) -  float(vmin));

		break;


		case 5: //mvir

		temp = (float(mergdata[i][j].vars[0]) - float(minz)) /(float(maxz) -  float(minz));

		yt = temp;
		temp2 = (float(mergdata[i+1][mapper[mergdata[i][j].childid]].vars[0]) - float(minz)) /(float(maxz) -  float(minz));

		break;

		}
		temp = temp*(pmax - pmin) + pmin;
		temp2 = temp2*(pmax - pmin) + pmin;

		float idx =float(i)/float(98.0f)*d - .9f;
		float idx2 = float(i+1)/float(98.0f)*d - .9f;

		float yy =   ((mousey - .85)*(1.0f - 0.0f)/(.10-.85));




		// find active ids
		if (activestep > -1 && i == activestep){



		if(yt > yy - 0.01f && yt < yy + 0.01f ){

		activeids.push_back(mergdata[i][j].id);
		activeidx.push_back(j);
		}

		if(activetime > 0){ //things already in there so find index

		for(int k =0; k< activeids.size(); k++){
		if(mergdata[i][j].id == activeids[k]){
		// call recursive function to draw everything
		// drawpath(activestep,i);
		activeidx.push_back(j);

		}
		}
		}


		}

		}
		emit this->newAidsLoaded(activestep,this->activeids);
		}
		*/

	}
	else{
		// do nothing
	}
	if (x > mp1 - 0.01f && x < mp1 + 0.01f){
		mousedown = 1;
		activeaxis = 0;
		emit this->newaxis(activeaxis);
	}

	else if (x > mp2 - 0.01f && x < mp2 + 0.01f){
		mousedown = 2;
		activeaxis = 1;
		emit this->newaxis(activeaxis);
	}

	else if (x > mp3 - 0.01f && x < mp3 + 0.01f){
		mousedown = 3;
		activeaxis = 2;
		emit this->newaxis(activeaxis);
	}

	else if (x > mp4 - 0.01f && x < mp4 + 0.01f){
		mousedown = 4;
		activeaxis = 3;
		emit this->newaxis(activeaxis);
	}
	else{
		qDebug() << "failed!  " << x << "\n";
	}

	// Save mouse press position
	mousePressPosition = QVector2D(event->localPos());

	// get mouse pose

	//  POINT mouse;
	//  GetCursorPos (&mouse);
	//  ScreenToClient (m_hwnd, &mouse);
	/*
	int viewport[4];
	float modelview[16];
	float projection[16];

	glGetFloatv (GL_MODELVIEW_MATRIX, modelview);
	glGetFloatv (GL_PROJECTION_MATRIX, projection);
	glGetIntegerv (GL_VIEWPORT, viewport);

	//  QMatrix4x4 v; v.fill(viewport);
	QMatrix4x4 mv(modelview);
	QMatrix4x4 p(projection);

	QMatrix4x4 m;

	m = m * mv;
	m = m.inverted();


	float winX = mousex  * 2.0 - 1.0;//(float)mouse.x;
	float winY = ((float)height() - (mousey * (float)height()))/(float)height()  * 2.0 - 1.0; ;//(float)m_height - (float)mouse.y; //(float)viewport[3] - (float)mouse.y;
	float winZ;

	glReadPixels ((int)winX, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
	float finalCoordX, finalCoordY, finalCoordZ;


	QVector4D vec(winX,winY,2.0 *winZ -1.0,1.0f);

	QVector4D vout = m * vec;

	if(vout.w() != 0){

	vout.setW(  1.0/vout.w());
	finalCoordX = vout.x() * vout.w();
	finalCoordY = vout.y() * vout.w();
	finalCoordZ = vout.z() * vout.w();
	}

	qDebug() << "x: " << finalCoordX << " y: " << finalCoordY << " z: " << finalCoordZ;

	*/

}

void PC::mouseMoveEvent(QMouseEvent *event)
{

	this->mousex = float(event->x()) / float(width());
	this->mousey = float(event->y()) / float(height());
	this->gmousex = event->globalX();
	this->gmousey = event->globalY();

	float pmin = -0.7;
	float pmax = 0.8;
	float d = .9f + .9f;

	//repaint();
	//qDebug() << mousex << " " << mousey;

	if (event->buttons() & Qt::MiddleButton){ //Qt::MidButton) {
		//selection->setGeometry(QRect(QPoint(gomousex,gomousey),event->pos()).normalized());

		//sel->setWindowFlags(Qt::ToolTip);
		//sel->setAutoFillBackground( true );
		//sel->setAttribute( Qt::WA_OpaquePaintEvent, true );
		zsel->setWindowOpacity(0.5);
		zsel->setGeometry(QRect(QPoint(cgomousex, cgomousey), event->pos()).normalized());
		// sel->move(QPoint(gomousex,gomousey));
		// sel->resize(abs(gomousex-event->pos().x()),abs(gomousey-event->pos().y()));
		//  sel->repaint();
		event->accept();
	}
	else  if (event->buttons() & Qt::LeftButton){ //Qt::MidButton) {
		//selection->setGeometry(QRect(QPoint(gomousex,gomousey),event->pos()).normalized());

		//sel->setWindowFlags(Qt::ToolTip);
		//sel->setAutoFillBackground( true );
		//sel->setAttribute( Qt::WA_OpaquePaintEvent, true );
		sel->setWindowOpacity(0.5);
		sel->setGeometry(QRect(QPoint(gomousex, gomousey), event->pos()).normalized());

		//QRect  q = QRect(QPoint(gomousex, gomousey), event->pos()).normalized();
		//qDebug() << "left " << q.left() << "toop " << q.top() << "right " << q.right() << "bottom " << q.bottom();
		// find and draw active ids

		// clear sender list
		//repaint();
		//return;
		/*
		senderids.clear();
		activeids.clear();
		activeidx.clear();
		QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
		f->glUseProgram(0);

		// del buffer
		f->glDeleteBuffers(1, activevbo);
		activeidsvbo.clear();

		float pmin = -0.7;
		float pmax = 0.8;

		float ts1 = 0.0f;
		float ts2 = 0.0f;

		float ub = 0.0f;
		float lb = 0.0f;

		if (sel != 0){
			ub = float(sel->geometry().top()) / float(height());

			lb = float(sel->geometry().bottom()) / float(height());

			ts1 = float(sel->geometry().left()) / float(width());

			ts2 = float(sel->geometry().right()) / float(width());


		}


		ub = 1.0f - ub;
		lb = 1.0f - lb;

		float yymax = lerp(ub, ymin, ymax);//((ub - .85)*(1.0f - 0.0f)/(.10-.85));
		float yymin = lerp(lb, ymin, ymax);//((lb - .85)*(1.0f - 0.0f)/(.10-.85));

		float xxmin = lerp(ts1, xmin, xmax);// ((ts1 - .85)*(1.0f - 0.0f)/(.10-.85));
		float xxmax = lerp(ts2, xmin, xmax);//((ts2 - .85)*(1.0f - 0.0f)/(.10-.85));

		// find selected steps



		float xxmin2 = (xxmin + 0.9f)*(98.0f - 0.0f) / (0.9f + 0.9f) - 0.9f;
		float xxmax2 = (xxmax + 0.9f)*(98.0f - 0.0f) / (0.9f + 0.9f) - 0.9f;

		int xstart = (int)ceil(xxmin2);
		int xstop = (int)xxmax2;

		if (xstart < 0){
			xstart = 0;
		}

		if (xstop > mergdata.size()){
			xstop = mergdata.size() - 1;
		}


		for (int i = xstart; i <= xstop; i++){
			for (int j = 0; j < mergdata[i].size(); j++){
				float temp = 0;
				float temp2 = 0;
				float yt = 0;



				switch (goff){
				case 0:

					temp = (float(mergdata[i][j].id) - float(minx)) / (float(maxx) - float(minx));

					yt = temp;
					//  temp2 = (float(mergdata[i][j].childid) - float(minx)) /(float(maxx) -  float(minx));

					break;
				case 1:

					temp = (float(j) / 9000.0f);

					yt = temp;
					// temp2 = (float(mergdata[i][j].cids[0]) / 9000.0f);

					break;

				case 2:
					temp = (float(j) / float(mergdata[i].size()));

					yt = temp;
					//temp2 = (float(mergdata[activesteps[i]][j].cids[0]) / float(mergdata[i+1].size()));
					break;
				case 3:
					temp = (float(mergdata[i][j].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));
					yt = temp;

					//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[1]) - float(vminz)) /(float(vmaxz) -  float(vminz));

					break;

				case 4:
					temp = (float(mergdata[i][j].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));
					yt = temp;

					//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[2]) - float(vmin)) /(float(vmax) -  float(vmin));

					break;


				case 5: //mvir

					temp = (float(mergdata[i][j].vars[0]) - float(minz)) / (float(maxz) - float(minz));

					yt = temp;
					//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[0]) - float(minz)) /(float(maxz) -  float(minz));

					break;

				}
				temp = temp*(pmax - pmin) + pmin;
				temp2 = temp2*(pmax - pmin) + pmin;

				yt = temp;

				float idx = float(i) / float(98.0f)*d - .9f;

				//  float yy1 =   ((ub - .85)*(1.0f - 0.0f)/(.10-.85));
				// float yy2 =  ((lb - .85)*(1.0f - 0.0f)/(.10-.85));



				// find active ids


				if (yt > yymin && yt < yymax){

					activeids.push_back(mergdata[i][j].id);
					activeidx.push_back(i);
					drawpath(i, j);
				}



			}
		} // times
		intList senderIDsSignal;
		qDebug() << "attemtping to send" << activeids.size() << "IDs";
		for (int num = 0; num < activeids.size(); num++)
		{
			senderIDsSignal.push_back(activeids[num]);
		}
		emit this->newAidsLoaded(senderIDsSignal);
		*/
		event->accept();
	}
	else {

	}
	repaint();
}

void PC::updateselect(){
	senderids.clear();
	activeids.clear();
	activeidx.clear();
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	f->glUseProgram(0);

	// del buffer
	f->glDeleteBuffers(1, activevbo);
	activeidsvbo.clear();

	float pmin = -0.7;
	float pmax = 0.8;

	float ts1 = 0.0f;
	float ts2 = 0.0f;

	float ub = 0.0f;
	float lb = 0.0f;

	float d = .9f + .9f;

	if (sel != 0){
		ub = float(sel->geometry().top()) / float(height());

		lb = float(sel->geometry().bottom()) / float(height());

		ts1 = float(sel->geometry().left()) / float(width());

		ts2 = float(sel->geometry().right()) / float(width());


	}


	ub = 1.0f - ub;
	lb = 1.0f - lb;

	float yymax = lerp(ub, ymin, ymax);//((ub - .85)*(1.0f - 0.0f)/(.10-.85));
	float yymin = lerp(lb, ymin, ymax);//((lb - .85)*(1.0f - 0.0f)/(.10-.85));

	float xxmin = lerp(ts1, xmin, xmax);// ((ts1 - .85)*(1.0f - 0.0f)/(.10-.85));
	float xxmax = lerp(ts2, xmin, xmax);//((ts2 - .85)*(1.0f - 0.0f)/(.10-.85));

	// find selected steps



	float xxmin2 = (xxmin + 0.9f)*(98.0f - 0.0f) / (0.9f + 0.9f) - 0.9f;
	float xxmax2 = (xxmax + 0.9f)*(98.0f - 0.0f) / (0.9f + 0.9f) - 0.9f;

	int xstart = (int)ceil(xxmin2);
	int xstop = (int)xxmax2;

	if (xstart < 0){
		xstart = 0;
	}

	if (xstop > mergdata.size()){
		xstop = mergdata.size() - 1;
	}

	if (xstart > xstop){
		return;
	}

	for (int i = xstart; i <= xstop; i++){
		for (int j = 0; j < mergdata[i].size(); j++){
			float temp = 0;
			float temp2 = 0;
			float yt = 0;



			switch (goff){
			case 0:

				temp = (float(mergdata[i][j].id) - float(minx)) / (float(maxx) - float(minx));

				yt = temp;
				//  temp2 = (float(mergdata[i][j].childid) - float(minx)) /(float(maxx) -  float(minx));

				break;
			case 1:

				temp = (float(j) / 9000.0f);

				yt = temp;
				// temp2 = (float(mergdata[i][j].cids[0]) / 9000.0f);

				break;

			case 2:
				temp = (float(j) / float(mergdata[i].size()));

				yt = temp;
				//temp2 = (float(mergdata[activesteps[i]][j].cids[0]) / float(mergdata[i+1].size()));
				break;
			case 3:
				temp = (float(mergdata[i][j].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));
				yt = temp;

				//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[1]) - float(vminz)) /(float(vmaxz) -  float(vminz));

				break;

			case 4:
				temp = (float(mergdata[i][j].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));
				yt = temp;

				//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[2]) - float(vmin)) /(float(vmax) -  float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[i][j].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				yt = temp;
				//temp2 = (float(mergdata[i+1][mapper[mergdata[activesteps[i]][j].childid]].vars[0]) - float(minz)) /(float(maxz) -  float(minz));

				break;

			}
			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;

			yt = temp;

			float idx = float(i) / float(98.0f)*d - .9f;

			//  float yy1 =   ((ub - .85)*(1.0f - 0.0f)/(.10-.85));
			// float yy2 =  ((lb - .85)*(1.0f - 0.0f)/(.10-.85));



			// find active ids


			if (yt > yymin && yt < yymax){

				activeids.push_back(mergdata[i][j].id);
				activeidx.push_back(i);
				drawpath(i, j);
			}



		}
	} // times
	intList senderids_intList;
	for (int i = 0; i < senderids.size(); i++)
	{
		senderids_intList[i] = senderids[i];
	}
	emit this->newAidsLoaded(senderids_intList);
	repaint();
}

void PC::wheelEvent(QWheelEvent * event)
{

	if (!event->isAccepted()) {
		m_distExp += event->delta();

		event->accept();
	}
}

void PC::mouseReleaseEvent(QMouseEvent *event)
{

	// selection->hide();
	//  selection->clearMask();

	if (zsel != 0){
		float ts1 = 0.0f;
		float ts2 = 0.0f;

		float ub = 0.0f;
		float lb = 0.0f;

		ub = float(zsel->geometry().top()) / float(height());

		lb = float(zsel->geometry().bottom()) / float(height());

		ts1 = float(zsel->geometry().left()) / float(width());

		ts2 = float(zsel->geometry().right()) / float(width());

		ub = 1.0f - ub;
		lb = 1.0f - lb;

		float yymax = lerp(ub, ymin, ymax);//((ub - .85)*(1.0f - 0.0f)/(.10-.85));
		float yymin = lerp(lb, ymin, ymax);//((lb - .85)*(1.0f - 0.0f)/(.10-.85));

		float xxmin = lerp(ts1, xmin, xmax);// ((ts1 - .85)*(1.0f - 0.0f)/(.10-.85));
		float xxmax = lerp(ts2, xmin, xmax);//((ts2 - .85)*(1.0f - 0.0f)/(.10-.85));

		zsel->hide();
		zsel->clearMask();

		emit this->newxlimit(xxmin, xxmax);
		emit this->newylimit(yymin, yymax);
		xmin = xxmin;
		xmax = xxmax;
		ymin = yymin;
		ymax = yymax;

		// qDebug() << "top: " << ub << "bottom: " << lb << "left: " << ts1 << "right: " << ts2;

		// qDebug() << "top: " << ymax << "bottom: " << ymin << "left: " << xmin << "right: " << xmax;
		zsel = 0;
		repaint();
	}

	switch (mousedown){

	case 1:
		ap1 = (((mousex - 0.04f)*(.9f + .9f)) / (0.96f - 0.04f)) - (.9f + .9f) / 2.0f;
		mp1 = mousex;
		mousedown = 0;
		break;
	case 2:
		ap2 = (((mousex - 0.04f)*(.9f + .9f)) / (0.96f - 0.04f)) - (.9f + .9f) / 2.0f;
		mp2 = mousex;
		mousedown = 0;
		break;
	case 3:
		ap3 = (((mousex - 0.04f)*(.9f + .9f)) / (0.96f - 0.04f)) - (.9f + .9f) / 2.0f;
		mp3 = mousex;
		mousedown = 0;
		break;
	case 4:
		ap4 = (((mousex - 0.04f)*(.9f + .9f)) / (0.96f - 0.04f)) - (.9f + .9f) / 2.0f;
		mp4 = mousex;
		mousedown = 0;
	default:
		mousedown = 0;
	}



	// Mouse release position - mouse press position
	QVector2D diff = QVector2D(event->localPos()) - mousePressPosition;

	// Rotation axis is perpendicular to the mouse position difference
	// vector
	QVector3D n = QVector3D(diff.y(), diff.x(), 0.0).normalized();

	// Accelerate angular speed relative to the length of the mouse sweep
	qreal acc = diff.length() / 100.0;

	// Calculate new rotation axis as weighted sum
	rotationAxis = (rotationAxis * angularSpeed + n * acc).normalized();

	// Increase angular speed
	angularSpeed += acc;



	if (event->button() == Qt::LeftButton){ //Qt::MidButton) {

		event->accept();
	}
}
//! [0]

//! [1]
void PC::timerEvent(QTimerEvent *)
{

}
//! [1]

void PC::initializeGL()
{

	makeCurrent();
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	f->initializeOpenGLFunctions();
	//return;
	//    dynamicTexture = pbuffer->generateDynamicTexture();

	// bind the dynamic texture to the pbuffer - this is a no-op under X11
	//       hasDynamicTextureUpdate = pbuffer->bindToDynamicTexture(dynamicTexture);
	// makeCurrent();

	// qglClearColor(QColor(51,51,51));
	f->glClearColor(QColor(m_color).redF(), QColor(m_color).redF(), QColor(m_color).blueF(), 1.0f);

	f->glEnable(GL_LINE_SMOOTH);
	f->glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	f->glEnable(GL_BLEND);
	f->glBlendEquation(GL_FUNC_ADD);
	f->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	initShaders();
	initTextures();

	int nrow = 46617;
	int ncol = 99;

	minx = 9000;
	maxx = -1;

	// let's load the data
	QStringList nameFilter("*.list.txt.binary");
	QDir directory("./cosmoVis_data_files/");
	QStringList files = directory.entryList(nameFilter);

	QString slash = "/";
	for (int i = 0; i < files.size(); i++){

		files[i] = directory.absolutePath() + slash + files[i];
		// qDebug() <<files[i];
	}

	for (int i = 0; i <files.size(); i++){

		FILE *Filep;

		Filep = fopen(files.at(i).toLocal8Bit().constData(), "rb");

		long Fsize;
		char * buffer;
		size_t result;



		//get file size
		fseek(Filep, 0, SEEK_END);
		Fsize = ftell(Filep);
		rewind(Filep);

		// allocate memory to contain the whole file:
		buffer = (char*)malloc(sizeof(char)*Fsize);
		if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

		// copy the file into the buffer:
		result = fread(buffer, 1, Fsize, Filep);
		if (result != Fsize) { fputs("Reading error", stderr); exit(3); }




		std::vector<HaloNode> vect;


		int count = Fsize / (20); // 4*5
		float mvir, mvirlog, vol;
		int id, cid;



		int k = 0;


		for (k = 0; k < count; k++) {
			HaloNode n;

			memcpy(&id, &buffer[k*(20)], 4);
			memcpy(&cid, &buffer[k*(20) + 4], 4);
			memcpy(&mvir, &buffer[k*(20) + 8], 4);
			memcpy(&mvirlog, &buffer[k*(20) + 12], 4);
			memcpy(&vol, &buffer[k*(20) + 16], 4);



			n.id = id;

			//calc min/max
			if (minx >= n.id){

				minx = n.id;
			}

			if (maxx <= n.id){
				maxx = n.id;
			}





			n.vars.push_back(mvir);
			n.vars.push_back(mvirlog);

			//calc min/max
			if (minz >= mvir){

				minz = mvir;
			}

			if (maxz <= mvir){
				maxz = mvir;
			}


			//calc min/max
			if (vminz >= mvirlog){

				vminz = mvirlog;
			}

			if (vmaxz <= mvirlog){
				vmaxz = mvirlog;
			}




			n.childid = cid;





			n.vars.push_back(vol);
			//calc min/max
			if (vmin >= vol){

				vmin = vol;
			}

			if (vmax <= vol){
				vmax = vol;
			}

			// we're done so push back
			vect.push_back(n);
			//add index to mapper
			mapper[n.id] = k;
		}


		//file's done so push back vect
		mergdata.push_back(vect);
		fclose(Filep);
		free(buffer);
	}


	// build anc and decn vectors

	for (int i = 0; i < mergdata.size(); i++){

		for (int j = 0; j < mergdata[i].size(); j++){

			// add parent data
			if (mapper.find(mergdata[i][j].childid) != mapper.end()){ // only if it exists
				mergdata[i + 1][mapper[mergdata[i][j].childid]].pids.push_back(j);
			}

			// add child data

			if (mergdata[i][j].childid > -1){ // only if it exists
				mergdata[i][j].cids.push_back(mapper[mergdata[i][j].childid]);
			}


		} // per halo

	} // per time step



	/*
	std::vector<GLfloat> vertices(nrow*4*2);



	for (int j = 0; j < nrow; j++){

	vertices[((j*2+0)*4)+3] = j;

	vertices[((j*2+1)*4)+0] = -1.0f;
	vertices[((j*2+1)*4)+3] = j;

	}
	*/


	f->glGenBuffers(6, vboIds2);

	//  glBindBuffer(GL_ARRAY_BUFFER, vboIds2[0]); //nrow* 4 *2
	//     glBufferData(GL_ARRAY_BUFFER, vertices.size()* sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

	//  fillvbo0(); // initial fill with goff = 0
	fillvbos(); // only done once


	// Use QBasicTimer because its faster than QTimer
	// timer.start(12, this);


	// set initial labels
	QString xlab = "Time";

	QString ylab = "ID";


	// draw y-axis values
	float lmin = 0;
	float lmax = 1;
	QString units = QString(" ");

	switch (goff){
	case 0:

		ylab = "ID";
		lmin = minx;//0.0f;
		lmax = maxx;//8900.0f;
		break;
	case 1:
		lmin = 0.0f;
		lmax = 9000.0f;//2174267.0f;
		ylab = "Layout 1 ID";
		break;
	case 2: // ?
		lmin = 0.0f;
		lmax = 1.0f;
		ylab = "Layout 2 ID";
		break;

	case 3:
		units = QString(" Log10(Msun /h)");

		ylab = "Mvir (log)";
		lmin = vminz;//19066000000.0f;
		lmax = vmaxz;//227220000000000.0f;
		break;

	case 4:

		units = QString(" Km /s");
		ylab = "Velocity";
		lmin = vmin; //0.0f;
		lmax = vmax; // 2196.426f;
		break;

	case 5:
		ylab = "Mvir";
		units = QString(" Msun /h");
		lmin = minz;//10.28026f;
		lmax = maxz;//14.35645f;
		break;

	}

	emit this->newyllimit(lmin, lmax);
	emit this->newylab(ylab.append(units));
	emit this->newxlab(xlab);
	emit this->newxllimit(0, 98);

}

void PC::fillvbos(){

	std::vector<GLfloat> vertices0;
	std::vector<GLfloat> vertices1;
	std::vector<GLfloat> vertices2;
	std::vector<GLfloat> vertices3;
	std::vector<GLfloat> vertices4;
	std::vector<GLfloat> vertices5;


	float pmin = -0.7;
	float pmax = 0.8;
	float d = .9f + .9f;

	for (int i = 0; i < mergdata.size() - 1; i++){

		for (int j = 0; j < mergdata[i].size(); j++){
			float temp = 0;
			float temp2 = 0;


			float idx = float(i) / float(98.0f)*d - .9f;
			float idx2 = float(i + 1) / float(98.0f)*d - .9f;

			//  case 0:

			temp = (float(mergdata[i][j].id) - float(minx)) / (float(maxx) - float(minx));


			temp2 = (float(mergdata[i][j].childid) - float(minx)) / (float(maxx) - float(minx));

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			// push first vertex
			vertices0.push_back(idx);
			vertices0.push_back(temp);
			vertices0.push_back(-1.0f);
			vertices0.push_back(1.0f);

			// push second vertex
			vertices0.push_back(idx2);
			vertices0.push_back(temp2);
			vertices0.push_back(-1.0f);
			vertices0.push_back(1.0f);



			//    case 1:

			temp = (float(j) / 9000.0f);


			temp2 = (float(mergdata[i][j].cids[0]) / 9000.0f);

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			// push first vertex
			vertices1.push_back(idx);
			vertices1.push_back(temp);
			vertices1.push_back(-1.0f);
			vertices1.push_back(1.0f);

			// push second vertex
			vertices1.push_back(idx2);
			vertices1.push_back(temp2);
			vertices1.push_back(-1.0f);
			vertices1.push_back(1.0f);



			//  case 2:
			temp = (float(j) / float(mergdata[i].size()));


			temp2 = (float(mergdata[i][j].cids[0]) / float(mergdata[i + 1].size()));

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			// push first vertex
			vertices2.push_back(idx);
			vertices2.push_back(temp);
			vertices2.push_back(-1.0f);
			vertices2.push_back(1.0f);

			// push second vertex
			vertices2.push_back(idx2);
			vertices2.push_back(temp2);
			vertices2.push_back(-1.0f);
			vertices2.push_back(1.0f);


			//   case 3:
			temp = (float(mergdata[i][j].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


			temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;

			// push first vertex
			vertices3.push_back(idx);
			vertices3.push_back(temp);
			vertices3.push_back(-1.0f);
			vertices3.push_back(1.0f);

			// push second vertex
			vertices3.push_back(idx2);
			vertices3.push_back(temp2);
			vertices3.push_back(-1.0f);
			vertices3.push_back(1.0f);


			//  case 4:
			temp = (float(mergdata[i][j].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


			temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;


			// push first vertex
			vertices4.push_back(idx);
			vertices4.push_back(temp);
			vertices4.push_back(-1.0f);
			vertices4.push_back(1.0f);

			// push second vertex
			vertices4.push_back(idx2);
			vertices4.push_back(temp2);
			vertices4.push_back(-1.0f);
			vertices4.push_back(1.0f);


			// case 5: //mvir

			temp = (float(mergdata[i][j].vars[0]) - float(minz)) / (float(maxz) - float(minz));


			temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[0]) - float(minz)) / (float(maxz) - float(minz));


			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;



			//   glVertex3f(idx, temp, -1.0f);
			//    glVertex3f(idx2, temp2, -1.0f);

			// push first vertex
			vertices5.push_back(idx);
			vertices5.push_back(temp);
			vertices5.push_back(-1.0f);
			vertices5.push_back(1.0f);

			// push second vertex
			vertices5.push_back(idx2);
			vertices5.push_back(temp2);
			vertices5.push_back(-1.0f);
			vertices5.push_back(1.0f);



		}

	} // time step

	count = vertices0.size();

	// vbo filling
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[0]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices0.size()* sizeof(GLfloat), vertices0.data(), GL_STATIC_DRAW);

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[1]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices1.size()* sizeof(GLfloat), vertices1.data(), GL_STATIC_DRAW);

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[2]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices2.size()* sizeof(GLfloat), vertices2.data(), GL_STATIC_DRAW);

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[3]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices3.size()* sizeof(GLfloat), vertices3.data(), GL_STATIC_DRAW);

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[4]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices4.size()* sizeof(GLfloat), vertices4.data(), GL_STATIC_DRAW);

	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[5]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, vertices5.size()* sizeof(GLfloat), vertices5.data(), GL_STATIC_DRAW);

	vertices0.clear();
	vertices1.clear();
	vertices2.clear();
	vertices3.clear();
	vertices4.clear();
	vertices5.clear();


}


void PC::fillvbo0(){

	std::vector<GLfloat> vertices;


	float pmin = -0.7;
	float pmax = 0.8;
	float d = .9f + .9f;

	for (int i = 0; i < mergdata.size() - 1; i++){

		for (int j = 0; j < mergdata[i].size(); j++){
			float temp = 0;
			float temp2 = 0;

			switch (goff){
			case 0:

				temp = (float(mergdata[i][j].id) - float(minx)) / (float(maxx) - float(minx));


				temp2 = (float(mergdata[i][j].childid) - float(minx)) / (float(maxx) - float(minx));

				break;
			case 1:

				temp = (float(j) / 9000.0f);


				temp2 = (float(mergdata[i][j].cids[0]) / 9000.0f);

				break;

			case 2:
				temp = (float(j) / float(mergdata[i].size()));


				temp2 = (float(mergdata[i][j].cids[0]) / float(mergdata[i + 1].size()));
				break;

			case 3:
				temp = (float(mergdata[i][j].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


				temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

				break;

			case 4:
				temp = (float(mergdata[i][j].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


				temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[i][j].vars[0]) - float(minz)) / (float(maxz) - float(minz));


				temp2 = (float(mergdata[i + 1][mapper[mergdata[i][j].childid]].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				break;

			}
			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;

			float idx = float(i) / float(98.0f)*d - .9f;
			float idx2 = float(i + 1) / float(98.0f)*d - .9f;


			//   glVertex3f(idx, temp, -1.0f);
			//    glVertex3f(idx2, temp2, -1.0f);

			// push first vertex
			vertices.push_back(idx);
			vertices.push_back(temp);
			vertices.push_back(-1.0f);
			vertices.push_back(1.0f);

			// push second vertex
			vertices.push_back(idx2);
			vertices.push_back(temp2);
			vertices.push_back(-1.0f);
			vertices.push_back(1.0f);



		}

	} // time step

	count = vertices.size();

	// vbo filling

	glBindBuffer(GL_ARRAY_BUFFER, vboIds2[0]); //nrow* 4 *2
	glBufferData(GL_ARRAY_BUFFER, vertices.size()* sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

	vertices.clear();


}

void PC::initShaders()
{

	// Compile vertex shader
	if (!program.addShaderFromSourceFile(QOpenGLShader::Vertex, "pcvshader.glsl"))
		close();

	// Compile geometry shader
	//  if (!program.addShaderFromSourceFile(QGLShader::Geometry, ":/pcgshader.glsl"))
	//    close();


	// Compile fragment shader
	if (!program.addShaderFromSourceFile(QOpenGLShader::Fragment, "pcfshader.glsl"))
		close();

	// Link shader pipeline
	if (!program.link())
		close();

	// Bind shader pipeline for use
	if (!program.bind())
		close();

}
//! [3]

//! [4]
void PC::initTextures()
{



}
//! [4]

//! [5]
void PC::resizeGL(int w, int h)
{
	makeCurrent();
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
	// Set OpenGL viewport to cover whole widget
	f->glViewport(0, 0, w, h);

	// Calculate aspect ratio
	qreal aspect = qreal(w) / qreal(h ? h : 1);

	// Set near plane to 3.0, far plane to 7.0, field of view 45 degrees
	const qreal zNear = 1.0, zFar = 70000.0, fov = 45.0;

	// Reset projection
	projection.setToIdentity();
	projection.ortho(-1, 1, -1, 1, zNear, zFar);



}
//! [5]

void PC::paintGL()
{

	makeCurrent();
	QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

	//qglClearColor(QColor(m_color));
	f->glClearColor(QColor(m_color).redF(), QColor(m_color).redF(), QColor(m_color).blueF(), 1.0f);

	QPainter pp(this);// = new QPainter;
	pp.begin(this);
	QBrush brush = QBrush(QColor(m_color));

	QPen pen = QPen(QColor(m_color));
	//pen.setWidth(5);
	// pen.setStyle(Qt::DashLine);
	pp.setPen(pen);
	pp.setBrush(brush);
	pp.drawRect(QRect(QPoint(0, 0), QPoint(width(), height())));
	//pp.end();
	//p->save();
	
	pp.beginNativePainting();

	//p->restore();
	//return;
	// QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

	//qglClearColor(QColor(m_color));
	// f->glClearColor(QColor(m_color).redF(),QColor(m_color).redF(),QColor(m_color).blueF(),1.0f);
	//if(xs == 0) return;
	//f->glClearColor(0.0,1.0,0.0,1.0);


	// Clear color and depth buffer
	//  f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	f->glEnable(GL_LINE_SMOOTH);
	f->glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	f->glEnable(GL_BLEND);
	f->glBlendEquation(GL_FUNC_ADD);
	f->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);



	const qreal zNear = 1.0, zFar = 70000.0, fov = 45.0;

	//glPushMatrix();

	//glLoadIdentity();
	//glOrtho(xmin,xmax,ymin,ymax,zNear,zFar);

	//glOrtho(xmin,xmax,ymin,ymax,zNear,zFar);

	QMatrix4x4 matrix;
	matrix.setToIdentity();

	projection.setToIdentity();
	projection.ortho(xmin, xmax, ymin, ymax, zNear, zFar);

	//matrix.translate(-5, -6, -50);
	//matrix.ortho(QRectF(-0.3f,0.9f,-1.0f,1.0f));

	//glPopMatrix();



	f->glUseProgram(0);
	//  glColor4f(1,0,0,1);
	f->glLineWidth(1);


	float min = -0.7;
	float max = 0.8;
	float d = .9f + .9f;


	/*
	// draw axis
	for(int i =0;i<99;i++){
	float ff = float(i)/float(98.0)*d - .9;
	glColor4f(.9,.9,.9,.2);
	glBegin(GL_LINES);
	glVertex3f(ff,max,0);
	glVertex3f(ff,min,0);
	glEnd();
	}
	*/

	f->glLineWidth(1);



	//oldmethod();

	// draw y-axis values
	float lmin = 0;
	float lmax = 1;
	QString units = QString(" ");

	switch (goff){
	case 0:

		//renderText(ff,0,0,QString("ID"));
		lmin = minx;//0.0f;
		lmax = maxx;//8900.0f;
		break;
	case 1:
	case 2: // ?
		lmin = 0.0f;
		lmax = 9000.0f;//2174267.0f;
		//renderText(ff,0,0,QString("Layout ID"));
		break;

	case 3:
		units = QString(" Log10(Msun /h)");
		//   units = QString(" Msun /h");
		//renderText(ff,0,0,QString("Mvir"));
		lmin = vminz;//19066000000.0f;
		lmax = vmaxz;//227220000000000.0f;
		break;

	case 4:

		units = QString(" Km /s");
		//   renderText(ff,0,0,QString("Velocity"));
		lmin = vmin; //0.0f;
		lmax = vmax; // 2196.426f;
		break;

	case 5:

		units = QString(" Msun /h");
		lmin = minz;//10.28026f;
		lmax = maxz;//14.35645f;
		break;

	}

	//emit this->newyllimit(lmin,lmax); // debug

	// draw y-axis values
	float cmin = 0;
	float cmax = 1;


	switch (coff){
	case 0:

		//renderText(ff,0,0,QString("ID"));
		cmin = minx;//0.0f;
		cmax = maxx;//8900.0f;
		break;
	case 1:
	case 2: // ?
		cmin = 0.0f;
		cmax = 9000.0f;//2174267.0f;
		//renderText(ff,0,0,QString("Layout ID"));
		break;

	case 3:
		//  units = QString(" Log10(Msun /h)");

		cmin = vminz;//19066000000.0f;
		cmax = vmaxz;//227220000000000.0f;
		break;

	case 4:

		//  units = QString(" Km /s");

		cmin = vmin; //0.0f;
		cmax = vmax; // 2196.426f;
		break;

	case 5:

		//  units = QString(" Msun /h");
		cmin = minz;//10.28026f;
		cmax = maxz;//14.35645f;
		break;

	}



	//new test

	f->glUseProgram(program.programId());



	// try vbo

	//glColor4f(1.0, 1.0, 0.0, 0.05);
	//glEnableVertexAttribArray(0);


	program.setUniformValue("p1", 0);

	program.setUniformValue("coff", coff);
	program.setUniformValue("goff", goff);
	program.setUniformValue("mvp_matrix", projection * matrix);

	program.setUniformValue("am", am);

	int deflt = program.attributeLocation("deflt");
	program.enableAttributeArray(deflt);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[0]);
	f->glVertexAttribPointer(deflt, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);

	int lo1 = program.attributeLocation("lo1");
	program.enableAttributeArray(lo1);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[1]);
	f->glVertexAttribPointer(lo1, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


	int lo2 = program.attributeLocation("lo2");
	program.enableAttributeArray(lo2);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[2]);
	f->glVertexAttribPointer(lo2, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


	int lmvir = program.attributeLocation("lmvir");
	program.enableAttributeArray(lmvir);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[3]);
	f->glVertexAttribPointer(lmvir, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);

	int vol = program.attributeLocation("vol");
	program.enableAttributeArray(vol);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[4]);
	f->glVertexAttribPointer(vol, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);

	int mvir = program.attributeLocation("mvir");
	program.enableAttributeArray(mvir);
	f->glBindBuffer(GL_ARRAY_BUFFER, vboIds2[5]);
	f->glVertexAttribPointer(mvir, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);

	/*  glVertexAttribPointer(
	0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
	4,                  // size
	GL_FLOAT,           // type
	GL_FALSE,           // normalized?
	0,                  // stride
	(void*)0            // array buffer offset
	);
	*/


	f->glActiveTexture(GL_TEXTURE0);
	f->glBindTexture(GL_TEXTURE_2D, tft);

	f->glDrawArrays(GL_LINES, 0, count);

	//glDisableVertexAttribArray(0);
	f->glDisableVertexAttribArray(deflt);
	f->glDisableVertexAttribArray(lo1);
	f->glDisableVertexAttribArray(lo2);
	f->glDisableVertexAttribArray(lmvir);
	f->glDisableVertexAttribArray(vol);
	f->glDisableVertexAttribArray(mvir);
	f->glBindBuffer(GL_ARRAY_BUFFER, 0);





	// vbo and sender list are filled

	f->glGenBuffers(1, activevbo);
	f->glBindBuffer(GL_ARRAY_BUFFER, activevbo[0]); //nrow* 4 *2
	f->glBufferData(GL_ARRAY_BUFFER, activeidsvbo.size()* sizeof(GLfloat), activeidsvbo.data(), GL_STATIC_DRAW);


	// end selection drawing
	f->glUseProgram(program.programId());



	// try vbo

	//glColor4f(1.0, 1.0, 0.0, 0.05);
	//glEnableVertexAttribArray(0);


	program.setUniformValue("p1", 0); // doesn't matter

	program.setUniformValue("coff", coff); // doesn't matter
	program.setUniformValue("goff", 0); // only use first vbo
	program.setUniformValue("mvp_matrix", projection * matrix);

	program.setUniformValue("am", 5); // draw selected as red

	int deflt2 = program.attributeLocation("deflt");
	program.enableAttributeArray(deflt2);
	f->glBindBuffer(GL_ARRAY_BUFFER, activevbo[0]);
	f->glVertexAttribPointer(deflt2, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


	/*  glVertexAttribPointer(
	0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
	4,                  // size
	GL_FLOAT,           // type
	GL_FALSE,           // normalized?
	0,                  // stride
	(void*)0            // array buffer offset
	);
	*/


	//  glActiveTexture(GL_TEXTURE0);
	//   glBindTexture(GL_TEXTURE_2D,tft);

	f->glDrawArrays(GL_LINES, 0, activeidsvbo.size());

	// send new info out
	//emit this->newAidsLoaded(senderids); //debug

	// return to normal
	f->glDisableVertexAttribArray(deflt);

	f->glActiveTexture(GL_TEXTURE0);
	f->glBindTexture(GL_TEXTURE_2D, 0);

	f->glBindBuffer(GL_ARRAY_BUFFER, 0);
	f->glUseProgram(0);

	//

	// f->glDeleteBuffers(1,activevbo);
	// activeidsvbo.clear();

	// draw selection lines

	if (sel != 0 && sel->isVisible()){

		glPushAttrib(GL_ENABLE_BIT);

		glColor4f(1.0, 1.0, 1.0, 1.0);

		float   ub = float(sel->geometry().top()) / float(height());

		float   lb = float(sel->geometry().bottom()) / float(height());

		float   ts1 = float(sel->geometry().left()) / float(width());

		float   ts2 = float(sel->geometry().right()) / float(width());





		ub = 1.0f - ub;
		lb = 1.0f - lb;

		float yymax = lerp(ub, ymin, ymax);//((ub - .85)*(1.0f - 0.0f)/(.10-.85));
		float yymin = lerp(lb, ymin, ymax);//((lb - .85)*(1.0f - 0.0f)/(.10-.85));

		float xxmin = lerp(ts1, xmin, xmax);// ((ts1 - .85)*(1.0f - 0.0f)/(.10-.85));
		float xxmax = lerp(ts2, xmin, xmax);//((ts2 - .85)*(1.0f - 0.0f)/(.10-.85));

		std::vector<GLfloat> lines;

		lines.push_back(-1);
		lines.push_back(yymax);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymax);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(-1);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmin);
		lines.push_back(ymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmax);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmax);
		lines.push_back(ymin);
		lines.push_back(-1);
		lines.push_back(1);

		GLuint dlbuff;

		f->glGenBuffers(1, &dlbuff);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff); //nrow* 4 *2
		f->glBufferData(GL_ARRAY_BUFFER, lines.size()* sizeof(GLfloat), lines.data(), GL_STATIC_DRAW);


		// end selection drawing
		f->glUseProgram(program.programId());

		//glLineStipple(1,0x3F07);
		//glEnable(GL_LINE_STIPPLE);

		// try vbo

		//glColor4f(1.0, 1.0, 0.0, 0.05);
		//glEnableVertexAttribArray(0);


		program.setUniformValue("p1", 0); // doesn't matter

		program.setUniformValue("coff", coff); // doesn't matter
		program.setUniformValue("goff", 0); // only use first vbo
		program.setUniformValue("mvp_matrix", projection * matrix);

		program.setUniformValue("am", 0); // draw selected as red

		int deflt2 = program.attributeLocation("deflt");
		program.enableAttributeArray(deflt2);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff);
		f->glVertexAttribPointer(deflt2, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


		/*  glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		4,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);
		*/


		//  glActiveTexture(GL_TEXTURE0);
		//   glBindTexture(GL_TEXTURE_2D,tft);

		f->glDrawArrays(GL_LINES, 0, lines.size());

		glPopAttrib();

		f->glDeleteBuffers(1, &dlbuff);

	}


	if (zsel != 0 && zsel->isVisible()){

		glPushAttrib(GL_ENABLE_BIT);

		glColor4f(1.0, 1.0, 1.0, 1.0);

		float   ub = float(zsel->geometry().top()) / float(height());

		float   lb = float(zsel->geometry().bottom()) / float(height());

		float   ts1 = float(zsel->geometry().left()) / float(width());

		float   ts2 = float(zsel->geometry().right()) / float(width());





		ub = 1.0f - ub;
		lb = 1.0f - lb;

		float yymax = lerp(ub, ymin, ymax);//((ub - .85)*(1.0f - 0.0f)/(.10-.85));
		float yymin = lerp(lb, ymin, ymax);//((lb - .85)*(1.0f - 0.0f)/(.10-.85));

		float xxmin = lerp(ts1, xmin, xmax);// ((ts1 - .85)*(1.0f - 0.0f)/(.10-.85));
		float xxmax = lerp(ts2, xmin, xmax);//((ts2 - .85)*(1.0f - 0.0f)/(.10-.85));

		std::vector<GLfloat> lines;

		lines.push_back(-1);
		lines.push_back(yymax);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymax);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(-1);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(xxmin);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmin);
		lines.push_back(ymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmax);
		lines.push_back(yymin);
		lines.push_back(-1);
		lines.push_back(1);
		lines.push_back(xxmax);
		lines.push_back(ymin);
		lines.push_back(-1);
		lines.push_back(1);

		GLuint dlbuff;

		f->glGenBuffers(1, &dlbuff);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff); //nrow* 4 *2
		f->glBufferData(GL_ARRAY_BUFFER, lines.size()* sizeof(GLfloat), lines.data(), GL_STATIC_DRAW);


		// end selection drawing
		f->glUseProgram(program.programId());

		//glLineStipple(1,0x3F07);
		//glEnable(GL_LINE_STIPPLE);

		// try vbo

		//glColor4f(1.0, 1.0, 0.0, 0.05);
		//glEnableVertexAttribArray(0);


		program.setUniformValue("p1", 0); // doesn't matter

		program.setUniformValue("coff", coff); // doesn't matter
		program.setUniformValue("goff", 0); // only use first vbo
		program.setUniformValue("mvp_matrix", projection * matrix);

		program.setUniformValue("am", 0); // draw selected as red

		int deflt2 = program.attributeLocation("deflt");
		program.enableAttributeArray(deflt2);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff);
		f->glVertexAttribPointer(deflt2, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


		/*  glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		4,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);
		*/


		//  glActiveTexture(GL_TEXTURE0);
		//   glBindTexture(GL_TEXTURE_2D,tft);

		f->glDrawArrays(GL_LINES, 0, lines.size());

		glPopAttrib();

		f->glDeleteBuffers(1, &dlbuff);

	}


	if (activetime > -1){

		glPushAttrib(GL_ENABLE_BIT);

		glColor4f(1.0, 1.0, 1.0, 1.0);


		float ff = float(activetime) / float(98.0)*d - .9;

		std::vector<GLfloat> lines;

		lines.push_back(ff);
		lines.push_back(1);
		lines.push_back(-1);
		lines.push_back(1);

		lines.push_back(ff);
		lines.push_back(-1);
		lines.push_back(-1);
		lines.push_back(1);


		GLuint dlbuff;

		f->glGenBuffers(1, &dlbuff);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff); //nrow* 4 *2
		f->glBufferData(GL_ARRAY_BUFFER, lines.size()* sizeof(GLfloat), lines.data(), GL_STATIC_DRAW);


		// end selection drawing
		f->glUseProgram(program.programId());

		//glLineStipple(1,0x3F07);
		//glEnable(GL_LINE_STIPPLE);

		// try vbo

		//glColor4f(1.0, 1.0, 0.0, 0.05);
		//glEnableVertexAttribArray(0);


		program.setUniformValue("p1", 0); // doesn't matter

		program.setUniformValue("coff", coff); // doesn't matter
		program.setUniformValue("goff", 0); // only use first vbo
		program.setUniformValue("mvp_matrix", projection * matrix);

		program.setUniformValue("am", 0); // draw selected as red

		int deflt2 = program.attributeLocation("deflt");
		program.enableAttributeArray(deflt2);
		f->glBindBuffer(GL_ARRAY_BUFFER, dlbuff);
		f->glVertexAttribPointer(deflt2, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);


		f->glDrawArrays(GL_LINES, 0, lines.size());

		glPopAttrib();

		f->glDeleteBuffers(1, &dlbuff);

	}
	// end new test



	f->glBindBuffer(GL_ARRAY_BUFFER, 0);


	f->glUseProgram(0);

	// draw labels

	QString xlab = "Time";

	QString ylab = "ID";

	int i = 48;
	float ff = float(i) / float(98.0)*d - .9;
	glColor4f(.9, .9, .9, 1);
	//renderText(ff,-0.94,0.0,QString("Time"));

	switch (goff){
	case 0:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("ID"));
		ylab = "ID";
		break;
	case 1:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("Layout 1 ID"));
		ylab = "Layout 1 ID";
		break;

	case 2:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("Layout 2 ID"));
		ylab = "Layout 2 ID";
		break;

	case 3:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("Mvir (log)"));
		ylab = "Mvir (log)";
		break;

	case 4:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("Velocity"));
		ylab = "Velocity";
		break;

	case 5:
		ff = float(-5) / float(98.0)*d - .9;
		glColor4f(.9, .9, .9, 1);
		//renderText(ff,0.0,0.0,QString("Mvir"));
		ylab = "Mvir";
		break;

	}

	float pmin = -0.7;
	float pmax = 0.8;

	//   for(int i =0;i<99;i++){

	//float ff = float(i)/float(98.0)*d - .9;
	//      float aa = 0.05f + (0.90f)*float(i)/98.0f;

	//   if(mousex > aa - 0.001f && mousex < aa + 0.001f){


	/*
	glUseProgram(0);
	glColor4f(1,0,0,.5);
	glLineWidth(1.5);




	glBegin(GL_LINES);
	glVertex3f(ff,max,0);
	glVertex3f(ff,min,0);
	glEnd();



	glColor4f(.9,.9,.9,1);
	renderText(ff,-0.84,0,QString::number(i));


	if(mousey > 0.10 && mousey < 0.85){
	*/
	// [.85,.10] to range [min,max]
	//

	// calculate y lable at mouse point
	float t = lerp((1.0f - mousey), ymin, ymax);

	t = (t - pmin)*(lmax - lmin) / (pmax - pmin) + lmin;

	QString ytool = QString::number(t, 'g', 6).append(units);

	// calculate x lable at mouse point
	t = lerp(mousex, xmin, xmax);

	float smin = -0.9f;
	float smax = 0.9f;

	t = (t - smin)*(98.0f - 0.0f) / (smax - smin) + 0.0f;

	QString xtool = QString(" \nTime: ").append(QString::number(t, 'g', 6));

	QString final = QString(ylab).append(" ").append(ytool).append(xtool);

	QToolTip::showText(QPoint(gmousex, gmousey), final, 0, QRect(), 3000);
	//  }
	//  }

	// }



	//emit this->newylab(ylab.append(units)); //debug
	//emit this->newxlab(xlab);
	//

	/*

	f->glEnable(GL_LINE_SMOOTH);
	f->glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	f->glEnable(GL_BLEND);
	f->glBlendEquation(GL_FUNC_ADD);
	f->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	*/



	// draw ticks

	// y ticks
	/*
	ff = float(-5)/float(98.0)*d - .9;
	glColor4f(.9,.9,.9,1);
	renderText(ff,min -0.05f,0.0,QString::number(lmin).append(units));
	glColor4f(.9,.9,.9,1);
	renderText(ff,max +0.05f,0.0,QString::number(lmax).append(units));



	// x ticks

	i = 0;
	ff = float(i)/float(98.0)*d - .9;
	glColor4f(.9,.9,.9,1);
	renderText(ff,-0.84,0.0,QString::number(i));

	i = 48;
	ff = float(i)/float(98.0)*d - .9;
	glColor4f(.9,.9,.9,1);
	renderText(ff,-0.84,0.0,QString::number(i));

	i = 98;
	ff = float(i)/float(98.0)*d - .9;
	glColor4f(.9,.9,.9,1);
	renderText(ff,-0.84,0.0,QString::number(i));
	*/


	pp.endNativePainting();

	if (zsel != 0)
		zsel->draw(pp);
	if (sel != 0)
		if (sel->isVisible())
			sel->draw(pp);

	pp.end();


}


void PC::drawparents(int time, int id){
	float d = .9f + .9f;

	float pmin = -0.7;//-2.9;
	float pmax = 0.8;

	if (time >= 0 && time < mergdata.size() && time == activetime){ // to avoid seg fault
		senderids.push_back(mergdata[time][id].id);
	}

	if (time == 0) return;
	// draw parents

	if (time > 0){

		for (int i = 0; i < mergdata[time][id].pids.size(); i++){

			float temp = 0;
			float temp2 = 0;
			float yt = 0;

			/*     if(goff == 0){
			temp = (float(mergdata[time][id].id) - float(minx)) /(float(maxx) -  float(minx));
			temp2 = (float(mergdata[time-1][mergdata[time][id].pids[i]].id) - float(minx)) /(float(maxx) -  float(minx));

			}

			else{
			temp = (float(id) / 9000.0f);

			yt = temp;
			temp2 = (float(mergdata[time][id].pids[i]) / 9000.0f);

			}
			*/
			switch (goff){
			case 0:

				temp = (float(mergdata[time][id].id) - float(minx)) / (float(maxx) - float(minx));
				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].id) - float(minx)) / (float(maxx) - float(minx));

				break;
			case 1:

				temp = (float(id) / 9000.0f);

				yt = temp;
				temp2 = (float(mergdata[time][id].pids[i]) / 9000.0f);

				break;

			case 2:
				temp = (float(id) / float(mergdata[time].size()));

				yt = temp;
				temp2 = (float(mergdata[time][id].pids[i]) / float(mergdata[time - 1].size()));
				break;

			case 3:
				temp = (float(mergdata[time][id].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

				break;

			case 4:
				temp = (float(mergdata[time][id].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[time][id].vars[0]) - float(minz)) / (float(maxz) - float(minz));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				break;

			}

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			float idx = float(time) / float(98.0f)*d - .9f;
			float idx2 = float(time - 1) / float(98.0f)*d - .9f;

			/*   glBegin(GL_LINES);

			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx, temp, -1.0f);
			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx2, temp2, -1.0f);

			glEnd();
			*/
			activeidsvbo.push_back(idx);
			activeidsvbo.push_back(temp);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			activeidsvbo.push_back(idx2);
			activeidsvbo.push_back(temp2);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			// and recurse

			drawparents(time - 1, mergdata[time][id].pids[i]);
		}

	}


}

void PC::drawchilds(int time, int id){
	float d = .9f + .9f;

	float pmin = -0.7;//-2.9;
	float pmax = 0.8;
	// draw children

	if (time >= 0 && time < mergdata.size() && time == activetime){ // to avoid seg fault
		senderids.push_back(mergdata[time][id].id);
	}

	if (time == 88) return;

	if (time < 88){

		for (int i = 0; i < mergdata[time][id].cids.size(); i++){

			float temp = 0;
			float temp2 = 0;
			float yt = 0;

			switch (goff){
			case 0:

				temp = (float(mergdata[time][id].id) - float(minx)) / (float(maxx) - float(minx));
				temp2 = (float(mergdata[time + 1][mergdata[time][id].cids[i]].id) - float(minx)) / (float(maxx) - float(minx));

				break;
			case 1:

				temp = (float(id) / 9000.0f);

				yt = temp;
				temp2 = (float(mergdata[time][id].cids[i]) / 9000.0f);

				break;

			case 2:
				temp = (float(id) / float(mergdata[time].size()));

				yt = temp;
				temp2 = (float(mergdata[time][id].cids[i]) / float(mergdata[time + 1].size()));
				break;

			case 3:
				temp = (float(mergdata[time][id].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

				break;

			case 4:
				temp = (float(mergdata[time][id].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[time][id].vars[0]) - float(minz)) / (float(maxz) - float(minz));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				break;
			}


			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			float idx = float(time) / float(98.0f)*d - .9f;
			float idx2 = float(time + 1) / float(98.0f)*d - .9f;

			/*    glBegin(GL_LINES);

			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx, temp, -1.0f);
			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx2, temp2, -1.0f);

			glEnd();
			*/

			activeidsvbo.push_back(idx);
			activeidsvbo.push_back(temp);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			activeidsvbo.push_back(idx2);
			activeidsvbo.push_back(temp2);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			// and recurse

			drawchilds(time + 1, mergdata[time][id].cids[i]);
		}

	}


}

void PC::drawpath(int time, int id){ // id is index

	// glUseProgram(0);
	float d = .9f + .9f;

	float pmin = -0.7;//-2.9;
	float pmax = 0.8;

	//senderids.clear();

	if (time >= 0 && time < mergdata.size() && time == activetime){ // to avoid seg fault
		senderids.push_back(mergdata[time][id].id);
	}
	// draw parents

	if (time > 0){

		for (int i = 0; i < mergdata[time][id].pids.size(); i++){
			//float temp = (float(mergdata[time][id].id) - float(minx)) /(float(maxx) -  float(minx));
			// float temp2 = (float(mergdata[time-1][mergdata[time][id].pids[i]].id) - float(minx)) /(float(maxx) -  float(minx));

			float temp = 0;
			float temp2 = 0;
			float yt = 0;

			switch (goff){
			case 0:

				temp = (float(mergdata[time][id].id) - float(minx)) / (float(maxx) - float(minx));
				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].id) - float(minx)) / (float(maxx) - float(minx));

				break;
			case 1:

				temp = (float(id) / 9000.0f);

				yt = temp;
				temp2 = (float(mergdata[time][id].pids[i]) / 9000.0f);

				break;

			case 2:
				temp = (float(id) / float(mergdata[time].size()));

				yt = temp;
				temp2 = (float(mergdata[time][id].pids[i]) / float(mergdata[time - 1].size()));
				break;

			case 3:
				temp = (float(mergdata[time][id].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

				break;

			case 4:
				temp = (float(mergdata[time][id].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[time][id].vars[0]) - float(minz)) / (float(maxz) - float(minz));


				temp2 = (float(mergdata[time - 1][mergdata[time][id].pids[i]].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				break;

			}

			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			float idx = float(time) / float(98.0f)*d - .9f;
			float idx2 = float(time - 1) / float(98.0f)*d - .9f;

			/*     glBegin(GL_LINES);

			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx, temp, -1.0f);
			glColor4f(1.0, 0.0, 0.0, 0.05);
			glVertex3f(idx2, temp2, -1.0f);

			glEnd();
			*/

			activeidsvbo.push_back(idx);
			activeidsvbo.push_back(temp);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			activeidsvbo.push_back(idx2);
			activeidsvbo.push_back(temp2);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			// and recurse

			drawparents(time - 1, mergdata[time][id].pids[i]);
		}

	}

	// draw children

	if (time < 88){

		for (int i = 0; i < mergdata[time][id].cids.size(); i++){
			// float temp = (float(mergdata[time][id].id) - float(minx)) /(float(maxx) -  float(minx));
			// float temp2 = (float(mergdata[time+1][mergdata[time][id].cids[i]].id) - float(minx)) /(float(maxx) -  float(minx));

			float temp = 0;
			float temp2 = 0;
			float yt = 0;

			/*      if(goff == 0){
			temp = (float(mergdata[time][id].id) - float(minx)) /(float(maxx) -  float(minx));
			temp2 = (float(mergdata[time+1][mergdata[time][id].cids[i]].id) - float(minx)) /(float(maxx) -  float(minx));

			}

			else{
			temp = (float(id) / 9000.0f);

			yt = temp;
			temp2 = (float(mergdata[time][id].cids[i]) / 9000.0f);

			}
			*/

			switch (goff){
			case 0:

				temp = (float(mergdata[time][id].id) - float(minx)) / (float(maxx) - float(minx));
				temp2 = (float(mergdata[time + 1][mergdata[time][id].cids[i]].id) - float(minx)) / (float(maxx) - float(minx));

				break;
			case 1:

				temp = (float(id) / 9000.0f);

				yt = temp;
				temp2 = (float(mergdata[time][id].cids[i]) / 9000.0f);

				break;

			case 2:
				temp = (float(id) / float(mergdata[time].size()));

				yt = temp;
				temp2 = (float(mergdata[time][id].cids[i]) / float(mergdata[time + 1].size()));
				break;

			case 3:
				temp = (float(mergdata[time][id].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[1]) - float(vminz)) / (float(vmaxz) - float(vminz));

				break;

			case 4:
				temp = (float(mergdata[time][id].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[2]) - float(vmin)) / (float(vmax) - float(vmin));

				break;


			case 5: //mvir

				temp = (float(mergdata[time][id].vars[0]) - float(minz)) / (float(maxz) - float(minz));


				temp2 = (float(mergdata[time + 1][mapper[mergdata[time][id].childid]].vars[0]) - float(minz)) / (float(maxz) - float(minz));

				break;

			}


			temp = temp*(pmax - pmin) + pmin;
			temp2 = temp2*(pmax - pmin) + pmin;
			float idx = float(time) / float(98.0f)*d - .9f;
			float idx2 = float(time + 1) / float(98.0f)*d - .9f;


			activeidsvbo.push_back(idx);
			activeidsvbo.push_back(temp);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			activeidsvbo.push_back(idx2);
			activeidsvbo.push_back(temp2);
			activeidsvbo.push_back(-1.0f);
			activeidsvbo.push_back(1.0f);

			// and recurse

			drawchilds(time + 1, mergdata[time][id].cids[i]);
		}

	}

	return;



}

void PC::oldmethod(){
	glUseProgram(0);
	float d = .9f + .9f;

	float pmin = -0.7;//-2.9;
	float pmax = 0.8;



	int activestep = -1;
	for (int i = 0; i < 99; i++){ //set active axis
		float aa = 0.05f + (0.90f)*float(i) / 98.0f;

		if (mousex > aa - 0.001f && mousex < aa + 0.001f){
			activestep = i;
			//  qDebug() << "Active axis: " << i;
			break;
		}

	}



	int k = 0;
	int lit = 0;

	std::vector <float> temp;
	std::vector <float> tempcol;
	std::vector <float> idx;

	for (int j = 0; j < dat.size(); j += 5){


		if (k == 0){

		}

		if (dat[j] > -1.0f){

			temp.push_back(dat[j + goff] * (pmax - pmin) + pmin);
			tempcol.push_back(dat[j + coff]);
			idx.push_back(float(k) / float(98.0f)*d - .9f);

			float yy = ((mousey - .85)*(1.0f - 0.0f) / (.10 - .85));
			float yt = dat[j + goff];//*(pmax - pmin) + pmin;
			if (activestep == k &&
				(yt > yy - 0.01f && yt < yy + 0.01f)){

				lit = 1;
			}
		}


		if (k == 98){

			glBegin(GL_LINE_STRIP);

			for (int i = 0; i < temp.size(); i++){

				if (coff == -1){
					if (lit > 0){
						glColor4f(1.0, 1.0, 0.0, 0.05);
						glVertex3f(idx[i], temp[i], -1.0f);
					}
					else {
						glColor4f(0.7, 0.7, 1.0, 0.01);
						glVertex3f(idx[i], temp[i], 0.0f);
					}

				}
				else{
					if (lit > 0){
						glColor4f(lerp(tempcol[i], float(float(QColor(cm_color).red()) / 255.0f), float(float(QColor(cl_color).red()) / 255.0f)), lerp(tempcol[i], float(float(QColor(cm_color).green()) / 255.0f), float(float(QColor(cl_color).green()) / 255.0f)), lerp(tempcol[i], float(float(QColor(cm_color).blue()) / 255.0f), float(float(QColor(cl_color).blue()) / 255.0f)), 0.5);
						glVertex3f(idx[i], temp[i], 1.0f);
					}
					else {
						glColor4f(lerp(tempcol[i], float(float(QColor(cm_color).red()) / 255.0f), float(float(QColor(cl_color).red()) / 255.0f)), lerp(tempcol[i], float(float(QColor(cm_color).green()) / 255.0f), float(float(QColor(cl_color).green()) / 255.0f)), lerp(tempcol[i], float(float(QColor(cm_color).blue()) / 255.0f), float(float(QColor(cl_color).blue()) / 255.0f)), 0.1);
						glVertex3f(idx[i], temp[i], 0.0f);
					}

				}

			}
			//clean up
			temp.clear();
			idx.clear();
			tempcol.clear();



			glEnd();

			k = 0;
			lit = 0;
		}
		else{
			k++;
		}


	}

}
/*
void PC::populateScene()
{
	//scene = new QGraphicsScene;

	/*  QImage image(":/qt4logo.png");

	// Populate scene
	int xx = 0;
	int nitems = 0;
	for (int i = -11000; i < 11000; i += 110) {
	++xx;
	int yy = 0;
	for (int j = -7000; j < 7000; j += 70) {
	++yy;
	qreal x = (i + 11000) / 22000.0;
	qreal y = (j + 7000) / 14000.0;

	QColor color(image.pixel(int(image.width() * x), int(image.height() * y)));
	QGraphicsItem *item = new Series(color, xx, yy);
	item->setPos(QPointF(i, j));
	scene->addItem(item);

	++nitems;
	}

}*/