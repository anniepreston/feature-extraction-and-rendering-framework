#ifndef _VOXELIZER_H_
#define _VOXELIZER_H_
#include <QOpenGLWidget>
#include <vec3f.h>
#include <vector>
#include <GLTextureBufferObject.h>
#include <GLTexture3D.h>

class Voxelizer
{
public:
	Voxelizer();
	//void setParticleArray(const std::vector<lily::vec3f>& particles);
	void voxelize(const std::vector<lily::vec3f>& particles, int w, int h, int d);
	~Voxelizer();
	lily::GLTexture3DRef Smoothing(const lily::GLTexture3DRef inTex3d);
private:
	lily::GLTextureBufferObjectRef m_tboParticles;
	lily::GLTexture3DRef m_tex3d;
};

#endif
