#ifndef TABONEWIDGET_H
#define TABONEWIDGET_H

#include <QWidget>
#include "ui_tabonewidget.h"

class tabOneWidget : public QWidget
{
	Q_OBJECT

public:
	tabOneWidget(QWidget *parent = 0);
	~tabOneWidget();

private:
	Ui::tabOneWidget ui;
};

#endif // TABONEWIDGET_H
