
#ifndef VIEW_H
#define VIEW_H

#include <QtGui>
#include <QWidget>
#include <QMouseEvent>
#include <vector>

#define terpi(t,a,b) (( t - a)/(b - a))
#define lerp(t, a, b) ( a + t * (b - a) )

class View : public QWidget
{
	Q_OBJECT

public:
    explicit View(const QString &name, QWidget *parent = 0);
     //  std::vector<Series*> series;
      QSet<int> cset;
       QStringList bah;

           void setOrientation(int ori);

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    float mousex,mousey;
    float xmin, xmax;
    float lxmin, lxmax;
    float smin,smax;
    float ymin, ymax;
    float lymin, lymax;
    float fontsize;
    QString lable;

    int ori;


public slots:
    void zoomIn(int level = 1);
    void zoomOut(int level = 1);
   // void onNewData(std::vector<Series*> &test);
        void onNewDataset(QSet<int> &test);
        void onnewmouse(float x, float y);
        void onnewlimit(float min, float max);
                void onnewllimit(float min, float max);
                void onnewlab(QString &lab);
                void onnewfontsize(float f);
signals:
    //void newActivated(int s1,int s2,int t1, int t2, int t3, int t4);
    void newActivated(int s1,int s2,float lv, float lh);//int t1, int t2, int t3, int t4);
    void newmouse(float x, float y);

private slots:

    void resetView();
    void setResetButtonEnabled();
    void setupMatrix();
    void togglePointerMode();
    void toggleOpenGL();
    void toggleAntialiasing();
    void print();
    void rotateLeft();
    void rotateRight();

};

#endif // VIEW_H
