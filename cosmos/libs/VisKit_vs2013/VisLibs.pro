TEMPLATE = subdirs
QT       += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


CONFIG += ordered debug_and_release
CONFIG(debug, debug|release):DEFINES += DEBUG

SUBDIRS += 	camera \
			shadermanager \
			UI \
			optionmanager/optionmanager.pro \
			Render/renderlib.pro \
			animator \
			preintegration
