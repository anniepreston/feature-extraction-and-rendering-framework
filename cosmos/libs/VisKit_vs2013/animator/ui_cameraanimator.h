/********************************************************************************
** Form generated from reading UI file 'cameraanimator.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CAMERAANIMATOR_H
#define UI_CAMERAANIMATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_CANWidget
{
public:
    QVBoxLayout *verticalLayout;
    QGridLayout *gridLayout;
    QLabel *label;
    QDoubleSpinBox *tspin;
    QLabel *label_2;
    QDoubleSpinBox *pspin;
    QLabel *label_3;
    QDoubleSpinBox *ux;
    QDoubleSpinBox *uy;
    QDoubleSpinBox *uz;
    QLabel *label_4;
    QDoubleSpinBox *lx;
    QDoubleSpinBox *ly;
    QDoubleSpinBox *lz;
    QLabel *label_5;
    QDoubleSpinBox *px;
    QDoubleSpinBox *py;
    QDoubleSpinBox *pz;
    QLabel *label_6;
    QDoubleSpinBox *dtspin;
    QHBoxLayout *horizontalLayout;
    QPushButton *apply;
    QPushButton *deselect;
    QPushButton *reset;
    QListWidget *listWidget;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *deleteframe;
    QPushButton *insertbutton;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *movedown;
    QPushButton *moveup;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *playbutton;
    QPushButton *stopbutton;
    QPushButton *playtoframe;
    QSpinBox *endframe;

    void setupUi(QWidget *CANWidget)
    {
        if (CANWidget->objectName().isEmpty())
            CANWidget->setObjectName(QStringLiteral("CANWidget"));
        CANWidget->resize(490, 833);
        verticalLayout = new QVBoxLayout(CANWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(CANWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        tspin = new QDoubleSpinBox(CANWidget);
        tspin->setObjectName(QStringLiteral("tspin"));

        gridLayout->addWidget(tspin, 0, 1, 1, 1);

        label_2 = new QLabel(CANWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        pspin = new QDoubleSpinBox(CANWidget);
        pspin->setObjectName(QStringLiteral("pspin"));

        gridLayout->addWidget(pspin, 1, 1, 1, 1);

        label_3 = new QLabel(CANWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 2, 0, 1, 1);

        ux = new QDoubleSpinBox(CANWidget);
        ux->setObjectName(QStringLiteral("ux"));
        ux->setDecimals(8);
        ux->setMinimum(-9001);
        ux->setMaximum(9001);
        ux->setSingleStep(1e-06);

        gridLayout->addWidget(ux, 2, 1, 1, 1);

        uy = new QDoubleSpinBox(CANWidget);
        uy->setObjectName(QStringLiteral("uy"));
        uy->setDecimals(8);
        uy->setMinimum(-9001);
        uy->setMaximum(9001);
        uy->setSingleStep(1e-06);

        gridLayout->addWidget(uy, 2, 2, 1, 1);

        uz = new QDoubleSpinBox(CANWidget);
        uz->setObjectName(QStringLiteral("uz"));
        uz->setDecimals(8);
        uz->setMinimum(-9001);
        uz->setMaximum(9001);
        uz->setSingleStep(1e-06);

        gridLayout->addWidget(uz, 2, 3, 1, 1);

        label_4 = new QLabel(CANWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 3, 0, 1, 1);

        lx = new QDoubleSpinBox(CANWidget);
        lx->setObjectName(QStringLiteral("lx"));
        lx->setDecimals(8);
        lx->setMinimum(-9001);
        lx->setMaximum(9001);
        lx->setSingleStep(1e-06);

        gridLayout->addWidget(lx, 3, 1, 1, 1);

        ly = new QDoubleSpinBox(CANWidget);
        ly->setObjectName(QStringLiteral("ly"));
        ly->setDecimals(8);
        ly->setMinimum(-9001);
        ly->setMaximum(9001);
        ly->setSingleStep(1e-06);

        gridLayout->addWidget(ly, 3, 2, 1, 1);

        lz = new QDoubleSpinBox(CANWidget);
        lz->setObjectName(QStringLiteral("lz"));
        lz->setDecimals(8);
        lz->setMinimum(-9001);
        lz->setMaximum(9001);
        lz->setSingleStep(1e-06);

        gridLayout->addWidget(lz, 3, 3, 1, 1);

        label_5 = new QLabel(CANWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 4, 0, 1, 1);

        px = new QDoubleSpinBox(CANWidget);
        px->setObjectName(QStringLiteral("px"));
        px->setDecimals(8);
        px->setMinimum(-9001);
        px->setMaximum(9001);
        px->setSingleStep(1e-06);

        gridLayout->addWidget(px, 4, 1, 1, 1);

        py = new QDoubleSpinBox(CANWidget);
        py->setObjectName(QStringLiteral("py"));
        py->setDecimals(8);
        py->setMinimum(-9001);
        py->setMaximum(9001);
        py->setSingleStep(1e-06);

        gridLayout->addWidget(py, 4, 2, 1, 1);

        pz = new QDoubleSpinBox(CANWidget);
        pz->setObjectName(QStringLiteral("pz"));
        pz->setDecimals(8);
        pz->setMinimum(-9001);
        pz->setMaximum(9001);
        pz->setSingleStep(1e-06);

        gridLayout->addWidget(pz, 4, 3, 1, 1);

        label_6 = new QLabel(CANWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        gridLayout->addWidget(label_6, 0, 2, 1, 1);

        dtspin = new QDoubleSpinBox(CANWidget);
        dtspin->setObjectName(QStringLiteral("dtspin"));

        gridLayout->addWidget(dtspin, 0, 3, 1, 1);


        verticalLayout->addLayout(gridLayout);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        apply = new QPushButton(CANWidget);
        apply->setObjectName(QStringLiteral("apply"));

        horizontalLayout->addWidget(apply);

        deselect = new QPushButton(CANWidget);
        deselect->setObjectName(QStringLiteral("deselect"));

        horizontalLayout->addWidget(deselect);

        reset = new QPushButton(CANWidget);
        reset->setObjectName(QStringLiteral("reset"));

        horizontalLayout->addWidget(reset);


        verticalLayout->addLayout(horizontalLayout);

        listWidget = new QListWidget(CANWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));

        verticalLayout->addWidget(listWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        deleteframe = new QPushButton(CANWidget);
        deleteframe->setObjectName(QStringLiteral("deleteframe"));

        horizontalLayout_2->addWidget(deleteframe);

        insertbutton = new QPushButton(CANWidget);
        insertbutton->setObjectName(QStringLiteral("insertbutton"));

        horizontalLayout_2->addWidget(insertbutton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        movedown = new QPushButton(CANWidget);
        movedown->setObjectName(QStringLiteral("movedown"));

        horizontalLayout_3->addWidget(movedown);

        moveup = new QPushButton(CANWidget);
        moveup->setObjectName(QStringLiteral("moveup"));

        horizontalLayout_3->addWidget(moveup);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        playbutton = new QPushButton(CANWidget);
        playbutton->setObjectName(QStringLiteral("playbutton"));

        horizontalLayout_4->addWidget(playbutton);

        stopbutton = new QPushButton(CANWidget);
        stopbutton->setObjectName(QStringLiteral("stopbutton"));

        horizontalLayout_4->addWidget(stopbutton);

        playtoframe = new QPushButton(CANWidget);
        playtoframe->setObjectName(QStringLiteral("playtoframe"));

        horizontalLayout_4->addWidget(playtoframe);

        endframe = new QSpinBox(CANWidget);
        endframe->setObjectName(QStringLiteral("endframe"));
        endframe->setMinimum(1);
        endframe->setMaximum(1000000);

        horizontalLayout_4->addWidget(endframe);


        verticalLayout->addLayout(horizontalLayout_4);


        retranslateUi(CANWidget);

        QMetaObject::connectSlotsByName(CANWidget);
    } // setupUi

    void retranslateUi(QWidget *CANWidget)
    {
        CANWidget->setWindowTitle(QApplication::translate("CANWidget", "Camera Animator Options", 0));
        label->setText(QApplication::translate("CANWidget", "Time:", 0));
        label_2->setText(QApplication::translate("CANWidget", "Pause:", 0));
        label_3->setText(QApplication::translate("CANWidget", "Up:", 0));
        label_4->setText(QApplication::translate("CANWidget", "Look:", 0));
        label_5->setText(QApplication::translate("CANWidget", "Position:", 0));
        label_6->setText(QApplication::translate("CANWidget", "Default T:", 0));
        apply->setText(QApplication::translate("CANWidget", "Apply", 0));
        deselect->setText(QApplication::translate("CANWidget", "De-Select", 0));
        reset->setText(QApplication::translate("CANWidget", "Reset", 0));
        deleteframe->setText(QApplication::translate("CANWidget", "Delete Key Frame", 0));
        insertbutton->setText(QApplication::translate("CANWidget", "Insert Key Frame", 0));
        movedown->setText(QApplication::translate("CANWidget", "Move Down", 0));
        moveup->setText(QApplication::translate("CANWidget", "Move Up", 0));
        playbutton->setText(QApplication::translate("CANWidget", "Play", 0));
        stopbutton->setText(QApplication::translate("CANWidget", "Stop", 0));
        playtoframe->setText(QApplication::translate("CANWidget", "Play To Frame", 0));
    } // retranslateUi

};

namespace Ui {
    class CANWidget: public Ui_CANWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CAMERAANIMATOR_H
