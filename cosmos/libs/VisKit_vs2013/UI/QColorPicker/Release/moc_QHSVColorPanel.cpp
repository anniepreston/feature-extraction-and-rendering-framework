/****************************************************************************
** Meta object code from reading C++ file 'QHSVColorPanel.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.4.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../QHSVColorPanel.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'QHSVColorPanel.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.4.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_QHSVColorPanel_t {
    QByteArrayData data[8];
    char stringdata[141];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QHSVColorPanel_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QHSVColorPanel_t qt_meta_stringdata_QHSVColorPanel = {
    {
QT_MOC_LITERAL(0, 0, 14), // "QHSVColorPanel"
QT_MOC_LITERAL(1, 15, 16), // "hsvColorChangedH"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 17), // "hsvColorChangedSV"
QT_MOC_LITERAL(4, 51, 23), // "changeBackgroundToBlack"
QT_MOC_LITERAL(5, 75, 22), // "changeBackgroundToGray"
QT_MOC_LITERAL(6, 98, 23), // "changeBackgroundToWhite"
QT_MOC_LITERAL(7, 122, 18) // "turnOnOffColorInfo"

    },
    "QHSVColorPanel\0hsvColorChangedH\0\0"
    "hsvColorChangedSV\0changeBackgroundToBlack\0"
    "changeBackgroundToGray\0changeBackgroundToWhite\0"
    "turnOnOffColorInfo"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QHSVColorPanel[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       4,    0,   46,    2, 0x0a /* Public */,
       5,    0,   47,    2, 0x0a /* Public */,
       6,    0,   48,    2, 0x0a /* Public */,
       7,    0,   49,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void QHSVColorPanel::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QHSVColorPanel *_t = static_cast<QHSVColorPanel *>(_o);
        switch (_id) {
        case 0: _t->hsvColorChangedH(); break;
        case 1: _t->hsvColorChangedSV(); break;
        case 2: _t->changeBackgroundToBlack(); break;
        case 3: _t->changeBackgroundToGray(); break;
        case 4: _t->changeBackgroundToWhite(); break;
        case 5: _t->turnOnOffColorInfo(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (QHSVColorPanel::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QHSVColorPanel::hsvColorChangedH)) {
                *result = 0;
            }
        }
        {
            typedef void (QHSVColorPanel::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&QHSVColorPanel::hsvColorChangedSV)) {
                *result = 1;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject QHSVColorPanel::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_QHSVColorPanel.data,
      qt_meta_data_QHSVColorPanel,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *QHSVColorPanel::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QHSVColorPanel::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_QHSVColorPanel.stringdata))
        return static_cast<void*>(const_cast< QHSVColorPanel*>(this));
    return QWidget::qt_metacast(_clname);
}

int QHSVColorPanel::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void QHSVColorPanel::hsvColorChangedH()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void QHSVColorPanel::hsvColorChangedSV()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
